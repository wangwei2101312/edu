# EDU - Educational sample applications
   - Author: Gyula Matyas
   - Version: 1.0.1
   - Last modified: 2018.09.04.
   - Based on: self requirements specified in doc/req sub-folder 


#### The current **README** contains the following chapters: 
   1. Supported platforms 
   2. Prerequisites 
   3. Installing the sources 
   4. Building the sources 
   5. Usage of the EDU command line application 
   6. Running the unit tests 
   7. Known issues 
   8. Known issues 
   9. Design considerations 
   10. Suggested improvements for the future 


## 1. Supported platforms 

###  1.1 Supported OS/Architecture 
 - Windows 32|64 - tested on Windows 7 64 bit      
 - Linux 32|64   - tested on Gentoo Linux 32|64 bit, Debian Linux 32|64 bit 

### 1.2 Supported C++ standards:
 - C++ 14 - tested for C++ 14 


## 2. Prerequisites 

### *2.1. CMake 3.0 or later 

### 2.2. GTest 1.7.0 or later, the Google Test framework for unit tests 

### *2.3. A C++ compiler, supporting at least the C++14 standard: 
 - gcc 4.9 or later (POSIX)       
 - MSVC 2015 (part of MS Visual Studio 2015), or later (Windows) 

### *2.4. STL library, std=C++14 or later 

### 2.5. (Optional) valgrind for memory tests 

### 2.6. (Optional) callgrind for profiling performances 

### 2.7. (Optional) doxygen for generating documentation from the sources 


*) = mandatory, to can build the application. 

   When non mandatory elements are missing, the build system needs to skip the affected sub-modules 
   Ex: no unit test will be built, when GTest is missing, or no memory leak tests are possible, when valgrind is missing


## 3. Installing the sources 

### 3.1. Copy the "edu.7z" archive into a local folder. The EDU archive can be downloaded from: 
 - a Google drive share: 
   - https://drive.google.com/open?id=11O65tMtLSHgA9yj_mfHq0V_7GPDHOHEW
 - a GitLab repository: 
   - https://gitlab.com/wangwei2101312/edu
   - 'git@gitlab.com:wangwei2101312/edu.git' 
     
### 3.2. Extract the archive - when everything went well, there shall appear a sub-folder with the content:
 - **doc** sub-folder - contains the relevant documentation: 
   -  requirements, in sub-folder "req" 
   -  doxygen generated HTML documentation, in sub-folder "doxygen" 
 - **source** sub-folder - contains the C++ source code for: 
   -  EDU library, in sub-folder **edulib** 
   -  TASKS library, in sub-folder **taskslib** 
   -  EDU command line application, in sub-folder **eduapp** 
 - **tests** sub-folder - contains tests 
   - unit tests, in sub-folder **unit** (GTest based) 
   - **externals** sub-folder - contains third part software like: 
     - Google test framework, to can build and run unit tests when there is no compatible variant installed on the system.
       For copyright and license information please consult the README and LICENSE files from externals/gmock-1.7.0
     - ASIO sockets library, the standalone variant (no boost depndencies), to can use TCP/UDP sockets 
       For copyright and license information please consult the README and LICENSE files from externals/asio-1.11.0
     - Web sockets library, based on standalone ASIO (no boost dependencies), to can use web-sockets 
       For copyright and license information please consult the README and LICENSE files from externals/websocketpp
 - **data** sub-folder - contains sample test data      
 - **CMakeLists.txt** the root cmake file of the solution
 - **build_linux.sh** a bash script, for automated build on Linux 
 - **build_linux_utest.sh** a bash script, for automated build on Linux, including the build of unit tests 
 - **build_windows.bat** a batch script, for semi-automated build on Windows
 - **build_windows_utest.bat** a batch script, for semi-automated build on Windows, including the build of unit tests 
 - **doxygen.cfg** configuration file for generating doxygen documentation
 - **README.md** the current Readme


## 4. Building the sources

### 4.1. Linux

#### 4.1.1. Use the CMake GUI/command line, or use "build_linux.sh" which generates CMake based make files and builds the binaries
 - When build_linux.sh is used:
   - it can transmit command line options further to CMake. 
   
     Command line options:
     * -DCMAKE_BUILD_TYPE:  Debug, or Release (default), specifies the build type
     * -DEDU_ENABLE_GTEST: ON, or OFF (default), specifies whether unit tests shall be built also
     * -DEDU_GTEST_MODE:   internal (default), or external, specifies where from to use GTest (please see 6.3)
     * -DEDU_ENABLE_ASIO: ON (default), or OFF, specifies whether ASIO sockets external library shall be built
     * -DEDU_ENABLE_ASIO_SLL: ON, or OFF (default), specifies whether SSL version of ASIO sockets external library shall be built
     * -DEDU_ENABLE_WEBSOCKETPP: ON (default), or OFF, specifies whether web-sockets external library shall be built
   
     *Example*:
     * sh build_linux.sh -DCMAKE_BUILD_TYPE=Debug  - for Debug build
     * sh build_linux.sh -DEDU_ENABLE_GTEST=ON     - for building the unit tests also

   - by default temporary CMake files and generated make files will be written into the "build/linux" sub-folder  
   - EDU static library (libedulib.a) is configured to be built into the "lib" sub-folder
   - TASKS static library (libtaskslib.a) is configured to be built into the "lib" sub-folder
   - EDU command line application (edu) is configured to be built into the "bin" sub-folder
     
 - When build_linux_utest.sh is used:
    - It does the same as build_linux.sh and in-plus includes automatically the unit tests in the generated solution 

          
### 4.2. Windows

#### 4.2.1. Use the CMake GUI, or use "build_windows.bat" which generates a CMake based solution
 - When build_windows.bat is used:
   - by default it uses the user's default build generator 
   - it can transmit command line options further to CMake 
              
     Command line options:
     * -DEDU_ENABLE_GTEST: ON, or OFF (default), specifies whether unit tests shall be built also
     * -DEDU_GTEST_MODE:   internal (default), or external, specifies where from to use GTest (please see 6.3)
     * -DEDU_ENABLE_ASIO: ON (default), or OFF, specifies whether ASIO sockets external library shall be built
     * -DEDU_ENABLE_ASIO_SLL: ON, or OFF (default), specifies whether SSL version of ASIO sockets external library shall be built
     * -DEDU_ENABLE_WEBSOCKETPP: ON (default), or OFF, specifies whether web-sockets external library shall be built
   
     *Example*:
     * build_windows.bat -G "Visual Studio 14 2015" - to specify the build generator explicitly
     * build_windows.bat -G "Visual Studio 14 2015" -DEDU_ENABLE_GTEST=ON - for the generator and unit tests

   - by default temporary CMake files and generated make files will be written into the "build/windows" sub-folder  
   - EDU static library (edulib.lib) is configured to be built into the "lib/[Debug][Release]" sub-folder
   - TASKS static library (taskslib.lib) is configured to be built into the "lib/[Debug][Release]" sub-folder
   - EDU command line application (edu.exe) is configured to be built into the "bin/[Debug][Release]" sub-folder
     
 - When build_windows_utest.bat is used:
    - It does the same as build_windows.bat and in-plus includes automatically the unit tests in the generated solution 

#### 4.2.2. Build the solution
 - Open the generated build files with Visual Studio (or the selected generator's IDE) and compile/build 
 - EDU static library (edulib.lib) is configured to be built into the "lib" sub-folder 
   (by MSVC under Debug/Release/x64/.. sub-folders - according to build type)
 - TASKS static library (taskslib.lib) is configured to be built into the "lib" sub-folder 
   (by MSVC under Debug/Release/x64/.. sub-folders - according to build type)
 - EDU command line application (edu.exe) is configured to be built into the "bin" sub-folder
   (by MSVC under Debug/Release/x64/.. sub-folders - according to build type) 
          

## 5. Usage of the **EDU** command line application

### 5.1. Run from command line with the -h option, it displays a help for how to use it
 - Launching the help:
   - edu -h 
 - For running the samples there is necessary to specify in the command line the list of desired sample names. 
   There is possible to select all samples, samples from a certain group, an individual sample, or combining multiple filtering options. 
   The list of available samples is listed by the command line help (see edu -h)
    
 *Examples*:
  - edu -all   # selects all samples
  - edu graphs:erouting    # selects the samples for routing algorithms using edge based directed graphs
  - edu :heap-sort :hash-map   # selects the samples for heap-sort and hash-map implementations
  - edu sort:   # selects the samples for all sort categories (heap-, quick, merge, radix, insertion, selection, bubble)
        

## 6. Running the unit tests

### 6.1. In the "tests/unit" folder we have a GTest implementation for the EDU library
 
### 6.2. By default, the unit test build is not enabled in the default CMake configuration. 
 - To enable unit tests use the command line argument:
   * -DEDU_ENABLE_GTEST=ON
   
   or make it active from the CMake-GUI

### 6.3. When GTest is localized by CMake, the unit test application is built into the "bin" sub-folder, as "edutest".
  - In case when GTest is missing from the system and installing it is not a reliable option, there exists the possibility to use the embedded variant from the "externals" folder, by specifying the desired CMake command line option
    * -DEDU_GTEST_MODE="internal", means GTest will be built and used from the externals sub-folder
    * -DEDU_GTEST_MODE="external", means GTest will be used as an installed package on the system
     
### 6.4. Executing the unit tests
 - We can run in command line the "edulib-utest" application and check the results
 - On Linux running the CTest variant as "make test" from the build folder (by default build/linux)
 - On Windows with MSVC using the RUN_TESTS entry in Solution explorer (applying a build for it)


## 7. Generating documentation from the source

### 7.1. Doxygen configuration
 - The doxygen.cfg file contains the configuration for generating the documentation of sources using doxygen
    - doxygen ./doxygen.cfg
 - By default the documentation will be placed in the doc/doxygen sub-folder.
   The default configuration includes class private members also and local classes, functions, global variables from source files, even from anonymous namespaces.

### 7.2. Using the documentation
 - The start page is the index.html file from the doc/doxygen sub-folder. The documentation provides the following grouping:
    - main section: a short description about each item from the libraries and a global overview
    - namespaces: consulting classes by namespace
    - files: consulting classes, structures, functions, enumerations, global variables on file level       
    - classes: consulting classes one by one
      

## 8. Known issues


## 9. Design considerations
  
### 9.1. Architectural design
 - Class diagrams and sequence diagrams are stored in the doc/design sub-folder
 - The folder doc/tasks/graphs contains some explanations (images, pdf) for explaining the graphs and the routing algorithms, using a given test data-set
	  
### 9.2. Thread-safe support
- No thread-safe support added yet, but the design preserves this possibility - e.g. public methods are never invoking other public methods in the same class hierarchy

### 9.3. Design principles
 - The edulib library contains the sample algorithms, design patterns, demonstrator implementations
 - The taskslib library supports with automated launcher tasks the run of each sample, offering filtering 
     possibilities.
 - Each sample task is declared and registered by the use of a macro, similar to the GTest TEST_SUITE use-cases

    
          EDU_TASK_SUITE(key, name, description)  
   
  where:
   - key - a string identifier for the task, offering grouping possibilities (ex: sorting:heap-sort)
   - name - a unique name, used for the decorated name of the generated the automated task class 
   - description - the description of task, to be displayed as a help paragraph

   
  Since the task suites are automatically created and registered into the task suites manager, there is necessary to ensure a full linkage (all objects) of the taskslib library into the eduapp executable. On MSVC compilers the most common option is /WHOLEARCHIVE:[libname], on GNU compilers the "wl,--whole-archive" linker options are used (see CMakeLists.txt of eduapp).
  
  To create a new task is simple (like by GTest):
   - create a cpp file and add it to the tasklib's cmake file
   - define a macro in it - example: to create the task, selectable for "my:", or ":one", or "my:one" is enough to do:  


          EDU_TASK_SUITE("my:one", MyOne, "Description text for 'my:one' to get printed by accessing the help")   
          {    
              //... your code here   
              EDU_EXPECT_TRUE(expression) // checks whether expression returns true, when not it reports as error   
              EDU_EXPECT_EQ(expr1, expr2) // checks whether expr1 is equal with expr2, otherwise reports an error   
              //...   
          }    

  Caution! 
   - Some MSVC compilers are not able to do incremental linking when a library is built with /WHOLEARCHIVE. In
      such cases the executables need a full rebuild to include well the latest object files
   - In case when automated tasks execution doesn't work (no available sample tasks are reported by the eduapp), please check whether your compiler needs another linker flag options.
   - The eduapp executable is an ultra-thin command line application for running the tasks of taskslib
      
### 9.4. Unit test coverage
  - Currently only the sort algorithms are covered with GTest type unit tests. However the tasks library provides some built-in testing functionalities, used by the implemented sample tasks

### 9.5. List of sample implementations

#### 9.5.1 Specifiers
 - specifiers:explicit -> a demonstrator to check how implicit conversions work, when no "explicit" keyword is present

#### 9.5.2 Algorithms
 - algorithms:binary-search -> task for the abstract binary-search algorithm
 - algorithms:strings -> task for presenting various small tricks on strings

#### 9.5.3 Sorting
 - sort:quick-sort -> task for the abstract quick-sort algorithm, with automatic pivot type selection, based on iterator category
 - sort:heap-sort -> task for the abstract heap-sort algorithm, using random access iterators
 - sort:merge-sort -> task for the abstract merge-sort algorithm, using random access iterators, with inline, or auxiliary container based merge
 - sort:radix-sort -> task for the abstract radix-sort algorithm, using forward iterators, with built-in support for signed/unsigned integers and ASCII character based strings
 - sort:insertion-sort -> task for the abstract insertion-sort algorithm, using bi-directional iterators
 - sort:selection-sort -> task for the abstract selection-sort algorithm, using forward iterators
 - sort:bubble-sort -> task for the abstract bubble-sort algorithm, using forward iterators

#### 9.5.4 Containers
 - containers:hash-map -> task for the similar scope implementation as std::unordered_map, but using less resources
 - containers:hash-set -> task for the similar scope implementation as std::unordered_set, but using less resources
 - containers:hash-multimap -> task for the similar scope implementation as std::unordered_multimap, but using less resources
 - containers:hash-multiset -> task for the similar scope implementation as std::unordered_multiset, but using less resources

#### 9.5.5 Graphs
 - graphs:egraph-simple -> task for building and presenting simple-edge based graph, without auxiliary information for connection edges
 - graphs:egraph-penalties -> task for building and presenting simple-edge based graph, without auxiliary information for connection edges (e.g. turn penalties)
 - graphs:vgraph -> task for building a vertex based graph (vertices are undirected points)
 - graphs:graph-builder -> task for presenting a graph-builder
 - graphs-erouting -> task for presenting a sample routing engine implementation, with the use of the abstract routing engine and a specialized adapter. 
   - For searching a route it is able to use:
     - _Depth First Search_ algorithm: first checks always the maximum depth as possible of a connection
     - _Breadth First Search_ algorithm: first checks all immediate neighbours, than is going to the next depth
     - _Dijkstra_ algorithm: uses a priority queue in order to find the best route quick as possible
   - Search direction can be:
     - _forward_: route search is done by consulting edges from start points towards end points
     - _backward_: route search is done by consulting edge from end points towards start points 
     - _bi-directional_: route search is done by consulting edges from start and end points in the same time
    - Handles forbidden/allowed turns
    - Handles the start/end vertices in a way (with special offseting) to not confuse the routing algorithm by start/end points on the same vertex (edge)
    - Open to handle routing in forbidden areas around start/end points
    - Open for flexible strategies for applying dynamic costs (e.g. traffic messages, conditional driving maneuvers etc.)
	  
## 10. Suggested improvements for the future
      
### 10.1. Adding multi-threading support for the routing algorithm and graphs
     
### 10.2. Adding support for alternative routes in the routing part

### 10.3. Adding 100% unit tests coverage

### 10.4. Adding a graphical demonstrator for the algorithms, to can follow better visually, how they work

### 10.5. Split better the specializations with the use of SFINAE

