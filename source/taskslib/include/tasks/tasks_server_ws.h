///////////////////////////////////////////////////////////////////////////////////////////////////
// Header file for web sockets based tasks server
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.09.04.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_SERVER_WS_H__
#define __TASKS_SERVER_WS_H__

#include <string>


namespace edu
{

/** \brief Main class for web sockets based tasks server
  */
class TasksServerWs
{
public:
    /** \brief Constructor for web sockets based task server
      * \param[in] dataFolder - root data folder
      * \param[in] port - port for web server socket
      */
    TasksServerWs(const std::string& dataFolder,
                  uint16_t port);

    /** \brief Destructor
      */
    ~TasksServerWs() = default;

    /// Disabled copy constructor
    TasksServerWs(const TasksServerWs&) = delete;

    /// Disabled move constructor
    TasksServerWs(TasksServerWs&&) = delete;

    /// Disabled copy operator
    TasksServerWs& operator=(const TasksServerWs&) = delete;

    /// Disabled move operator
    TasksServerWs& operator=(TasksServerWs&&) = delete;

public:
    /** \brief Runs the task server
      * \returns true, when succeed, otherwise false
      */
    bool run();

private:
    const std::string&  m_dataFolder; //!< root data folder
    const uint16_t      m_port;       //!< port for web server socket
};

}

#endif //__TASKS_SERVER_WS_H__
