///////////////////////////////////////////////////////////////////////////////////////////////////
// Header file for local console tasks server
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.09.04.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_SERVER_LC_H__
#define __TASKS_SERVER_LC_H__

#include <string>


namespace edu
{

/** \brief Main class for tasks server
  */
class TasksServerLc
{
public:
    /** \brief Constructor for the local console server
      * \param[in] dataFolder - root data folder
      */
    TasksServerLc(const std::string& dataFolder);

    /** \brief Destructor
      */
    ~TasksServerLc() = default;

    /// Disabled copy constructor
    TasksServerLc(const TasksServerLc&) = delete;

    /// Disabled move constructor
    TasksServerLc(TasksServerLc&&) = delete;

    /// Disabled copy operator
    TasksServerLc& operator=(const TasksServerLc&) = delete;

    /// Disabled move operator
    TasksServerLc& operator=(TasksServerLc&&) = delete;

public:
    /** \brief Runs the task server
      * \returns true, when succeed, otherwise false
      */
    bool run();

private:
    const std::string&  m_dataFolder; //!< root data folder
};

}

#endif //__TASKS_SERVER_LC_H__
