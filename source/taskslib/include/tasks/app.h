///////////////////////////////////////////////////////////////////////////////////////////////////
// Header file for application main controller 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.09.04.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_APP_H__
#define __TASKS_APP_H__

#include <string>
#include <list>
#include <vector>
#include <tuple>


namespace edu
{

/** \brief Main controller class to run selected tasks
  *
  *  - when started in standalone mode: executes all tasks filtered from the command line
  *
  *  - when started in service mode: expects for an explicit request to execute a task
  * 
  *  see the -h command line option for further details about command line options 
  */
class App
{
public:
    /** \brief Constructor
      */
    App() = default;
	
    /** \brief Destructor
      */
    ~App() = default;

    /// Disabled copy constructor
    App(const App&) = delete;

    /// Disabled move constructor
    App(App&&) = delete;

    /// Disabled copy operator
    App& operator=(const App&) = delete;

    /// Disabled move operator
    App& operator=(App&&) = delete;

public:
    /** \brief Entry point into the application controller.
      *
      * Initializes the application, than launches the task manager to execute tasks sequentially, 
      * or in parallel by demand 
      * When captures the help ("-h") command prints help and returns shortly
      * 
      * \param[in] options - list of input command line options
      * \returns true, when succeed, otherwise false
      */
    bool run(const std::list<std::string>& options);

    /** \brief Help displaying method
      * \param[in] filter - filtering help messages
      */
    static void help(const std::string& filter);

private:
    /** \brief Initializes the application controller
      *
      *  Parses and validates command line options, fills-up the validated configuration items
      * 
      * \param[in]  options - list of input command line options
      * \param[out] config -  container for validated configuration items, 
      *                          based on the input command line options
      * \returns true, when succeed, otherwise false
      */
    bool init(const std::list<std::string>& options,
                    std::vector<std::pair<uint8_t, std::string>>& config);

    /** \brief Parses start-up configuration
      * \param[in,out] options - list of command line options
      * \param[out]    config -  container for start-up options
      * \returns true, when succeed, otherwise false
      */
    bool parse_config(std::list<std::string>& options,
                      std::vector<std::pair<uint8_t, std::string>>& config);

    /** \brief Filters out executable tasks, based on the input options 
      * \param[in,out] options - list of command line options
      */
    void filter_tasks(std::list<std::string>& options);

    /** \brief Execute tasks linearly
      * \param[in] dataFolder - root data folder
      */
    void execute_tasks(const std::string& dataFolder);
};

}

#endif //__TASKS_APP_H__
