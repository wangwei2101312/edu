///////////////////////////////////////////////////////////////////////////////////////////////////
// Helpers for hash container samples
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_HASH_HELPERS_H__
#define __TASKS_HASH_HELPERS_H__

#include <edu/log.h>
#include <iostream>
#include <vector>
#include <deque>
#include <memory>
#include <algorithm>
#include <sstream>
#include <assert.h>


namespace edu
{
namespace containers
{

namespace
{
    /** \brief Sample class for hashing
      */
    class A
    {
        // Enabled access for externally implemented operators and functors
        friend bool operator ==(const A&, const A&);
        friend struct is_equal<A>;
    public:
        /** \brief Constructor
          * \param[in] name - string for class name
          */
        A(const std::string& name) : m_name(name) {}

        /** \brief Copy constructor
          * \param[in] other - object where from copy
          */
        A(const A& other) : m_name(other.m_name) {}

        /** \brief Move constructor
          * \param[in] other - object where from copy
          */
        A(A&& other) : m_name(std::move(other.m_name)) {}

        /** \brief Destructor
          */
        ~A() = default;

        /** \brief Move operator
          * \param[in] other - object where from copy
          * \returns current instance
          */
        A& operator =(A&& other)
        {
            if (&other != this)
            {
                m_name = std::move(other.m_name);
            }
            return *this;
        }

        // Deleted copy and move operators
        A& operator =(const A&) = delete;

    public:
        /** \brief Operator for retrieving name of instance
          * \returns constant string with the name of instance
          */
        const std::string& operator()() const
        {
            return m_name;
        }

        /** \brief Operator for comparing with other object
          * \param[in] other - object to get compared
          * \returns true when other is equal, otherwise false
          */
        bool operator ==(const A& other)
        {
            return m_name == other.m_name;
        }
        
    private:
        std::string m_name; // name of instance
    };

    /** \brief Function to generate key value from integers for various key types
      * \param[in] num - numeric key 
      * \returns instance of desired object/type
      */
    template<class T>
    T createKey(int32_t num)
    {
        return num;
    }
    template<>
    A createKey<A>(int32_t num)
    {
        char p[6] = { char(((num / 10000) % 10) + '0'), 
                      char(((num / 1000) % 10) + '0'),
                      char(((num / 100) % 10) + '0'),
                      char(((num / 10) % 10) + '0'),
                      char((num % 10) + '0'),
                      0 };
        return A(p);
    }

    /** \brief Function to transform value into printable
      * \param[in] val - value to print
      * \returns printable value
      */
    template<class T>
    T printable(const T& val)
    {
        return val;
    }
    const std::string printable(const A& val)
    {
        return val();
    }
    
}

/** \brief Specialized hash functor for class A
 */
template<>
struct hash_of<A>
{
    /** \brief Operator for functor invoke
      * \param[in] l - left side instance of class A
      * \param[in] r - right side instance of class A
      * \returns hash value as unsigned integer
      */
    size_t operator()(const A& key) const
    {
        return hash_of<std::string>()((key)());
    }
};

/** \brief Specialized equal_to functor for class A
  */
template<>
struct is_equal<A>
{
    /** \brief Operator for functor invoke
      * \param[in] l - left side instance of class A
      * \param[in] r - right side instance of class A
      * \returns true, when left instance has the same name with right isntace
      */
    bool operator()(const A& l, 
                    const A& r) const
    {
        return (l.m_name == r.m_name);
    }
};


}
}

#endif //__TASKS_HASH_HELPERS_H__
