///////////////////////////////////////////////////////////////////////////////////////////////////
// Sample hash-set implementation
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.26.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/task.h>
#include <edu/containers/hashset.h>
#include "hash_helpers.h"
#include <edu/log.h>

namespace edu
{
namespace containers
{

namespace
{

    /** \brief Function for inserting range of values into the hash set
      * \param[in] hash - hash-set reference
      * \param[in] from - start of integer range
      * \param[in] to - end of integer range
      * \param[in] res_size - expected size of container
      * \returns true, when succeed, otherwise false
      */
    template<class T>
    bool insertRange(HashSet<T>& hash,
                     int32_t from,
                     int32_t to,
                     size_t res_size)
    {
        bool res = true;
        // Insert the values
        for (int32_t num = from; num < to; num++)
        {
            hash.insert(createKey<T>(num));
            hash.insert(createKey<T>(to - (num - from) - 1));
        }
        // Checkc container size
        if (hash.size() != res_size)
        {
            logError("<INSERT> size of container expected ", res_size, " instead of ", hash.size());
            res = false;
        }
        // Check inserted values
        for (int32_t num = from; num < to; num++)
        {
            auto it = hash.find(createKey<T>(num));
            if (it != hash.end())
            {
                if (*it == createKey<T>(num))
                {
                    continue;
                }
            }
            logError("<INSERT> Item not refound {", printable(createKey<T>(num)), "}");
            res = false;
        }
        return res;
    }

    /** \brief Function for removing range of values from the hash-set
      * \param[in] hash - hash-set reference
      * \param[in] from - start of integer range
      * \param[in] to - end of integer range
      * \param[in] res_size - expected size of container
      * \returns true, when succeed, otherwise false
      */
    template<class T>
    bool deleteRange(HashSet<T>& hash,
                     int32_t from,
                     int32_t to,
                     size_t res_size)
    {
        bool res = true;
        // Delete the values
        for (int32_t num = from; num < to; num++)
        {
            hash.erase(createKey<T>(num));
            hash.erase(createKey<T>(to - (num - from) - 1));
        }
        // Check container size
        if (hash.size() != res_size)
        {
            logError("<DELETE> size of container expected ", res_size, " instead of ", hash.size());
            res = false;
        }
        // Check removed values
        for (int32_t num = from; num < to; num++)
        {
            auto it = hash.find(createKey<T>(num));
            if (it != hash.end())
            {
                logError("<DELETE> Deleted item refound {", printable(*it), "} for number ", num);
                res = false;
            }
        }
        return res;
    }

    /** \brief Function for associating range of values into the hash-set
      * \param[in] hash - hash-set reference
      * \param[in] from - start of integer range
      * \param[in] to - end of integer range
      * \param[in] res_size - expected size of container
      * \returns true, when succeed, otherwise false
      */
    template<class T>
    bool associateRange(HashSet<T>& hash,
                        int32_t from,
                        int32_t to,
                        size_t res_size)
    {
        bool res = true;
        // Associate the values
        for (int32_t num = from; num < to; num++)
        {
            hash[createKey<T>(num)];
            hash[createKey<T>(to - (num - from) - 1)];
        }
        // Check container size
        if (hash.size() != res_size)
        {
            logError("<ASSOCIATE> size of container expected ", res_size, " instead of ", hash.size());
            res = false;
        }
        // Check associated values
        for (int32_t num = from; num < to; num++)
        {
            auto it = hash.find(createKey<T>(num));
            if (it != hash.end())
            {
                if (*it == createKey<T>(num))
                {
                    continue;
                }
            }
            logError("<ASSOCIATE> Item not refound {", printable(createKey<T>(num)), ", ", num, "}");
            res = false;
        }
        return res;
    }

    /** \brief Function for verifying range of values in the hash-set
      * \param[in] hash - hash-set reference
      * \param[in] from - start of integer range
      * \param[in] to - end of integer range
      * \returns true, when succeed, otherwise false
      */
    template<class T>
    bool checkRange(HashSet<T>& hash,
                    int32_t from,
                    int32_t to)
    {
        bool res = true;
        // Check content
        for (int32_t num = from; num < to; num++)
        {
            auto it = hash.find(createKey<T>(num));
            if (it != hash.end())
            {
                if (*it == createKey<T>(num))
                {
                    continue;
                }
            }
            logError("<CHECK> Item not refound {", createKey<T>(num), "}");
            res = false;
        }
        return res;
    }

    /** \brief Verify table
      * \param[in] hash - hash-set to dump
      * \param[in] control - vector with control values
      * \param[in] dump - flag to indicate whther to dump values
      * \returns true, when succeed, otherwise false
    */
    template<class T>
    bool verifyHash(HashSet<T>& hash,
                     std::vector<T>& control,
                     bool dump)
    {
        bool res = true;
        // Check content
        if (hash.size() != control.size())
        {
            logError("<VERIFY> size of container expected ", control.size(), " instead of ", hash.size());
            res = false;
        }
        // Verify hash values in container with forward iterator
        std::for_each(std::begin(hash), std::end(hash), [&](const T& val)
        {
            auto it = std::find_if(control.begin(), control.end(), [&val](const T& ctr)
            {
                return is_equal<T>()(val, ctr);
            });
            if (it == control.end())
            {
                logError("<CHECK> Item not refound {", printable(val), "}");
                res = false;
            }
        });
        // Verify hash values in container with reverse iterator
        std::for_each(hash.rbegin(), hash.rend(), [&](const T& val)
        {
            auto it = std::find_if(control.begin(), control.end(), [&val](const T& ctr)
            {
                return is_equal<T>()(val, ctr);
            });
            if (it == control.end())
            {
                logError("<CHECK> Item not refound {", printable(val), "}");
                res = false;
            }
        });
        // Verify hash values by iterating cotnainer
        std::for_each(control.begin(), control.end(), [&](const T& val)
        {
            auto it = hash.find(val);
            if (it == hash.end())
            {
                logError("<CHECK> Item not refound {", printable(val), "}");
                res = false;
            }
        });
        // Dump content
        if (dump)
        {
            logInfo_("<DUMP> Hash = ");
            for (auto& val : hash)
            {
                logInfo_("{", printable(val), "}, ");
            }
            logInfo("");
        }
        return res;
    }

}


///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("containers:hash-set", HashSet, 
               "Containers: hash-set sample implementation -> Q(N) = 1")
{
    {
        logInfo("Testing hash-set for \"class A\" as key:");
        HashSet<A, hash_of<A>, is_equal<A>> hash;
        EDU_EXPECT_TRUE(insertRange(hash, 0, 100, 100));
        EDU_EXPECT_TRUE(insertRange(hash, 90, 150, 150));
        EDU_EXPECT_TRUE(associateRange(hash, 120, 450, 450));
        EDU_EXPECT_TRUE(deleteRange(hash, 40, 550, 40));
        EDU_EXPECT_TRUE(deleteRange(hash, 10, 30, 20));
        EDU_EXPECT_TRUE(deleteRange(hash, 0, 5, 15));
        EDU_EXPECT_TRUE(associateRange(hash, 0, 15, 25));
        EDU_EXPECT_TRUE(deleteRange(hash, 5, 35, 10));
        std::vector<A> control
        { 
            { createKey<A>(0) },
            { createKey<A>(1) },
            { createKey<A>(2) },
            { createKey<A>(3) },
            { createKey<A>(4) },
            { createKey<A>(35) },
            { createKey<A>(36) },
            { createKey<A>(37) },
            { createKey<A>(38) },
            { createKey<A>(39) }
        };
        EDU_EXPECT_TRUE(verifyHash(hash, control, true));
    }
    {
        logInfo("Testing hash-set for \"int32_t\" as key:");
        HashSet<int32_t> hash;
        EDU_EXPECT_TRUE(insertRange(hash, 0, 100, 100));
        EDU_EXPECT_TRUE(insertRange(hash, 90, 150, 150));
        EDU_EXPECT_TRUE(associateRange(hash, 120, 450, 450));
        EDU_EXPECT_TRUE(deleteRange(hash, 40, 550, 40));
        EDU_EXPECT_TRUE(deleteRange(hash, 10, 30, 20));
        EDU_EXPECT_TRUE(deleteRange(hash, 0, 5, 15));
        EDU_EXPECT_TRUE(associateRange(hash, 0, 15, 25));
        EDU_EXPECT_TRUE(deleteRange(hash, 5, 35, 10));
        std::vector<int32_t> control
        {
            { createKey<int32_t>(0) },
            { createKey<int32_t>(1) },
            { createKey<int32_t>(2) },
            { createKey<int32_t>(3) },
            { createKey<int32_t>(4) },
            { createKey<int32_t>(35) },
            { createKey<int32_t>(36) },
            { createKey<int32_t>(37) },
            { createKey<int32_t>(38) },
            { createKey<int32_t>(39) }
        };
        HashSet<int32_t> _hash(std::forward<HashSet<int32_t>&&>(hash));
        EDU_EXPECT_TRUE(verifyHash(_hash, control, true));
        _hash.erase(_hash.find(control[4]));
        _hash.clear();
    }
}

}
}
