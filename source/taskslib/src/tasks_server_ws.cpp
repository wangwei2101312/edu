///////////////////////////////////////////////////////////////////////////////////////////////////
// Source file for web sockets based tasks server
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.09.04.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/tasks_server_ws.h>
#include <tasks/task.h>
#include <tasks/app.h>
#include <edu/log.h>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

namespace edu
{

namespace
{
    // pull out the type of messages sent by our config
    using web_server = websocketpp::server<websocketpp::config::asio>;
    using message_ptr = web_server::message_ptr;

    // Define a callback to handle incoming messages
    void on_message(TasksServerWs& tasks,
                    web_server& ws,
                    websocketpp::connection_hdl hdl, 
                    message_ptr msg) 
    {
        // Log incomming message
        logInfo("");
        logInfo(">> ", msg->get_payload());
        // Handle incomming message
        if (msg->get_payload() == "exit") 
        {
            ws.stop_listening();
            ws.stop();
            return;
        }
        else
        {

        }

        try 
        {
            ws.send(hdl, msg->get_payload(), msg->get_opcode());
        }
        catch (websocketpp::exception const & e) 
        {
            logError(e.what());
        }
    }
    
    /** \brief Thread worker function
      * \param[in] tasks - instance of tasks server
      * \param[in] ws - instance of web server socket
      * \param[in] port - port for server socket
      */
    void thread_execute(TasksServerWs& tasks, 
                        web_server& ws,
                        uint16_t port)
    {
        try
        {
            // Set logging settings
            ws.set_access_channels(websocketpp::log::alevel::none);

            // Initialize Asio
            ws.init_asio();

            // Register our message handler
            ws.set_message_handler(bind(&on_message, std::ref(tasks), std::ref(ws), 
                                        websocketpp::lib::placeholders::_1,
                                        websocketpp::lib::placeholders::_2));

            // Listen on the configured port 
            ws.listen(port);

            // Start the server accept loop
            ws.start_accept();

            // Start the ASIO io_service run loop
            ws.run();
        }
        catch (websocketpp::exception const & e)
        {
            logError(e.what());
        }
        catch (...)
        {
            logError("Unexpected error");
        }
    }

}


///////////////////////////////////////////////////////////////////////////////////////////////////
TasksServerWs::TasksServerWs(const std::string& dataFolder, uint16_t port)
: m_dataFolder(dataFolder)
, m_port(port)
{
}


///////////////////////////////////////////////////////////////////////////////////////////////////
bool TasksServerWs::run()
{
    bool isOK = true;
    // Create a server endpoint
    web_server ws;
    // Initialize worker thread
    std::thread th(&thread_execute, std::ref(*this), std::ref(ws), m_port);
    if (isOK)
    {
        // Listen for local command
        bool do_exit = false;
        logInfo("Started web socket based tasks server.");
        logInfo("Type \"-h [[group]:[task]]\" for help, type \"exit\" to terminate");
        while (!do_exit)
        {
            std::string cmd;
            logInfo_("EDU tasks server>");
            std::getline(std::cin, cmd);
            if (cmd == "exit")
            {
                // Session terminated
                if (ws.is_listening())
                {
                    ws.stop_listening();
                }
                if (!ws.stopped())
                {
                    ws.stop();
                }
                do_exit = true;
            }
            else if (cmd.find("-h", 0) != std::string::npos)
            {
                cmd = cmd.substr(2);
                // Trim help parameter
                cmd.erase(0, cmd.find_first_not_of(' '));
                if (!cmd.empty())
                {
                    cmd.erase(cmd.find_last_not_of(' ') + 1);
                }
                App::help(cmd);
            }
            else if (!cmd.empty())
            {
                logError("Unrecognized command \"", cmd, "\"");
            }
        }
    }
    // Wait until thread finishes
    th.join();
    return isOK;
}


}
