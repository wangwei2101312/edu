///////////////////////////////////////////////////////////////////////////////////////////////////
// Search algorithms
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.26.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/task.h>
#include <edu/algorithms/binary_search.h>
#include "search_helpers.h"
#include <edu/log.h>
#include <sstream>
#include <algorithm>
#include <functional>
#include <vector>
#include <deque>
#include <assert.h>


namespace edu
{
namespace algorithms
{


///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("algorithms:binary-search", BinarySearch, 
               "Search algorithms: binary search -> Q(N) = log(N)")
{
    {
        std::vector<int32_t> container(vec_int1);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching an integer vector"));
    }
    {
        std::vector<size_t> container(vec_int2);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching an unsigned integer vector", std::greater<size_t>()));
    }
    {
        std::vector<std::string> container(vec_str1);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching a string vector"));
    }
    {
        std::vector<std::string> container(vec_str1);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching a string vector"));
    }
    {
        std::vector<std::string> container(vec_str2);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching a string vector"));
    }
    {
        std::vector<std::string> container(vec_str3);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching a string vector"));
    }
    {
        std::vector<std::string> container(vec_str4);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching a string vector"));
    }
    {
        std::vector<std::string> container(vec_str5);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching a string vector"));
    }
    {
        std::vector<Child> container(vec_child);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching a child vector by height"));
    }
    {
        std::vector<Child> container(vec_child);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching a child vector by shortest height", ChildByShortest()));
    }
    {
        std::vector<Child> container(vec_child);
        EDU_EXPECT_TRUE(test_binary_search(std::begin(container), std::end(container), "Searching a child vector by name", ChildByName()));
    }
    {
        const int32_t size = 2000;
        std::vector<int32_t> vec;
        vec.reserve(2 * size);
        for (int32_t i = 0; i < size; i++)
        {
            vec.insert(vec.begin(), -2 * i);
            vec.push_back(2 * i);
        }
        EDU_EXPECT_TRUE(test_binary_search(std::begin(vec), std::end(vec), "Searching a large integer vector", std::less<int32_t>(), false));
    }
    {
        const int32_t size = 2000;
        std::deque<int32_t> deq;
        for (int32_t i = 0; i < size; i++)
        {
            deq.push_front(2 * i);
            deq.push_back(-2 * i);
        }
        EDU_EXPECT_TRUE(test_binary_search(std::begin(deq), std::end(deq), "Searching a large integer deque", std::greater<int32_t>(), false));
    }
    {
        const int32_t size = 2000;
        int32_t arr[2 * size];
        for (int32_t i = 0; i < size; i++)
        {
            arr[i] = 2 * (i - int32_t(size));
            arr[i + size] = 2 * i;
        }
        EDU_EXPECT_TRUE(test_binary_search(std::begin(arr), std::end(arr), "Searching a large integer array", std::less<int32_t>(), false));
    }
    {
        logInfo("Executing range search on an integer vector");
        std::vector<int32_t> vec{ -2, -2, -2, 0, 1, 2, 3, 3, 3};
        auto res = make_binary_search_range(std::begin(vec), std::end(vec), -2);
        if ( !( (std::get<0>(res) == vec.begin() && (std::get<1>(res) == (vec.begin() + 3) ) ) ) )
        {
            EDU_EXPECT_TRUE(false);
            logError("Range search failed!");
        }
        res = make_binary_search_range(std::begin(vec), std::end(vec), 3);
        if ( !( (std::get<0>(res) == (vec.begin() + 6) && (std::get<1>(res) == (vec.begin() + 9) ) ) ) )
        {
            EDU_EXPECT_TRUE(false);
            logError("Range search failed!");
        }
        res = make_binary_search_range(std::begin(vec), std::end(vec), 0);
        if ( !( (std::get<0>(res) == (vec.begin() + 3) && (std::get<1>(res) == (vec.begin() + 4) ) ) ) )
        {
            EDU_EXPECT_TRUE(false);
            logError("Range search failed!");
        }
        res = make_binary_search_range(std::begin(vec), std::end(vec), -1);
        if ( !( (std::get<0>(res) == vec.end()) && (std::get<1>(res) == vec.end() ) ) )
        {
            EDU_EXPECT_TRUE(false);
            logError("Range search failed!");
        }
        res = make_binary_search_range(std::begin(vec), std::end(vec), 7);
        if ( !( (std::get<0>(res) == vec.end()) && (std::get<1>(res) == vec.end() ) ) )
        {
            EDU_EXPECT_TRUE(false);
            logError("Range search failed!");
        }
        res = make_binary_search_range(std::begin(vec), std::end(vec), -7);
        if ( !( (std::get<0>(res) == vec.end()) && (std::get<1>(res) == vec.end() ) ) )
        {
            EDU_EXPECT_TRUE(false);
            logError("Range search failed!");
        }
    }
}


}
}
