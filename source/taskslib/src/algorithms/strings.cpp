///////////////////////////////////////////////////////////////////////////////////////////////////
// Algorithms on strings
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.26.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/task.h>
#include <edu/algorithms/strings.h>
#include <edu/log.h>
#include <assert.h>
#include <tuple>


namespace edu
{


namespace algorithms
{

namespace
{
    /** \brief Test data for "invertWordChars"
      */
    const std::string white_spaces("!?:),. ");
    using iwc_entry = std::tuple<const std::string, const std::string>;
    const iwc_entry iwc_data[] 
    {
        iwc_entry("", ""),
        iwc_entry("?:)", "?:)"),
        iwc_entry("Hello!!:) how are you, OK?", "olleH!!:) woh era uoy, KO?")
    };

    /** \brief Test data for "findLongestSequence"
    */
    using fls_entry = std::tuple<const std::string, const int32_t, const size_t>;
    const fls_entry fls_data[]
    {
        fls_entry("aabbbbbbbaaaaaaaaaaaacccudddddddddddddddddddddd", 25, 22),
        fls_entry("aabbbbbbbaaaaaaaaaaaacccuddddddddddddddddddddddkkkeiiii", 25, 22),
        fls_entry("", -1, 0),
        fls_entry("abc", 0, 1),
        fls_entry("aaaaaaaaaaaaaaaaabcccccaaaaaaaaaaaaaaa", 0, 17),
        fls_entry("kaaaaaaaaaaaaaaaaabcccccaaaaaaaaaaaaaaa", 1, 17),
    };

    /** \brief Test data for "isUniqueChars"
    */
    using iuc_entry = std::tuple<const std::string, const int32_t, const bool>;
    const iuc_entry iuc_data[]
    {
        iuc_entry("dekirk", 5, false),
        iuc_entry("dekirks", 5, false),
        iuc_entry("dekirsd", 6, false),
        iuc_entry("dekirsubt", -1, true),
        iuc_entry("aachen", 1, false),
        iuc_entry("", -1, true),
        iuc_entry("@", -1, true)
    };
}


///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("algorithms:strings", Strings, 
               "Algorithms on strings: various algorithms executed on strings")
{
    // Function to run "findLongestSequence" with sample data
    {
        logInfo("");
        logInfo("Sample for finding longest consecutive characters in a word:");
        for (const auto& data : fls_data)
        {
            int32_t pos = -1;
            size_t len = 0;
            logInfo("");
            logInfo("Input string: ", std::get<0>(data));
            string::findLongestSequence(std::get<0>(data), pos, len);
            logInfo("Longest sequence: '", ((pos >= 0) ? std::get<0>(data).at(pos) : '\0'),
                    "' at pos = ", pos, " length = ", len);
            EDU_EXPECT_TRUE((pos == std::get<1>(data)) && (len == std::get<2>(data)));
        }
        logInfo("");
    }
    
    // Function to run "findLongestSequence" with sample data
    {
        logInfo("");
        logInfo("Sample for verifying unique characters in a word:");
        for (const auto& data : iuc_data)
        {
            int32_t pos = -1;
            logInfo("");
            logInfo("Input string: ", std::get<0>(data));
            // First variant
            auto res = string::isUniqueChars(std::get<0>(data), pos);
            logInfo_("Unique characters: ", (res ? "YES" : "NO -> "));
            if (!res)
            {
                logInfo_("Char = '", std::get<0>(data).at(pos), "' at pos = ", pos);
            }
            logInfo("");
            EDU_EXPECT_TRUE((pos == std::get<1>(data)) && (res == std::get<2>(data)));
            // Second variant
            res = string::isUniqueChars2(std::get<0>(data), pos);
            EDU_EXPECT_TRUE((pos == std::get<1>(data)) && (res == std::get<2>(data)));
        }
        logInfo("");
    }
    
    // Function to run "invertWordChars" with sample data
    {
        logInfo("");
        logInfo("Sample for inverting chars in words:");
        for (const auto& data : iwc_data)
        {
            std::string str(std::get<0>(data));
            logInfo("");
            logInfo("Input string: ", str);
            string::invertWordChars(str, white_spaces);
            logInfo("Output string: ", str);
            EDU_EXPECT_TRUE(str == std::get<1>(data));
        }
        logInfo("");
    }
}


}
}
