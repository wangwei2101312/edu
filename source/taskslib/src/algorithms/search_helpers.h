///////////////////////////////////////////////////////////////////////////////////////////////////
// Helpers for ssearch samples
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_SEARCH_HELPERS_H__
#define __TASKS_SEARCH_HELPERS_H__

#include <edu/log.h>
#include <iostream>
#include <vector>
#include <deque>
#include <memory>
#include <algorithm>
#include <sstream>
#include <assert.h>


namespace edu
{
namespace algorithms
{

namespace
{
    /** \brief Local sample structure
      */
    struct Child
    {
        std::string name;
        int32_t     height;

        /** \brief Less or equal operator
          * \param[in] other - the other child object to get compared
          * \returns true, when current object is less as height, otherwise false
          */
        bool operator < (const Child& other) const
        {
            return (height < other.height);
        }

        /** \brief Conversion operator to char*
          * \returns a text array, composed from name and height
          */
        operator const char* () const
        {
            static std::string res_;

            std::stringstream text("");
            text << name.c_str() << ":" << height;

            res_ = text.str();

            return res_.c_str();
        }
    };
    
    /** \brief Equal operator
      * \param[in] l - left child object to get compared
      * \param[in] r - right child object to get compared
      * \returns true, when left object equal with right object, otherwise false
      */
    bool operator ==(const Child& l, const Child& r)
    {
        return (l.name == r.name) && (l.height == r.height);
    }

    /** \brief Functor for sorting child objects ascendant by name
      */
    struct ChildByName
    {
        bool operator()(const Child& l, const Child& r)
        {
            return (l.name < r.name);
        }
    };

    /** \brief Functor for sorting child objects ascendant by height
      */
    struct ChildByShortest
    {
        bool operator()(const Child& l, const Child& r)
        {
            return (l.height < r.height);
        }
    };
    
    /** \brief Increment a certain value
      */
    template<typename T>
    T _increment(const T& val)
    {
        return val + 1;
    }
    /** \brief Decrement a certain value
      */
    template<typename T>
    T _decrement(const T& val)
    {
        return val - 1;
    }
        
    template<>    
    std::string _increment(const std::string& val)
    {
        std::string str(val);
        if (str.length() > 0)
        {
            str[str.length() - 1]++;
        }
        return str;
    }
    template<>    
    std::string _decrement(const std::string& val)
    {
        std::string str(val);
        if (str.length() > 0)
        {
            str[str.length() - 1]--;
        }
        return str;
    }

    template<>    
    Child _increment(const Child& val)
    {
        Child child(val);
        if (child.name.length() > 0)
        {
            child.name[child.name.length() - 1]++;
        }
        child.height++;
        return child;
    }
    template<>    
    Child _decrement(const Child& val)
    {
        Child child(val);
        if (child.name.length() > 0)
        {
            child.name[child.name.length() - 1]--;
        }
        child.height--;
        return child;
    }
    
    /** \brief Local sample data
      */
    const std::vector<int32_t> vec_int1({ -100, -80, -6, -4, -2, 0, 10, 18, 110, 112, 134, 220, 224, 334 });
    const std::vector<size_t> vec_int2({ 24, 22, 20, 18, 16, 14, 12, 10, 8, 6, 4, 2, 0 });
    const std::vector<std::string> vec_str1({ "a", "c", "e", "g" });
    const std::vector<std::string> vec_str2({ "u" });
    const std::vector<std::string> vec_str3({ "u", "z" });
    const std::vector<std::string> vec_str4({ "a", "m", "z" });
    const std::vector<std::string> vec_str5({ "a", "c", "m", "x", "z" });
    const std::deque<std::string> deq_str({ "1one", "2two", "3three", "4four", "5five", "6six", "7seven", "8eight", "9nine" });
    const std::vector<Child> vec_child({ { "A. John", 160 }, { "B. Eduard", 164 }, { "C. Lionel", 168 }, { "D. Lionel", 170 }, { "E. Ed", 174 }, { "F. Sarah", 176 } });


    /** \brief Initialize binary search
      * \param[in] first - iterator, pointing to the lower bound of sorted range
      * \param[in] last - iterator, pointing to the upper bound of sorted range
      * \param[in] message - message to print
      * \param[in] compare - compare function, usually a functor
      * \param[in] dump - when true, dumps the content of source and sorted ranges
      */
    template<typename Iterator, 
             typename Fnc>
    bool test_binary_search(Iterator first, 
                            Iterator last,
                            const std::string& message,
                            Fnc compare, 
                            bool dump = true)
    {
        bool isOK = true;
        logInfo(message);
        // Print the container
        if (dump)
        {
            logInfo_("The sorted range: ");
            std::for_each(first, last, [](const auto& val)
            {
                logInfo_(val, ", ");
            });
            logInfo("");
        }
        size_t fake_founds = 0;
        size_t misses = 0;
        std::for_each(first, last, [&](const auto& val)
        {
            Iterator res;
            // Shall find direct value
            res = make_binary_search(first, last, val, compare);
            if ((res == last) || (!(*res == val)))
            {
                isOK = false;
                misses++;
            }
            
            // Shall find not fake value
            res = make_binary_search(first, last, _increment(val), compare);
            if (res != last)
            {
                isOK = false;
                fake_founds++;
            }
            
            // Shall find not fake value
            res = make_binary_search(first, last, _decrement(val), compare);
            if (res != last)
            {
                isOK = false;
                fake_founds++;
            }            
        });
        if (!isOK)
        {
            logError("Search error observed!! Misses = ", misses, "; Fake founds = ", fake_founds);
        }
        logInfo("");

        return isOK;
    }
    
    /** \brief Initialize binary search
      * \param[in] first - iterator, pointing to the lower bound of sorted range
      * \param[in] last - iterator, pointing to the upper bound of sorted range
      * \param[in] message - message to print
      * \param[in] dump - when true, dumps the content of source and sorted ranges
      */
    template<typename Iterator> 
    bool test_binary_search(Iterator first, 
                            Iterator last,
                            const std::string& message, 
                            bool dump = true)
    {
        bool isOK = true;
        logInfo(message);
        // Print the container
        if (dump)
        {
            logInfo_("The sorted range: ");
            for (auto it = first; it != last; ++it)
            {
                logInfo_(*it, ", ");
            }
            logInfo("");
        }
        size_t fake_founds = 0;
        size_t misses = 0;
        for (auto it = first; it != last; ++it)
        {
            Iterator res;
            // Shall find direct value
            res = make_binary_search(first, last, *it);
            if ((res == last) || (!(*res == *it)))
            {
                isOK = false;
                misses++;
            }
            
            // Shall find not fake value
            res = make_binary_search(first, last, _increment(*it));
            if (res != last)
            {
                isOK = false;
                fake_founds++;
            }
            
            // Shall find not fake value
            res = make_binary_search(first, last, _decrement(*it));
            if (res != last)
            {
                isOK = false;
                fake_founds++;
            }            
        }
        if (!isOK)
        {
            logError("Search error observed!! Misses = ", misses, "; Fake founds = ", fake_founds);
        }
        logInfo("");

        return isOK;
    }
}


}
}

#endif //__TASKS_SEARCH_HELPERS_H__
