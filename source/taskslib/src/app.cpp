///////////////////////////////////////////////////////////////////////////////////////////////////
// Source file for application main controller 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.09.04.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/app.h>
#include <tasks/task.h>
#include <edu/log.h>
#include <algorithm>
#include <functional>
#include <forward_list>
#include <assert.h>
#include <fstream>
#include <sstream>
#include <tasks/tasks_server_lc.h>
#ifdef _EDU_WEBSOCKET_
#include <tasks/tasks_server_ws.h>
#endif


namespace edu
{

namespace
{
    // Configuration results
    const uint8_t   config_is_missing = 0;  //!< configuration is missing
    const uint8_t   config_is_ok      = 1;  //!< configuration is present
    const uint8_t   config_is_wrong   = 2;  //!< configuration error

    /** \brief Configuration items
      */
    enum Config
    {
        config_help = 0,          //!< help has been requested from command line
        config_data_folder,       //!< data folder for selected tasks, pointing to the root data folder 
        config_service_mode_sw,   //!< runs in service mode, communicating via web sockets
        config_service_mode_sl,   //!< runs in service mode, communicating via local console
        config_dump,              //!< dumps noisy messages during task executions
        config_all_tasks,         //!< select all tasks, ignoring the task filtering input options
        config_last               //!< used for encounting configuration item identifiers  
    };

    /** \brief Startup options, specifying
      * - Configuration item's internal identifier - a "Config" enumeration value
      * - Configuration item's external identifier - a unique string
      * - Configuration item needs an argument: true = yes, false = not
      * - Configuration item's help display
      */
    const std::vector<std::tuple<Config, std::string, bool, std::string>>
    startup_options
    {
        { std::make_tuple(config_help,              "-h",    false, "Display helps for the usage of tasks launcher") },
        { std::make_tuple(config_data_folder,       "-d",    true,  "Data folder to use for certain tasks") },
        { std::make_tuple(config_service_mode_sw,   "-sw",   true,  "Run in service mode: communication is done on web-sockets, specify port") },
        { std::make_tuple(config_service_mode_sl,   "-sl",   false, "Run in service mode: communication is done on local console") },
        { std::make_tuple(config_all_tasks,         "-all",  false, "Select all available tasks for execution") },
        { std::make_tuple(config_dump,              "-dump", false, "Dump noisy messages") }
    };


    /** \brief Display help on the output console
      */
    void display_help()
    {
        // Print help headlines
        logInfo("Educational sample tasks for C++ 11/14 programming and algorithms");
        logInfo("Accepts command line arguments :");
        // Parse available startup options
        for (auto it = std::begin(startup_options); it != std::end(startup_options); ++it)
        {
            logInfo("    ", std::get<1>(*it), std::get<2>(*it) ? " [argument] = " : " = ", std::get<3>(*it));
        }
        logInfo("");
        logInfo("Extra options for filtering available tasks:");
        logInfo_("    [group 1:task 1] .. [group k:task n] = optional, explicit filtering for tasks to run ");
        logInfo("by specifiying the task names.");
        logInfo("      Task names can be partial also, as [group:], or [:name]");
        logInfo("");
        logInfo("      Example: :binary-search :graph - will run \"algorithms:binary-search\" and \"graphs:graph\" ");
        logInfo("               sort: - will run all tasks from group \"sort:\" ");
        logInfo("");
        // Print available tasks with their summary
        logInfo("Available programming sample tasks: ");
        size_t pos = 1;
        std::for_each(TaskSuites::instance().begin(), TaskSuites::instance().end(), [&pos](const TaskSuites::task_entry& task)
        {
            // Print task's unique name
            logInfo(pos, ". \"", std::get<0>(task), "\"");
            // Print task's summary
            logInfo(std::get<2>(task)());
            logInfo("");

            // Increment position
            pos++;
        });
    }

}


///////////////////////////////////////////////////////////////////////////////////////////////////
bool App::run(const std::list<std::string>& options)
{
    auto isOK = true;
    logInfo("Educational sample tasks: version ", _get_version());
    // Initialize - parse startup options
    std::vector<std::pair<uint8_t, std::string>> config(config_last);
    isOK = init(options, config);
    // When startup requests help - print help and skip further executions
    if ((config_is_ok == config[config_help].first))
    {
        display_help();
    }
    // When no errors observed
    else if (isOK)
    {
        // Initialize log file
        if (config[config_data_folder].first == config_is_ok)
        {
            // Normalize data folder
            std::replace(config[config_data_folder].second.rbegin(), config[config_data_folder].second.rend(), '\\', '/');
            if (*config[config_data_folder].second.rbegin() != '/')
            {
                config[config_data_folder].second.append("/");
            }

            // Initialize log file
            static std::ofstream log_file;
            log_file.open((config[config_data_folder].second + "log.txt").c_str());
            // Add to log channels - when valid
            if (log_file.is_open())
            {
                logchannels().add_channel(log_file);
            }
        }

        // When there exists active tasks 
        if (TaskSuites::instance().begin() != TaskSuites::instance().end())
        {
            if (config[config_service_mode_sw].first == config_is_ok)
            {
            #ifdef _EDU_WEBSOCKET_
                // When web-socket service mode is invoked - install web service
                logInfo("Starting service for web server socket...");
                // Extract port from configuration options
                std::stringstream _s(config[config_service_mode_sw].second);
                uint16_t port{};
                _s >> port;
                // Instantiate and start the server
                isOK = TasksServerWs(config[config_data_folder].second, port).run();
            #else
                logError("Support for web-sockets not enabled in build configuration!");
                isOK = false;
            #endif
            }
            else if (config[config_service_mode_sl].first == config_is_ok)
            {
                // When local service mode is invoked - install the console service
                logInfo("Starting local service for tasks...");
                // Instantiate and start the server
                isOK = TasksServerLc(config[config_data_folder].second).run();
            }
            else
            {
                // When no service mode is invoked -> execute the tasks linearly
                logInfo("Executing selected tasks...");
                execute_tasks(config[config_data_folder].second);
            }
        }
        else
        {
            // When sample not found, report for user
            logError("No active tasks selected! Type -h for consulting the usage mode");
        }

    }
    return isOK;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
bool App::init(const std::list<std::string>& options,
               std::vector<std::pair<uint8_t, std::string>>& config)
{
    auto isOK = true;
    // Container for startup options
    std::list<std::string> options_(options);
    // Parse configurations
    isOK = parse_config(options_, config);
    // Fill up log stream, when needed
    if (config[config_data_folder].first == config_is_ok)
    {
        std::replace(config[config_data_folder].second.rbegin(), config[config_data_folder].second.rend(), '\\', '/');
        if (*config[config_data_folder].second.rbegin() != '/')
        {
            config[config_data_folder].second.append("/");
        }
    }
    if (isOK
        && (config_is_missing == config[config_help].first)
        && (config_is_missing == config[config_all_tasks].first))
    {
        // When not all tasks selected - parse task filtering options
        filter_tasks(options_);
            
        // Check if still options there
        for (auto it = std::begin(options_); it != std::end(options_); ++it)
        {
            // When sample not found, report for user
            logError("Unrecognized start-up options: \"", *it, "\"!");
            isOK = false;
        }
    }
    return isOK;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
bool App::parse_config(std::list<std::string>& options,
                       std::vector<std::pair<uint8_t, std::string>>& config)
{
    auto isOK = true;
    for (auto it = std::begin(options); it != std::end(options);)
    {
        // Search input option in the available startup options list
        // startup_options: {0 = configuration id, 1 = unique string identifier, 2 = description for user help, 3 = has/not has argument}
        // config: {0 = result (present/misssing/wrong, argument}
        auto it_ = std::find_if(std::begin(startup_options), std::end(startup_options), [&it](const auto& val)
        {
            // Unique string identifier is identical with the command line option
            return (std::get<1>(val) == *it);
        });
        // When configuration item matched 
        if (it_ != std::end(startup_options))
        {
            // Initialize configuration item as OK
            config[std::get<0>(*it_)].first = config_is_ok;
            // When help reuested - can stop further parsing
            if (config_help == std::get<0>(*it_))
            {
                // No more need to process options - help is present 
                options.clear();
                // Clean up error flag and return shortly
                isOK = true;
                break;
            }
            // Mark startup option as present
            config[std::get<0>(*it_)].first = true;
            // Check if argument needed for option
            if (std::get<2>(*it_))
            {
                // Remove processed option
                it = options.erase(it);
                // Go for the argument
                if (it == std::end(options))
                {
                    // Argument is missing from options - mark the configuration item as wrong
                    config[std::get<0>(*it_)].first = config_is_wrong;
                    // Set error flag
                    isOK = false;
                    break;
                }
                // Store the argument for configuration
                config[std::get<0>(*it_)].second = *it;
            }
            // Remove processed option
            it = options.erase(it);
            continue;
        }
        ++it;
    }
    if (!isOK)
    {
        // Report the configuration errors
        for (auto it = std::begin(config); it != std::end(config); ++it)
        {
            if (config_is_wrong == it->first)
            {
                logError("Invalid startup configuration: ", std::get<1>(startup_options[it - std::begin(config)]), " expects an argument!");
            }
        }
        // Last message
        logInfo("Please type -h for usage information");
    }
    return isOK;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
void App::filter_tasks(std::list<std::string>& options)
{
    // Iterate task descriptors
    for (auto it = std::begin(TaskSuites::instance()); it != std::end(TaskSuites::instance());)
    {
        // When task found, mark it enabled
        auto it_ = (std::find_if(std::begin(options), std::end(options), [&it](auto& opt)
        {
            return TaskSuites::instance().match(*it, opt);
        })); 
        if (it_ != std::end(options))
        {
            // Remove processed option
            options.erase(it_);
        }
        else
        {
            // Remove task, when not in options
            it = TaskSuites::instance().erase(it);
            continue;
        }
        ++it;
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
void App::execute_tasks(const std::string& dataFolder)
{
    // Iterate samples
    auto task_id = 1;
    auto errors = 0;
    std::for_each(TaskSuites::instance().begin(), TaskSuites::instance().end(),            
        [&dataFolder, &task_id, &errors](const TaskSuites::task_entry& task)
    {
        // When sample enabled -> execute
        logInfo(task_id, ". ", "Executing \"", std::get<0>(task), "\"");
        if (std::get<1>(task)(dataFolder + std::get<0>(task) + "/") != 0)
        {
            errors++;
        }
        logInfo("");
        // Increment task id
        task_id++;
    });

    logInfo("");
    if (0 == errors)
    {
        // When no errors: print the number of executed samples
        logInfo((task_id - 1), " samples executed with success!");
    }
    else
    {
        // When errors: print the number of errors from the executed samples
        logError(errors, " samples from ", (task_id - 1), " encountered errors !!!");
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
void App::help(const std::string& filter)
{
    if (filter.empty())
    {
        // Print a full help
        display_help();
    }
    else
    {
        // Print available tasks with their summary
        size_t pos = 1;
        std::for_each(TaskSuites::instance().begin(), TaskSuites::instance().end(), [&pos, &filter](const TaskSuites::task_entry& task)
        {
            // When task found, mark it enabled
            if (TaskSuites::instance().match(task, filter))
            {
                // Print task's unique name
                logInfo(pos, ". \"", std::get<0>(task), "\"");
                // Print task's summary
                logInfo(std::get<2>(task)());
                logInfo("");
                // Increment position
                pos++;
            }
        });
    }
}


}
