///////////////////////////////////////////////////////////////////////////////////////////////////
// Source file for local console tasks server
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.09.04.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/tasks_server_lc.h>
#include <tasks/task.h>
#include <tasks/app.h>
#include <edu/log.h>

namespace edu
{

///////////////////////////////////////////////////////////////////////////////////////////////////
TasksServerLc::TasksServerLc(const std::string& dataFolder)
: m_dataFolder(dataFolder)
{
}


///////////////////////////////////////////////////////////////////////////////////////////////////
bool TasksServerLc::run()
{
    bool isOK = true;
    if (isOK)
    {
        // Listen for local command
        bool do_exit = false;
        logInfo("Started local tasks service.");
        logInfo("Type \"-h [[group]:[task]]\" for help, type \"exit\" to terminate");
        logInfo("Type \"[group]:[task]\" for executing a certain task");
        while (!do_exit)
        {
            std::string cmd;
            logInfo_("EDU tasks>");
            std::getline(std::cin, cmd);
            if (cmd == "exit")
            {
                // Session terminated
                do_exit = true;
            }
            else if (cmd.find("-h", 0) != std::string::npos)
            {
                // Help requested
                cmd = cmd.substr(2);
                // Trim help parameter
                cmd.erase(0, cmd.find_first_not_of(' '));
                cmd.erase(cmd.find_last_not_of(' ') + 1);
                App::help(cmd);
            }
            else if (!cmd.empty())
            {
                // Task execution requested
                cmd.erase(0, cmd.find_first_not_of(' '));
                cmd.erase(cmd.find_last_not_of(' ') + 1);
                auto task_id = 1;
                if (!cmd.empty())
                {
                    // Iterate samples
                    std::for_each(TaskSuites::instance().begin(), TaskSuites::instance().end(),
                        [&task_id, &cmd, this](const TaskSuites::task_entry& task)
                    {
                        // When task found, mark it enabled
                        if (TaskSuites::instance().match(task, cmd))
                        {
                            // When sample matched -> execute
                            logInfo(task_id, ". ", "Executing \"", std::get<0>(task), "\"");
                            std::get<1>(task)(this->m_dataFolder + std::get<0>(task) + "/");
                            // Increment task id
                            task_id++;
                        }
                    });
                }
                if (1 == task_id)
                {
                    logError("Unrecognized task \"", cmd, "\"");
                }
            }
        }
    }
    // Wait until thread finishes
    return isOK;
}


}
