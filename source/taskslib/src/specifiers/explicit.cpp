///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding "explicit" keyword
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.26.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/task.h>
#include <edu/specifiers/explicit.h>
#include <edu/log.h>
#include <iostream>
#include <assert.h>


namespace edu
{


namespace specifiers
{

namespace
{
    /** \brief Sample class used for demonstrating "exlicit" keyword
      */
    using A = Explicit<int32_t>;

    /** \brief Tries to apply an iomplicit conversion
      * \param[in] a - object of A
      * \returns true
      */
    auto f(A a)->bool
    {
        logInfo(" -> Could use implicit conversion for ", a.dump());
        return true;
    }

    /** \brief When the above could not be used buy the compiler, it relies here
      * \param[in] a - any type, what could no be converted above
      * \returns false
      */
    template<class T> 
    auto f(T& a)->bool
    {
        logInfo(" -> Could NOT use implicit conversion for ", a);
        return false;
    }

    /** \brief Sample string class for tests purpose
      */
    class string_my : public std::string
    {
    public:
        /** \brief Constructor
          * \param[in] p - array of characters
          */
        string_my(const char *p)
        : std::string(p)
        {
        }

        /** \brief Operator for converting into an integer value
          * \returns value of first character as integer
          */
        operator int32_t() const
        {
            return c_str()[0];
        }
    };
}



///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("specifiers:explicit", Explicit, 
    "Explicit specifier: how to prevent implicit conversions made by the compiler")
{
    // Converts implicitly to A(int)
    logInfo("Converting f(int) into f(A(int))");
    EDU_EXPECT_TRUE(f(90)); // converts implicitely to A(int)
    logInfo("");

    // Converts implicitly to A(string_my)
    logInfo("Converting f(string_my) into f(A(string_my))");
    EDU_EXPECT_TRUE(f(string_my("a"))); // converts implicitely to A(string_my)
    logInfo("");

    // Converts implicitly to A(const char*)
    logInfo("Converting f(const char*) into f(A(const char*))");
    EDU_EXPECT_TRUE(!f("a")); // no implicit conversion shall be observed
    logInfo("");
}


}
}
