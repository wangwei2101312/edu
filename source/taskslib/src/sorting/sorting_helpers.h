///////////////////////////////////////////////////////////////////////////////////////////////////
// Helpers for sorting samples
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_SORTING_HELPERS_H__
#define __TASKS_SORTING_HELPERS_H__

#include <edu/log.h>
#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <forward_list>
#include <memory>
#include <algorithm>
#include <sstream>
#include <assert.h>


namespace edu
{
namespace sorting
{

namespace
{
    /** \brief Functor iterator based generic "less" compare
      */
    struct is_less
    {
        /** \brief Launching operator
          * \param[in] l - left side iterator
          * \param[in] r - right side iterator
          * \returns true, when left side is less than right side, otherwise false
          */
        template<typename Iterator>
        bool operator()(Iterator l, Iterator r) const
        {
            return *l < *r;
        }
    };

    /** \brief Functor iterator based generic "greater" compare
      */
    struct is_greater
    {
        /** \brief Launching operator
          * \param[in] l - left side iterator
          * \param[in] r - right side iterator
          * \returns true, when left side is greater than right side, otherwise false
          */
        template<typename Iterator>
        bool operator()(Iterator l, Iterator r) const
        {
            return *l > *r;
        }
    };

    /** \brief Local sample structure
      */
    struct Child
    {
        std::string name;
        int32_t     height;

        /** \brief Less operator
          * \param[in] other - the other child object to get compared
          * \returns true, when current object is less as height, otherwise false
          */
        bool operator < (const Child& other) const
        {
            return (height < other.height);
        }

        /** \brief Conversion operator to char*
          * \returns a text array, composed from name and height
          */
        operator const char* () const
        {
            static std::string res_;

            std::stringstream text("");
            text << name.c_str() << ":" << height;

            res_ = text.str();

            return res_.c_str();
        }
    };


    /** \brief Functor for sorting child objects ascendant by name
      */
    struct ChildByName
    {
        template<typename Iterator>
        bool operator()(Iterator l, Iterator r) const
        {
            return l->name < r->name;
        }

        bool operator()(const Child& l, const Child& r) const
        {
            return l.name < r.name;
        }        
    };

    /** \brief Functor for sorting child objects descendant by height
      */
    struct ChildByShortest
    {
        template<typename Iterator>
        bool operator()(Iterator l, Iterator r) const
        {
            return l->height < r->height;
        }

        bool operator()(const Child& l, const Child& r) const
        {
            return l.height < r.height;
        }
        
    };
    
    /** \brief Functor for sorting child objects descendant by height
      */
    struct ChildByTallest
    {
        template<typename Iterator>
        bool operator()(Iterator l, Iterator r) const
        {
            return (l->height > r->height);
        }
        
        bool operator()(const Child& l, const Child& r) const
        {
            return l.height > r.height;
        }
    };

    /** \brief Local sample data
      */
    const std::vector<int32_t> vec_int00({});
    const std::vector<int32_t> vec_int01({ 5 });
    const std::vector<int32_t> vec_int02({ 5, 2 });
    const std::vector<int32_t> vec_int03({ 2, 5 });
    const std::vector<uint32_t> vec_int1({ 5, 2, 4, 6, 7, 9, 8, 10, 1, 34, 2, 4, 3, 4, 9, 4, 2 });
    const std::vector<int32_t> vec_int2({ 12, -11, -10, 9, -8, 7, -6, 5, 4, 3, 2, 1, 0 });
    const std::vector<std::string> vec_str1({ "a", "b", "z", "k" });
    const std::vector<std::string> vec_str2({ "z", "u" });
    const std::vector<std::string> vec_str3({ "u", "z" });
    const std::vector<std::string> vec_str4({ "z", "m", "a" });
    const std::vector<std::string> vec_str5({ "a", "z", "m" });
    const std::deque<std::string> deq_str({ "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" });
    const std::vector<Child> vec_child({ { "John", 160 }, { "Eduard", 154 }, { "Lionel", 161 }, { "Lionel", 157 }, { "Ed", 141 }, { "Sarah", 151 } });
    const std::list<int32_t> lst_int1({ 120, -11, -10, 9, -8, 76, -6, 5, 4, 3, 2, 1, 0 });
    const std::forward_list<uint32_t> lst_int2({ 5, 2, 4, 6, 7, 9, 8, 10, 1, 34, 2, 4, 3, 4, 9, 4, 2 });


    /** \brief Initialize sorting test
      * \param[in] first - iterator, pointing to the lower bound of range to sort
      * \param[in] last - iterator, pointing to the upper bound of range to sort
      * \param[in] message - message to print
      * \param[in] dump - when true, dumps the content of source and sorted ranges
      */
    template<typename Iterator>
    void sort_test_init(Iterator first,
                        Iterator last,
                        const std::string& message,
                        bool dump)
    {
        logInfo(message);
        // Print the unsorted range
        if (dump)
        {
            logInfo_("The unsorted range: ");
            std::for_each(first, last, [](const auto& val)
            {
                logInfo_(val, ", ");
            });
            logInfo("");
        }
    }

    /** \brief Evaluates results of sorting 
      * \param[in] first - iterator, pointing to the lower bound of range to sort
      * \param[in] last - iterator, pointing to the upper bound of range to sort
      * \param[in] compare - compare function, usually a functor
      * \param[in] dump - when true, dumps the content of source and sorted ranges
      * \returns true, when succeed, otherwise false
      */
    template<typename Iterator,
             typename Fnc>
    bool sort_test_evaluate(Iterator first,
                            Iterator last,
                            Fnc compare,
                            bool dump)
    {
        // Print the sorted range
        if (dump)
        {
            logInfo_("The sorted range: ");
        }
        auto pos = first;
        auto pos_ = first;
        auto isOK = true;
        std::for_each(first, last, [&](const auto& val)
        {
            if (dump)
            {
                logInfo_(val, ", ");
            }
            // Test validity of results
            if (std::function<bool(Iterator, Iterator)>(compare))
            {
                isOK &= !((pos != first) && (compare(pos, pos_)));
            }
            else
            {
                isOK &= !((pos != first) && (val < *(pos_)));
            }
            if (pos != first)
            {
                ++pos_;
            }
            ++pos;
        });
        if (dump)
        {
            logInfo("");
        }
        if (!isOK)
        {
            logError("Sort error observed!!");
        }
        logInfo("");
        return isOK;
    }

}

}
}

#endif //__TASKS_SORTING_HELPERS_H__
