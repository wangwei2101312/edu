///////////////////////////////////////////////////////////////////////////////////////////////////
// Insertion-sort sample implementation 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.26.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/task.h>
#include "sorting_helpers.h"
#include <edu/sorting/insertionsort.h>
#include <edu/log.h>
#include <iostream>
#include <vector>
#include <deque>
#include <memory>
#include <sstream>
#include <assert.h>


namespace edu
{
namespace sorting
{

namespace
{
    /** \brief Local sorter function
      * \param[in] first - random access iterator, pointing to the lower bound of range to sort
      * \param[in] last - random access iterator, pointing to the upper bound of range to sort
      * \param[in] message - message to print
      * \param[in] compare - compare function, usually a functor
      * \param[in] dump - when true, dumps the content of source and sorted containers
      * \returns true, when succeed, otherwise false
      */
    template<typename Iterator, 
             typename Fnc = std::function<bool(Iterator, Iterator)>>
    bool sort_test(Iterator first, 
                   Iterator last, 
                   const std::string& message, 
                   Fnc compare = nullptr, 
                   bool dump = true)
    {
        // Initialize the test
        sort_test_init(first, last, message, dump);
        // Invoke the sorter
        if (std::function<bool(Iterator, Iterator)>(compare))
        {
            make_insertion_sort(first, last, compare);
        }
        else
        {
            make_insertion_sort(first, last);
        }
        // Evaluate the results
        return sort_test_evaluate(first, last, compare, dump);
    }

}


///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("sort:insertion-sort", InsertionSort, 
               "Sorting algorithms: insertion sort -> O(N) = N x N")
{
    {
        auto container(vec_int00);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_int01);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_int02);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_int03);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_int1);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_int2);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_str1);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string vector"));
    }
    {
        auto container(vec_str2);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string vector"));
    }
    {
        auto container(vec_str3);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string vector"));
    }
    {
        auto container(vec_str4);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string vector"));
    }
    {
        auto container(vec_str5);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string vector"));
    }
    {
        auto container(deq_str);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string deque"));
    }
    {
        auto container(vec_child);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a child vector by shortest height"));
    }
    {
        auto container(vec_child);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a child vector by tallest height", ChildByTallest()));
    }
    {
        auto container(vec_child);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a child vector by name", ChildByName()));
    }
    {
        auto container(lst_int1);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer list"));
    }
    {
        const int32_t size = 2000;
        std::vector<int32_t> vec;
        vec.reserve(4 * size);
        for (int32_t i = 0; i < size; i++)
        {
            vec.push_back(i);
            vec.push_back(i / 2);
            vec.push_back(size - i);
            vec.push_back((size - i) / 2);
        }
        EDU_EXPECT_TRUE(sort_test(std::begin(vec), std::end(vec), "Sorting a large integer vector", is_greater(), false));
    }
    {
        const int32_t size = 2000;
        std::vector<int32_t> vec;
        vec.reserve(size);
        for (int32_t i = 0; i < size; i++)
        {
            vec.push_back(i);
        }
        EDU_EXPECT_TRUE(sort_test(std::begin(vec), std::end(vec), "Sorting a large integer vector", is_less(), false));
    }
    {
        const int32_t size = 2000;
        std::deque<int32_t> deq;
        for (int32_t i = 0; i < size; i++)
        {
            deq.push_back(size - i);
        }
        EDU_EXPECT_TRUE(sort_test(std::begin(deq), std::end(deq), "Sorting a large integer deque", is_less(), false));
    }
    {
        const int32_t size = 2000;
        int32_t arr[size];
        for (int32_t i = 0; i < size; i++)
        {
            arr[i] = (size - i);
        }
        EDU_EXPECT_TRUE(sort_test(std::begin(arr), std::end(arr), "Sorting a large integer array", is_less(), false));
    }
}


}
}
