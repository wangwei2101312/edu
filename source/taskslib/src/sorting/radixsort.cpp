///////////////////////////////////////////////////////////////////////////////////////////////////
// Radix-sort sample implementation 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.26.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/task.h>
#include "sorting_helpers.h"
#include <edu/sorting/radixsort.h>
#include <edu/log.h>
#include <iostream>
#include <vector>
#include <deque>
#include <memory>
#include <sstream>
#include <assert.h>


namespace edu
{
namespace sorting
{

namespace
{
        
	/** \brief Functor for child buckets by name for radix-sort
	  */
	template<typename T = Child, bool = false>
	struct buckets_child_by_name
	{
		// Buckets count for character values
		static const unsigned char size = 255;

		// Determines the bucket index for a certain character on a certain sort levels
		static unsigned char index(const Child& child,
								   size_t level,
								   size_t max_levels)
		{
            return (((max_levels - level - 1) < child.name.length()) ? child.name.at(max_levels - level - 1) : 0);
		}

		// Length of string
		static size_t length(const Child& child)
		{
			return child.name.length();
		}
	};


	// Divider
	static constexpr size_t divider(size_t level) noexcept
	{
		return (level ? (10 * divider(level - 1)) : 1);
	}

	// Digits
	static constexpr size_t digits(size_t num) noexcept
	{
		return (num ? (digits(num / 10) + 1) : 0);
	}

	/** \brief Functor for child buckets by shortest height for radix-sort
	  */
	template<typename T = Child, bool = false>
	struct buckets_child_by_shortest
	{
		// Buckets count for unsigned integer values
		static const size_t size = 10;

		// Determines the bucket index for a certain digit
		static size_t index(const Child& child,
						    size_t level,
							size_t)
		{
			return ((child.height / divider(level)) % 10);
		}

		// Digits of child height
		static size_t length(const Child& child)
		{
			return digits(child.height);
		}
	};

    
	/** \brief Functor for child buckets by tallest height for radix-sort
	  */
	template<typename T = Child, bool = false>
	struct buckets_child_by_tallest
	{
        // Buckets count for unsigned integer values
        static const size_t size = 10;

        // Determines the bucket index for a certain digit
        static size_t index(const Child& child,
							size_t level,
							size_t)
		{
			return (9 - ((child.height / divider(level)) % 10));
		}

        // Digits of child height
        static size_t length(const Child& child)
		{
			return digits(child.height);
		}
	};

    
    /** \brief Local sorter function with default bucket handler
      * \param[in] first - random access iterator, pointing to the lower bound of range to sort
      * \param[in] last - random access iterator, pointing to the upper bound of range to sort
      * \param[in] message - message to print
      * \param[in] compare - compare function, usually a functor
      * \param[in] dump - when true, dumps the content of source and sorted containers
      * \returns true, when succeed, otherwise false
      */
    template<typename Iterator,
             template<typename T_,bool = std::is_integral<T_>::value>class bucket_policy = radix_sort_buckets,
             typename Fnc = std::function<bool(Iterator, Iterator)>>
    bool sort_test(Iterator first, 
                   Iterator last, 
                   const std::string& message,
                   Fnc compare = nullptr,
                   bool dump = true)
    {
        // Initialize the test
        sort_test_init(first, last, message, dump);
        // Invoke the sorter
        make_radix_sort(first, last);
        // Evaluate the results
        return sort_test_evaluate(first, last, compare, dump);
    }
    
    
    /** \brief Local sorter function with explicit bucket handler
      * \param[in] first - random access iterator, pointing to the lower bound of range to sort
      * \param[in] last - random access iterator, pointing to the upper bound of range to sort
      * \param[in] message - message to print
      * \param[in] dump - when true, dumps the content of source and sorted containers
      * \returns true, when succeed, otherwise false
      */
    template<template<typename,bool = false>class bucket_policy,
             typename compare,
             typename Iterator>
    bool sort_test(Iterator first, 
                   Iterator last, 
                   const std::string& message,
                   bool dump = true)
    {
        // Initialize the test
        sort_test_init(first, last, message, dump);
        // Invoke the sorter
        make_radix_sort<bucket_policy>(first, last);
        // Evaluate the results
        return sort_test_evaluate(first, last, compare(), dump);
    }
    

}


///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("sort:radix-sort", RadixSort, 
               "Sorting algorithms: radix sort -> O(N) = N")
{
    {
        auto container(vec_int00);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_int01);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_int02);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_int03);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_int1);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_int2);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer vector"));
    }
    {
        auto container(vec_str1);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string vector"));
    }
    {
        auto container(vec_str2);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string vector"));
    }
    {
        auto container(vec_str3);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string vector"));
    }
    {
        auto container(vec_str4);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string vector"));
    }
    {
        auto container(vec_str5);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string vector"));
    }
    {
        auto container(deq_str);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting a string deque"));
    }
    {
        auto container(vec_child);
        EDU_EXPECT_TRUE((sort_test<buckets_child_by_shortest, ChildByShortest>(std::begin(container), std::end(container), "Sorting a child vector by shortest height")));
    }
    {
        auto container(vec_child);
        EDU_EXPECT_TRUE((sort_test<buckets_child_by_tallest, ChildByTallest>(std::begin(container), std::end(container), "Sorting a child vector by tallest height")));
    }
    {
        auto container(vec_child);
        EDU_EXPECT_TRUE((sort_test<buckets_child_by_name, ChildByName>(std::begin(container), std::end(container), "Sorting a child vector by name")));
    }
    {
        auto container(lst_int1);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer list"));
    }
    {
        auto container(lst_int2);
        EDU_EXPECT_TRUE(sort_test(std::begin(container), std::end(container), "Sorting an integer forward list"));
    }
    {
        const int32_t size = 2000;
        std::vector<int32_t> vec;
        vec.reserve(4 * size);
        for (int32_t i = 0; i < size; i++)
        {
            vec.push_back(i);
            vec.push_back(i / 2);
            vec.push_back(size - i);
            vec.push_back((size - i) / 2);
        }
        EDU_EXPECT_TRUE(sort_test(std::begin(vec), std::end(vec), "Sorting a large integer vector", is_less(), false));
    }
    {
        const int32_t size = 2000;
        std::vector<int32_t> vec;
        vec.reserve(size);
        for (int32_t i = 0; i < size; i++)
        {
            vec.push_back(i);
        }
        EDU_EXPECT_TRUE(sort_test(std::begin(vec), std::end(vec), "Sorting a large integer vector", is_less(), false));
    }
    {
        const int32_t size = 2000;
        std::deque<int32_t> deq;
        for (int32_t i = 0; i < size; i++)
        {
            deq.push_back(size - i);
        }
        EDU_EXPECT_TRUE(sort_test(std::begin(deq), std::end(deq), "Sorting a large integer deque", is_less(), false));
    }
    {
        const int32_t size = 2000;
        int32_t arr[size];
        for (int32_t i = 0; i < size; i++)
        {
            arr[i] = (size - i);
        }
        EDU_EXPECT_TRUE(sort_test(std::begin(arr), std::end(arr), "Sorting a large integer array", is_less(), false));
    }
}


}
}
