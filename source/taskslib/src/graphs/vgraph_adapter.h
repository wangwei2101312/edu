///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding Graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.08.02.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_GRAPHS_VGRAPH_ADAPTERS_H__
#define __TASKS_GRAPHS_VGRAPH_ADAPTERS_H__

#include <tasks/task.h>
#include <edu/graphs/vgraph.h>
#include <edu/graphs/graph_builder.h>
#include "graph_data.h"
#include <edu/log.h>
#include <assert.h>

namespace edu
{
namespace graph
{

namespace
{
    /** \brief Sample vertex structure for vertex based graphs
      */
    struct VVertex
    {
        size_t      id;
        std::string name;
    };
    /** \brief Sample edge structure for vertex based graphs
      */
    struct VEdge 
    {
        int32_t cost;
    };

    /** \brief Adapter for filling up vertex based graph from graph builder
      */
    template<typename Graph, 
             typename Builder>
    class VGraphAdapter
    {
    public:
        /** \brief Constructor
          * \param[in] graph - graph instance
          * \param[in] builder - graph builder instance
          */
        VGraphAdapter(Graph& graph,
                      Builder& builder)
        : m_graph(graph)
        , m_builder(builder)
        {
        }

        /** \brief Fills up the graph from builder
          * \returns true, when succeed, otherwise false 
          */
        bool operator()()
        {
            m_graph.resize(m_builder.vertices());
            bool res = m_builder.pull_vertices(*this);
            if (res)
            {
                res = m_builder.pull_connected_graph(*this, true, *this);
            }
            return res;
        }

        /** \brief Error handler invoked by the builder
          * \param[in] edge - problematic edge
          * \param[in] msg - builder message, the reason of error
          * \param[in] res - reference, or non-reference node of edge
          * \returns false, when pulling shall be aborted, otherwsie true
          */
        bool operator()(const InputEdge& edge, 
                        BuilderMessage msg,
                        bool ref)
        {
            if (BuilderMessage::vertex_is_missing == msg)
            {
                // Missed vertex 
                logError("Missing vertex for ", (ref ? "reference" : "non-reference"), " node for Edge = {id:",
                          edge.id, ", name:", edge.name, "}");
                return true;
            }
            return false;
        }

        /** \brief Pulls a vertex from the builder, without connected edges
          * \param[in] vertex - pulled vertex
          * \param[in] vertex_index - index of pulled vertex
          * \returns false, when pulling shall be aborted, otherwsie true
          */
        bool operator()(const InputVertex& vertex,
                        size_t vertex_index)
        {
            std::stringstream s;
            s << "V-" << vertex.id;
            m_graph.set_vertex(vertex_index,
                               std::move(VVertex{ vertex.id, s.str() }));
            return true;
        }

        /** \brief Pulls a graph edge from the builder, with connected edge, or vertex indexes
          * \param[in] edge - pulled edge
          * \param[in] edge_index - index of pulled edge
          * \param[in] ref_first - lower bound of range with connected edges in reference node
          * \param[in] ref_last - upper bound of range with connected edges in reference node
          * \param[in] nref_first - lower bound of range with connected edges in non-reference node
          * \param[in] nref_last - upper bound of range with connected edges in non-reference node
          * \returns false, when pulling shall be aborted, otherwsie true
          */
        template<typename Iterator>
        bool operator()(const InputEdge& edge,
                        size_t edge_index,
                        Iterator ref_first,
                        Iterator ref_last,
                        Iterator nref_first,
                        Iterator nref_last)
        {
            // The self-error handler prevents to have invalid vertices
            assert(ref_first != ref_last);
            assert(nref_first != nref_last);
            // Set edge between reference -> non-reference vertices
            if (edge.costFw)
            {
                m_graph.set_edge((*ref_first).first, (*nref_first).first,
                                  std::move(VEdge{ edge.costFw }));
            }
            // Set edge between non-reference -> reference vertices
            if (edge.costBw)
            {
                m_graph.set_edge((*nref_first).first, (*ref_first).first,
                                  std::move(VEdge{ edge.costBw }));
            }
            return true;
        }

    private:
        Graph&    m_graph;
        Builder&  m_builder;
    };

}
    

}
}
#endif //__TASKS_GRAPHS_VGRAPH_ADAPTERS_H__
