///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding Graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.26.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/task.h>
#include <edu/graphs/vgraph.h>
#include <edu/graphs/egraph.h>
#include <edu/graphs/graph_builder.h>
#include "graph_data.h"
#include "vgraph_adapter.h"
#include "egraph_adapter.h"
#include <edu/log.h>
#include <assert.h>

namespace edu
{
namespace graph
{


///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("graphs:vgraph-bidi", BidiGraphVertex, 
               "Bi-directional (for forward/backward routing) vertex based graph")
{
    // Bidirectional graph
    using _Graph = VGraph<VVertex, VEdge>;
    _Graph graph;

    // Build graph
    {
        logInfo("Building the bi-drectional vertex based graph");
        logInfo("Input data:");
        std::for_each(std::begin(edge_set_1), std::end(edge_set_1), [](const auto& edge)
        {
            logInfo("id:", edge.id, ", name:", edge.name,
                    ", R:", edge.startNode, ", NR:", edge.endNode,
                    ", Fw:", edge.costFw, ", Bw:", edge.costBw);
        });
         using _Builder = GraphBuilder<InputVertex, InputEdge>;
        _Builder builder;
        // Push edge set
        EDU_EXPECT_EQ(builder.push_edges(std::begin(edge_set_1), std::end(edge_set_1), true, error_observer()),
                          (std::end(edge_set_1) - std::begin(edge_set_1)));
        // Sort vertices by ID in builder
        builder.sort_vertices([](const InputVertex& l, const InputVertex& r)->bool 
                                {return l.id < r.id; });
        // Fill up the graph from builder
        EDU_EXPECT_TRUE((VGraphAdapter<_Graph, _Builder>(graph, builder)()));
    }
    // Verify the graph
    const size_t vertex_count = 19;
    EDU_EXPECT_EQ(graph.size(), vertex_count);
    size_t node_id = 1;
    for (auto it = graph.begin(); it != graph.end(); ++it)
    {
        // Each vertex must be between 1 and vertex_count
        EDU_EXPECT_EQ((*it).id, node_id++);
    }
    // Dump the graph
    logInfo("Graph content: ");
    size_t pos = 0;
    for (auto it = graph.begin(); it != graph.end(); ++it)
    {
        // Dump each vertex
        logInfo_(pos+1, ". Vertex[", pos, "] = {", it->id, ", ", it->name, "} TO -> {");
        auto to_edges = graph.get_vertex_edges(it, true); 
        for( auto it = to_edges.first; it != to_edges.second; ++it)
        {
            logInfo_("V: ", graph.get_vertex(it->first)->id, "=" , it->second.cost , "; ");
        }
        logInfo_("} FROM -> {");
        auto from_edges = graph.get_vertex_edges(it, false);
        for (auto it = from_edges.first; it != from_edges.second; ++it)
        {
            logInfo_("V: ", graph.get_vertex(it->first)->id, "=", it->second.cost, "; ");
        }
        logInfo("}");
        pos++;
    }

}

///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("graphs:egraph-simple", BidiGraphEdgeThin,
               "Bi-directional (for forward/backward routing) edge based graph without transition penalties")
{
    // Build graph
    using _Graph = EGraph<EVertex>;
    _Graph graph;

    {
        logInfo("Building the bi-drectional edge based graph without connection costs, excluding forbidden turns");
        logInfo("Input data:");
        std::for_each(std::begin(edge_set_1), std::end(edge_set_1), [](const auto& edge)
        {
            logInfo("id:", edge.id, ", name:", edge.name,
                ", R:", edge.startNode, ", NR:", edge.endNode,
                ", Fw:", edge.costFw, ", Bw:", edge.costBw);
        });
        using _Builder = GraphBuilder<InputVertex, InputEdge>;
        _Builder builder;
        // Push edge set
        EDU_EXPECT_EQ(builder.push_edges(std::begin(edge_set_1), std::end(edge_set_1), true, error_observer()),
            (std::end(edge_set_1) - std::begin(edge_set_1)));
        // Sort vertices by ID in builder
        builder.sort_vertices([](const InputVertex& l, const InputVertex& r)->bool
        {return l.id < r.id; });
        // Fill up the graph from builder
        EDU_EXPECT_TRUE((EGraphAdapter<_Graph, _Builder>(graph, builder)()));
    }
    // Verify the graph
    const size_t vertex_count = 28;
    EDU_EXPECT_EQ(graph.size(), vertex_count);
    size_t node_id = 1;
    for (auto it = graph.begin(); it != graph.end(); ++it)
    {
        // Each vertex must be between 1 and vertex_count
        EDU_EXPECT_EQ((*it).id, node_id++);
    }
    // Dump the graph
    logInfo("Graph content: ");
    size_t pos = 0;
    for (auto it = graph.begin(); it != graph.end(); ++it)
    {
        // Dump each vertex
        logInfo_(pos+1, ". Edge[", pos, "] = {", it->id, ", ", it->name, "} FORWARD -> {");
        auto to_edges = graph.get_vertex_edges(it, true);
        for (auto it_ = to_edges.first; it_ != to_edges.second; ++it_)
        {
            logInfo_("E: ", (it_->second ? "<Fw>" : "<Bw>"), graph.get_vertex(it_->first)->id, "=", it->costFw, "; ");
        }
        logInfo_("} BACKWARD -> {");
        auto from_edges = graph.get_vertex_edges(it, false);
        for (auto it_ = from_edges.first; it_ != from_edges.second; ++it_)
        {
            logInfo_("E: ", (it_->second ? "<Fw>" : "<Bw>"), graph.get_vertex(it_->first)->id, "=", it->costBw, "; ");
        }
        logInfo("}");
        pos++;
    }

}


///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("graphs:egraph-penalties", BidiGraphEdgeThick,
               "Bi-directional (for forward/backward routing) edge based graph with transition penalties")
{
    // Build graph
    using _Graph = EGraph<EVertex, EEdge>;
    _Graph graph;

    {
        logInfo("Building the bi-drectional edge based graph, having connection costs, including forbidden turns also");
        logInfo("Input data:");
        std::for_each(std::begin(edge_set_1), std::end(edge_set_1), [](const auto& edge)
        {
            logInfo("id:", edge.id, ", name:", edge.name,
                ", R:", edge.startNode, ", NR:", edge.endNode,
                ", Fw:", edge.costFw, ", Bw:", edge.costBw);
        });
        using _Builder = GraphBuilder<InputVertex, InputEdge>;
        _Builder builder;
        // Push edge set
        EDU_EXPECT_EQ(builder.push_edges(std::begin(edge_set_1), std::end(edge_set_1), true, error_observer()),
            (std::end(edge_set_1) - std::begin(edge_set_1)));
        // Sort vertices by ID in builder
        builder.sort_vertices([](const InputVertex& l, const InputVertex& r)->bool
        {return l.id < r.id; });
        // Fill up the graph from builder
        EDU_EXPECT_TRUE((EGraphAdapter<_Graph, _Builder>(graph, builder, false, penalty_supplier())()));
    }
    // Verify the graph
    const size_t vertex_count = 28;
    EDU_EXPECT_EQ(graph.size(), vertex_count);
    size_t node_id = 1;
    for (auto it = graph.begin(); it != graph.end(); ++it)
    {
        // Each vertex must be between 1 and vertex_count
        EDU_EXPECT_EQ((*it).id, node_id++);
    }
    // Dump the graph
    logInfo("Graph content: ");
    size_t pos = 0;
    for (auto it = graph.begin(); it != graph.end(); ++it)
    {
        // Dump each vertex
        logInfo_(pos+1, ". Edge[", pos, "] = {", it->id, ", ", it->name, "} FORWARD -> {");
        auto to_edges = graph.get_vertex_edges(it, true);
        for (auto it_ = to_edges.first; it_ != to_edges.second; ++it_)
        {
            logInfo_("E: ", (it_->first.second ? "<Fw>" : "<Bw>"), graph.get_vertex(it_->first.first)->id, "=", it->costFw, "(Fw:", it_->second.first, "-Bw:", it_->second.second, "); ");
        }
        logInfo_("} BACKWARD -> {");
        auto from_edges = graph.get_vertex_edges(it, false);
        for (auto it_ = from_edges.first; it_ != from_edges.second; ++it_)
        {
            logInfo_("E: ", (it_->first.second ? "<Fw>" : "<Bw>"), graph.get_vertex(it_->first.first)->id, "=", it->costBw, "(Fw:", it_->second.first, "-Bw:", it_->second.second, "); ");
        }
        logInfo("}");
        pos++;
    }
}


}
}
