///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding Graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.26.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/task.h>
#include <edu/graphs/vgraph.h>
#include <edu/graphs/egraph.h>
#include <edu/graphs/graph_builder.h>
#include <edu/graphs/routing.h>
#include <edu/graphs/dfs.h>
#include <edu/graphs/bfs.h>
#include <edu/graphs/dijkstra.h>
#include <iterator>
#include <tuple>
#include <vector>
#include <list>
#include "graph_data.h"
#include "vgraph_adapter.h"
#include "egraph_adapter.h"
#include "erouting_adapter.h"
#include <edu/log.h>
#include <assert.h>

namespace edu
{
namespace graph
{

namespace
{
    // Special compare functor for routes - since transition costs are placed differently on edges by FW/BW routing 
    // by FW routing transition point (with cost) is on parent vertex (transition cost A->B stored in cost of A)
    // by BW routing transition point (with cost) is on connected vertex (transition cost A->B stored in cost of B)
    struct path_compare
    {
        bool operator()(std::list<std::pair<ERouteVertex, ERouteCost>>& l,
                        std::list<std::pair<ERouteVertex, ERouteCost>>& r)
        {
            bool res = false;
            auto it_l = std::begin(l);
            auto it_r = std::begin(r);
            ERouteCost cost_l = 0;
            ERouteCost cost_r = 0;
            while (it_l != std::end(l) && it_r != std::end(r))
            {
                if (!((it_l->first.id == it_r->first.id) && (it_l->first.fw == it_r->first.fw)))
                {
                    // routes are not identical
                    break;
                }
                // accumulate the path cost
                cost_l += it_l->second;
                cost_r += it_r->second;
                ++it_l;
                ++it_r;
            }
            if ((it_l == std::end(l)) && (it_r == std::end(r)) && (cost_l == cost_r))
            {
                // when length, or total cost, or any of vertex are not equal -> routes resulted not identical
                res = true;
            }
            return res;
        }
    };
}

///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("graphs:erouting", ERouting, 
               "Routing on edge based graph, having transition penalties, including forbidden turns")
{
    // Bidirectional graph
    using _Graph = EGraph<EVertex, EEdge>;
    _Graph graph;

    // Test case structure: 
    // list of start points, list of end points, U-turn penalty, forbidden path penalty, include turn penalties,
    // algorithms = combination of: ( 0 = none; 1 = DSF; 2 = BSF; 4 = Dijkstra; 8 = DSF-first; 16 = BSF-first) 
    // direction = combination of: (0 = none; 1 = forward; 2 = backward; 4 = bidirectional)
    std::vector<std::tuple<
        std::vector<std::pair<ERouteVertex, size_t>>,
        std::vector<std::pair<ERouteVertex, size_t>>,
        int32_t, int32_t, bool, uint8_t, uint8_t>>
    // Test casex for routing, using various scenarios 
    test_cases =
    {
        // start/end edges {E-1: r-side 20%}, {E-15 l-side 80%}, U-turn = free, forbidden path = 100000, no turn penalties - all algo and directions
        { { { { 0,  true }, 20 } }, { { { 14, false }, 80 } } , 0, 10000, false, 0xFF, 0xFF },
        // start/end edges {E-1: r-side 20%}, {E-15 l-side 80%}, U-turn = free, forbidden path = 100000, turn penalties = YES - all algo and directions
        { { { { 0,  true }, 20 } }, { { { 14, false }, 80 } } , 0, 10000, true, 0xFF, 0xFF },
        // start/end edges {E-1: r-side 20%}, {E-15 l-side 80%}, U-turn = 4, forbidden path = 100000, no turn penalties - all algo and directions
        { { { { 0,  true }, 20 } }, { { { 14, false }, 80 } } , 4, 10000, false, 0xFF, 0xFF },
        // start/end edges {E-1: r-side 20%}, {E-15 l-side 80%}, U-turn = 4, forbidden path = 100000, turn penalties  = YES - all algo and directions
        { { { { 0,  true }, 20 } },{ { { 14, false }, 80 } } , 4, 10000, true, 0xFF, 0xFF },

        // Same route with way-points on the same edge
        // start/end edges {E-15: r-side 20%}, {E-15 r-side 80%}, U-turn = disabled, forbidden path = disabled, no turn penalties - all algo and directions
        { { { { 14,  true }, 20 } },{ { { 14, true }, 80 } },  0, 10000, false , 0xFF, 0xFF },
        // start/end edges {E-15: r-side 80%}, {E-15 r-side 20%}, U-turn = disabled, forbidden path = disabled, no turn penalties - all algo and directions
        { { { { 14,  true }, 80 } },{ { { 14, true }, 20 } },  0, 10000, false , 0xFF, 0xFF },

        // start/end edges {E-1: r-side 0%}, {E-2 r-side 0%}, U-turn = free, forbidden path = 100000, no turn penalties - all algo and directions
        { { { { 0,  true }, 0 } },{ { { 1, true }, 0 } } , 0, 10000, false, 0xFF, 0xFF },
        // start/end edges {E-1: r-side 100%}, {E-2 r-side 100%}, U-turn = free, forbidden path = 100000, no turn penalties - all algo and directions
        { { { { 0,  true }, 100 } },{ { { 1, true }, 100 } } , 0, 10000, false, 0xFF, 0xFF },
        // start/end edges {E-1: r-side 100%}, {E-2 r-side 0%}, U-turn = free, forbidden path = 100000, no turn penalties - all algo and directions
        { { { { 0,  true }, 100 } },{ { { 1, true }, 0 } } , 0, 10000, false, 0xFF, 0xFF },

        // Various samples with multiple way-points - the best matched route will be returned
        // start/end edges {E-1: r-side 20%}, {E-15 l-side 80%}, U-turn = disabled, forbidden path = disabled, no turn penalties - all algo and directions
        { { { { 0,  true }, 20 } }, { { { 14, false }, 80 } } ,  std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max(), false , 0xFF, 0xFF },
        // start/end edges {E-1: r-side 20%, E-12: l-side 60%}, {E-15 l-side 80%}, U-turn = free, forbidden path = 10000, no turn penalties - all algo and directions
        { { { { 0,  true }, 20 } , { { 11, false }, 60 } }, { { { 14, false }, 80 } } ,  0, 10000, false , 0xFF, 0xFF },
        // start/end edges {E-1: r-side 20%, E-12: l-side 60%}, {E-15 l-side 80% E22 l-side 45%}, U-turn = free, forbidden path = 10000, no turn penalties - all algo and directions
        { { { { 0,  true }, 20 } ,{ { 11, false }, 60 } }, { { { 14, false }, 80 }, { { 21, false }, 45 } } ,  0, 10000, false , 0xFF, 0xFF },
        // start/end edges {E-1: r-side 60% E-5: l-side 45%}, {E-27 l-side 40%, E-21 r-side 70%}, U-turn = 0, forbidden path = 100000, no turn penalties - all algo and directions
        { { { { 0,  true }, 10 },{ { 4,  false }, 45 } },{ { { 27, false }, 40 },{ { 20, true }, 70 } } , 0, 10000, false, 0x07, 0xFF },

    };
    // Use or not penalties (do not change it here)
    bool penalty = false;

    // Flag to enable/disable noisy routing (logging the full path search)
    const bool noisy_routing = false;
    for (auto test_case = std::begin(test_cases); test_case != std::end(test_cases); ++test_case)
    {
        // Reference route (do not change it here)
        bool ref_path_valid = false;
        std::list<std::pair<ERouteVertex, ERouteCost>> ref_path;

        // need re-fill the graph if want to add/remove penalties, or first use
        if (penalty != std::get<4>(*test_case) || (test_case == std::begin(test_cases)))
        {
            graph.clear();
            // Store penalty option for next run compare
            penalty = std::get<4>(*test_case);
            // (Re)build graph
            using _Builder = GraphBuilder<InputVertex, InputEdge>;
            _Builder builder;
            // Push edge set
            EDU_EXPECT_EQ(builder.push_edges(std::begin(edge_set_1), std::end(edge_set_1), true, error_observer()),
                (std::end(edge_set_1) - std::begin(edge_set_1)));
            // Sort vertices by ID in builder
            builder.sort_vertices([](const InputVertex& l, const InputVertex& r)->bool
                                    {return l.id < r.id; });
            // Build with penalties
            EDU_EXPECT_TRUE(penalty 
                ? (EGraphAdapter<_Graph, _Builder> (graph, builder, false, penalty_supplier())())
                : (EGraphAdapter<_Graph, _Builder>(graph, builder, false)()));
        }
        // Initialize routing adapter, setting up U-Turn costs to zero
        ERoutingAdapter<_Graph> adapter(graph,
                                        std::begin(std::get<0>(*test_case)), std::end(std::get<0>(*test_case)),
                                        std::begin(std::get<1>(*test_case)), std::end(std::get<1>(*test_case)), 
                                         std::get<2>(*test_case), std::get<3>(*test_case), noisy_routing);
        // Announce test case
        logInfo("Start route calculation for: ");
        logInfo_("  - Start points: ( ");
        for (auto w = std::begin(std::get<0>(*test_case)); w != std::end(std::get<0>(*test_case)); ++w)
        {
            logInfo_("{ ", graph.get_vertex(w->first.id)->name, ":", w->first.fw ? "r-side:" : "l-side:", w->second, "% }  ");
        }
        logInfo(" )");
        logInfo_("  - End points: ( ");
        for (auto w = std::begin(std::get<1>(*test_case)); w != std::end(std::get<1>(*test_case)); ++w)
        {
            logInfo_("{ ", graph.get_vertex(w->first.id)->name, ":", w->first.fw ? "r-side:" : "l-side:", w->second, "% }  ");
        }
        logInfo(" )");
        logInfo("   - U-Turn=", std::get<2>(*test_case), " Forbidden cost=", std::get<3>(*test_case), " Use turn penalties=", std::get<4>(*test_case) ? "Yes" : "No");
        logInfo("");
        if ( (std::get<5>(*test_case) & 0x01) && (std::get<6>(*test_case) & 0x01) )
        {
            logInfo(" - Routing with DFS-forward ...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::forward, Dfs, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE( router(make_route_logger(adapter)) );
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::front_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            if (ref_path_valid)
            {
                EDU_EXPECT_TRUE(path_compare()(ref_path, path));
            }
            else
            {
                ref_path.swap(path);
                ref_path_valid = true;
            }
            logInfo("");
        }
        if ( (std::get<5>(*test_case) & 0x01) && (std::get<6>(*test_case) & 0x02) )
        {
            logInfo(" - Routing with DFS-backward ...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::backward, Dfs, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::back_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            if (ref_path_valid)
            {
                EDU_EXPECT_TRUE(path_compare()(ref_path, path));
            }
            else
            {
                ref_path.swap(path);
                ref_path_valid = true;
            }
            logInfo("");
        }
        if ( (std::get<5>(*test_case) & 0x01) && (std::get<6>(*test_case) & 0x04) )
        {
            logInfo(" - Routing with DFS-bidirectional ...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::bidirectional, Dfs, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::front_inserter(path), std::back_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            if (ref_path_valid)
            {
                EDU_EXPECT_TRUE(path_compare()(ref_path, path));
            }
            else
            {
                ref_path.swap(path);
                ref_path_valid = true;
            }
            logInfo("");
        }

        if ( (std::get<5>(*test_case) & 0x02) && (std::get<6>(*test_case) & 0x01) )
        {
            logInfo(" - Routing with BFS-forward ...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::forward, Bfs, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::front_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            if (ref_path_valid)
            {
                EDU_EXPECT_TRUE(path_compare()(ref_path, path));
            }
            else
            {
                ref_path.swap(path);
                ref_path_valid = true;
            }
            logInfo("");
        }
        if ( (std::get<5>(*test_case) & 0x02) && (std::get<6>(*test_case) & 0x02) )
        {
            logInfo(" - Routing with BFS-backward ...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::backward, Bfs, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::back_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            if (ref_path_valid)
            {
                EDU_EXPECT_TRUE(path_compare()(ref_path, path));
            }
            else
            {
                ref_path.swap(path);
                ref_path_valid = true;
            }
            logInfo("");
        }
        if ( (std::get<5>(*test_case) & 0x02) && (std::get<6>(*test_case) & 0x04) )
        {
            logInfo(" - Routing with BFS-bidirectional ...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::bidirectional, Bfs, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::front_inserter(path), std::back_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            if (ref_path_valid)
            {
                EDU_EXPECT_TRUE(path_compare()(ref_path, path));
            }
            else
            {
                ref_path.swap(path);
                ref_path_valid = true;
            }
            logInfo("");
        }
        
        if ( (std::get<5>(*test_case) & 0x04) && (std::get<6>(*test_case) & 0x01) )
        {
            logInfo(" - Routing with Dijkstra-forward ...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::forward, Dijkstra, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::front_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            if (ref_path_valid)
            {
                EDU_EXPECT_TRUE(path_compare()(ref_path, path));
            }
            else
            {
                ref_path.swap(path);
                ref_path_valid = true;
            }
            logInfo("");
        }
        if ( (std::get<5>(*test_case) & 0x04) && (std::get<6>(*test_case) & 0x02) )
        {
            logInfo(" - Routing with Dijkstra-backward ...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::backward, Dijkstra, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::back_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            if (ref_path_valid)
            {
                EDU_EXPECT_TRUE(path_compare()(ref_path, path));
            }
            else
            {
                ref_path.swap(path);
                ref_path_valid = true;
            }
            logInfo("");
        }
        if ( (std::get<5>(*test_case) & 0x04) && (std::get<6>(*test_case) & 0x04) )
        {
            logInfo(" - Routing with Dijkstra-bidirectional ...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::bidirectional, Dijkstra, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::front_inserter(path), std::back_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            if (ref_path_valid)
            {
                EDU_EXPECT_TRUE(path_compare()(ref_path, path));
            }
            else
            {
                ref_path.swap(path);
                ref_path_valid = true;
            }
            logInfo("");
        }

        if ( (std::get<5>(*test_case) & 0x08) && (std::get<6>(*test_case) & 0x01) )
        {
            logInfo(" - Routing with DFS-First-forward (first route, not the optimal one)...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::forward, DfsFirst, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::front_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            logInfo("");
        }
        if ( (std::get<5>(*test_case) & 0x08) && (std::get<6>(*test_case) & 0x02) )
        {
            logInfo(" - Routing with DFS-First-backward (first route, not the optimal one)...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::backward, DfsFirst, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::back_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            logInfo("");
        }
        if ( (std::get<5>(*test_case) & 0x08) && (std::get<6>(*test_case) & 0x04) )
        {
            logInfo(" - Routing with DFS-First-bidirectional (first route, not the optimal one)...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::bidirectional, DfsFirst, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::front_inserter(path), std::back_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            logInfo("");
        }

        if ( (std::get<5>(*test_case) & 0x10) && (std::get<6>(*test_case) & 0x01) )
        {
            logInfo(" - Routing with BFS-First-forward (first route, not the optimal one)...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::forward, BfsFirst, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::front_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            logInfo("");
        }
        if ( (std::get<5>(*test_case) & 0x10) && (std::get<6>(*test_case) & 0x02) )
        {
            logInfo(" - Routing with BFS-First-backward (first route, not the optimal one)...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::backward, BfsFirst, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::back_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            logInfo("");
        }
        if ( (std::get<5>(*test_case) & 0x10) && (std::get<6>(*test_case) & 0x04) )
        {
            logInfo(" - Routing with BFS-First-bidirectional (first route, not the optimal one)...");
            // Router engine
            using _Router = Routing<ERoutingAdapter<_Graph>, ERouteVertex, ERouteCost, 
                                    RoutingMode::bidirectional, BfsFirst, vertex_hash>;
            _Router router(adapter);
            // Launch a route calculation
            EDU_EXPECT_TRUE(router(make_route_logger(adapter)));
            // Dump the calculated the route
            // - first extract the route
            std::list<std::pair<ERouteVertex, ERouteCost>> path;
            EDU_EXPECT_TRUE(router.pull(std::front_inserter(path), std::back_inserter(path)));
            logInfo("Visited vertices: ", router.visiteds());
            // - ask adapter to dump the route as graph edge sequences    
            adapter.dump_route(std::begin(path), std::end(path));
            adapter.convert_route(std::begin(path), std::end(path));
            logInfo("");
        }

        logInfo("");
    }   
}


}
}
