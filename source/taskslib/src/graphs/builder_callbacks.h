///////////////////////////////////////////////////////////////////////////////////////////////////
// Callback classes and functors for graph builder
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_GRAPHS_BUILDER_CALLBACKS_H__
#define __TASKS_GRAPHS_BUILDER_CALLBACKS_H__

#include <edu/graphs/graph_builder.h>
#include "graph_data.h"
#include <string>
#include <list>
#include <vector>


namespace edu
{
namespace graph
{

    
namespace
{

    /** \brief Local functor for receving builder notifications by pushing edges
      */    
    template<typename Resolver>
    struct push_callback
    {
        
        /** \brief Constructor
          * \param[in] resolver_ - rejected edge resolver
          */
        push_callback(Resolver resolver_ = 
                      [](const InputEdge&, BuilderMessage)->bool
                      {return false;})
        : resolver(resolver_)
        {
        }

        /** \brief Launcher for callback
          * \param[in] edge - edge reported as rejected by the builder
          * \param[in] msg - reason of reject
          * \returns true - when push need to be aborted by builder, otherwise false
          */
        bool operator()(const InputEdge& edge, 
                        BuilderMessage msg)
        {
            const std::string err[] 
            {
                "unknown",
                "Vertex already exists!",
                "Edge already exists!",
                "Edge is a loop!",
                "Could not find vertex!",
                "Connection is missing!"
            };
            
            if (BuilderMessage::none != msg)
            {
                logWarning("Could not insert edge: {id:", edge.id, ", name:", edge.name, "} due to:", 
                         err[static_cast<int32_t>(msg)]);
            }
            // Try to resolve the issue
            return !resolver(edge, msg);
        }

    private:
        // Edge resolver instance
        Resolver resolver;
    };
    
    
    /** \brief Abstract functor for resolving rejected edges
      */
    template<typename InsertIterator, BuilderMessage msg>
    struct _rejected_edge_resolver_ {};

    /** \brief Specialized functor for resolving "edge exists"
      */
    template<typename InsertIterator>
    struct _rejected_edge_resolver_<InsertIterator, BuilderMessage::edge_exists>
    {
        /** \brief Launcher for specialized resolver
          * \param[in] edge - edge reported as rejected by the builder
          * \returns true - when edge could be solved, or ignored, otherwise false
          */
        bool operator()(const InputEdge& edge)
        {
            // Stop the insertion, when edge id = 5 is reported as failed
            return (5 != edge.id);
        }
    };
    
    /** \brief Specialized functor for resolving "edge is loop"
      */
    template<typename InsertIterator>
    struct _rejected_edge_resolver_<InsertIterator, BuilderMessage::edge_is_loop>
    {
        /** \brief Launcher for specialized resolver
          * \param[in] edge - edge reported as rejected by the builder
          * \param[in] enqueuer - insertion iterator to enqueue resolved edges for re-insertion
          * \param[in] max_edge_id - upper bound of unique edge identifers
          * \param[in] max_node_id - upper bound of unique node (vertex) identifers
          * \returns true - when edge could be solved, or ignored, otherwise false
          */
        bool operator()(const InputEdge& edge,
                        InsertIterator enqueuer, 
                        size_t& max_edge_id,
                        size_t& max_node_id)
        {
            logWarning("Loop edge is rejected, need to split it: Edge {id:", 
                        edge.id, ", name:", edge.name, 
                        ", R:", edge.startNode, ", NR:", edge.endNode, "} into:");
            // Create 2+1 new edges
            InputEdge edge_1(edge);
            InputEdge edge_2(edge);
            InputEdge edge_3(edge);
            // New Id-s needed for 2 edges
            edge_1.id = ++max_edge_id;
            edge_3.id = ++max_edge_id;
            // Now connect them Edge_1 -> Edge 2 -> Edge 3
            edge_2.startNode = ++max_node_id;
            edge_2.endNode = ++max_node_id;
            edge_1.endNode = edge_2.startNode;
            edge_3.startNode = edge_2.endNode;
            // Resolve edge costs
            edge_1.costFw = edge.costFw / 3;
            edge_1.costBw = edge.costBw / 3;
            edge_3.costFw = edge.costFw / 3;
            edge_3.costBw = edge.costBw / 3;
            edge_2.costFw = edge.costFw - edge_1.costFw - edge_3.costFw;
            edge_2.costBw = edge.costBw - edge_1.costBw - edge_3.costBw;
            // Log split info
            logInfo(" 1. - Edge {id:", edge_1.id, ", name:", edge_1.name, ", RV:", edge_1.startNode, ", NRV:", edge_1.endNode, "} :");
            logInfo(" 2. - Edge {id:", edge_2.id, ", name:", edge_2.name, ", RV:", edge_2.startNode, ", NRV:", edge_2.endNode, "} :");
            logInfo(" 3. - Edge {id:", edge_3.id, ", name:", edge_3.name, ", RV:", edge_3.startNode, ", NRV:", edge_3.endNode, "} :");
            // Enqeue new edges for re-insertion
            *enqueuer = edge_1;
            *enqueuer = edge_2;
            *enqueuer = edge_3;
            // Issue solved
            return true;
            return true;
        }
    };
    
    /** \brief Local functor for edge resolving
      */
    template<typename InsertIterator>
    struct rejected_edge_resolver
    {
        /** \brief Constructor
          * \param[in] enqueuer_ - insertion iterator to enqueue resolved edges for re-insertion
          * \param[in] max_edge_id_ - upper bound of unique edge identifers
          * \param[in] max_node_id_ - upper bound of unique node (vertex) identifers
          */
        rejected_edge_resolver(InsertIterator enqueuer_,
                               size_t& max_edge_id_,
                               size_t& max_node_id_)
        : enqueuer(enqueuer_)
        , max_edge_id(max_edge_id_)
        , max_node_id(max_node_id_)
        {
        }
        
        /** \brief Launcher for resolver
          * \param[in] edge - edge reported as rejected by the builder
          * \param[in] msg - builder message, telling the reason
          * \returns true - when edge could be solved, or ignored, otherwise false
          */
        bool operator()(const InputEdge& edge,
                        BuilderMessage msg)
        {
            bool res = false;
            if (BuilderMessage::edge_exists == msg)
            {
                res = _rejected_edge_resolver_<InsertIterator, BuilderMessage::edge_exists>()
                        (edge);
            }
            if (BuilderMessage::edge_is_loop == msg)
            {
                res = _rejected_edge_resolver_<InsertIterator, BuilderMessage::edge_is_loop>()
                        (edge, enqueuer, max_edge_id, max_node_id);
            }
            return res;
        }
        
    private:
        InsertIterator  enqueuer;
        size_t&         max_edge_id;
        size_t&         max_node_id;
    };  
    
}
 
 
}
} 

#endif //__TASKS_GRAPHS_BUILDER_CALLBACKS_H__
