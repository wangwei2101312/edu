///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding Graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.26.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/task.h>
#include <edu/graphs/vgraph.h>
#include <edu/graphs/graph_builder.h>
#include "graph_data.h"
#include "vgraph_adapter.h"
#include "egraph_adapter.h"
#include <edu/log.h>
#include <assert.h>

namespace edu
{
namespace graph
{

  
///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("graphs:vgraph-uni", UniGraph, 
               "Uni-directional (for forward routing only) vertex based graph")
{
    // Uni-directional graph
    using _Graph = VGraph<VVertex, VEdge, true>;
    _Graph graph;

    // Build graph
    {
        logInfo("Building the uni-directional vertex based graph");
        logInfo("Input data:");
        std::for_each(std::begin(edge_set_1), std::end(edge_set_1), [](const auto& edge)
        {
            logInfo("id:", edge.id, ", name:", edge.name,
                    ", R:", edge.startNode, ", NR:", edge.endNode,
                    ", Fw:", edge.costFw, ", Bw:", edge.costBw);
        });
         using _Builder = GraphBuilder<InputVertex, InputEdge>;
        _Builder builder;
        // Push edge set
        EDU_EXPECT_EQ(builder.push_edges(std::begin(edge_set_1), std::end(edge_set_1), true, error_observer()),
                          (std::end(edge_set_1) - std::begin(edge_set_1)));
        // Sort vertices by ID in builder
        builder.sort_vertices([](const InputVertex& l, const InputVertex& r)->bool 
                                {return l.id < r.id; });
        // Fill up the graph from builder
        EDU_EXPECT_TRUE((VGraphAdapter<_Graph, _Builder>(graph, builder)()));
    }
    // Verify the graph
    const size_t vertex_count = 19;
    EDU_EXPECT_EQ(graph.size(), vertex_count);
    size_t node_id = 1;
    for (auto it = graph.begin(); it != graph.end(); ++it)
    {
        // Each vertex must be between 1 and vertex_count
        EDU_EXPECT_EQ((*it).id, node_id++);
    }
    // Dump the graph
    logInfo("Graph content: ");
    size_t pos = 0;
    for (auto it = graph.begin(); it != graph.end(); ++it)
    {
        // Dump each vertex
        logInfo_(pos + 1, ". Vertex[", pos, "] = {", it->id, ", ", it->name, "} TO -> {");
        auto to_edges = graph.get_vertex_edges(it); 
        for( auto it = to_edges.first; it != to_edges.second; ++it)
        {
            logInfo_("TV:", graph.get_vertex(it->first)->id, "=" , it->second.cost , "; ");
        }
        pos++;
        logInfo("}");
    }
}


}
}
