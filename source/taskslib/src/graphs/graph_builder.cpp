///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding Graph builder 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/task.h>
#include <edu/graphs/graph_builder.h>
#include "builder_callbacks.h"
#include "builder_adapter.h"
#include <edu/log.h>
#include <deque>
#include <vector>
#include <sstream>
#include <iterator>


namespace edu
{
namespace graph
{

namespace
{
    /** \brief Sets of test data
      */
    const InputEdge _edge_set_1[] =
    {
        {  1, "E-1",   1,  2,  7, 11 },
        {  2, "E-2",   2,  4,  8,  3 },
        {  3, "E-3",   2,  3,  2,  4 },
        {  4, "E-4",   4,  7, 11,  5 },
        {  5, "E-5",   3,  5, 11,  5 },
        {  6, "E-6",   3,  7,  2,  6 },
        {  7, "E-7",   5,  6, 12,  6 },
        {  8, "E-8",   6,  7,  9,  4 },
        {  9, "E-9",   7, 10,  3,  5 },
        { 10, "E-10",  7, 11,  5,  6 },
        { 11, "E-11", 10, 11, 23, 14 },
        { 12, "E-12", 10, 14, 12,  8 }
    };
    const InputEdge _edge_set_2[] =
    {
        { 99, "E-X",  10, 10, 10, 20 },
        { 13, "E-13", 14, 15, 14, 15 },
        { 14, "E-14", 11, 15,  6,  5 },
        { 15, "E-15", 11, 17,  7,  8 },
        { 4,  "E-4",   4,  7,  1,  2 },
        { 16, "E-16", 11, 12,  5,  5 },
        { 5,  "E-5",   3,  5,  1,  2 },
        { 17, "E-17",  7,  9,  6,  7 },
        { 18, "E-18",  8,  9,  8,  8 }
    };
    const InputEdge _edge_set_3[] =
    {
        {  1, "E-1",   1,  2,  7, 11 },
        {  2, "E-2",   2,  4,  8,  3 },
        {  3, "E-3",   2,  3,  2,  4 },
        { 17, "E-17",  7,  9,  6,  7 },
        { 18, "E-18",  8,  9,  8,  8 },
        { 19, "E-19",  6,  8,  3,  0 },
        { 20, "E-20",  5, 18, 41,  0 },
        { 21, "E-21", 18, 19, 34,  0 },
        { 22, "E-22", 13, 19,  5,  6 },
        { 23, "E-23",  9, 13,  0,  4 },
        { 24, "E-24", 12, 13,  6,  0 },
        { 25, "E-25", 16, 17,  8,  9 },
        { 26, "E-26", 15, 16,  3,  4 },
        { 27, "E-27", 14, 16,  3,  0 },
        { 28, "E-28",  5,  8,  7,  2 }
    };


}

///////////////////////////////////////////////////////////////////////////////////////////////////
EDU_TASK_SUITE("graphs:graph-builder", GraphBuilder,
               "Generic graph builder sample to collect, build edges and vertices for graphs")
{
    {
        GraphBuilder<InputVertex, InputEdge> builder;
        // Container for collecting resolved edges
        std::deque<InputEdge> resolved_edges;
        size_t max_vertex_id = 1000;
        size_t max_edge_id = 1000;

        // Add one-by-one 12 edges and 10 vertices
        logInfo("Pushing edges from data set 1 ...");
        std::for_each(std::begin(_edge_set_1), std::end(_edge_set_1), [&builder, this](const InputEdge& edge)
        {
            EDU_EXPECT_TRUE(builder.push_edge(edge));
        });
        EDU_EXPECT_EQ(builder.edges(), 12);
        EDU_EXPECT_EQ(builder.vertices(), 10);

        // Add by range 4 edges and 3 vertices - we stop when edge Id = 5 is duplicated
        logInfo("Pushing edges from data set 2 ...");
        // Declare rejected eject resolver
        rejected_edge_resolver<decltype(std::back_inserter(resolved_edges))>
            resolver(std::back_inserter(resolved_edges),
                max_edge_id,
                max_vertex_id);
        // Push a range    
        EDU_EXPECT_EQ(builder.push_edges(std::begin(_edge_set_2),
            std::end(_edge_set_2),
            true,
            push_callback<decltype(resolver)>(resolver)), 4);
        EDU_EXPECT_EQ(builder.edges(), 16);
        EDU_EXPECT_EQ(builder.vertices(), 13);

        // Add by range 9 edges and 6 vertices - we don't want notification for dupplicates
        logInfo("Pushing edges from data set 3 ...");
        EDU_EXPECT_EQ(builder.push_edges(std::begin(_edge_set_3),
            std::end(_edge_set_3)), 12);
        EDU_EXPECT_EQ(builder.edges(), 28);
        EDU_EXPECT_EQ(builder.vertices(), 19);

        // Add the resolved edges
        logInfo("Pushing resolved edges (earlier rejected) ...");
        EDU_EXPECT_EQ(builder.push_edges(std::begin(resolved_edges),
            std::end(resolved_edges)), 3);
        EDU_EXPECT_EQ(builder.edges(), 31);
        EDU_EXPECT_EQ(builder.vertices(), 21);
        // Test edge pull
        EDU_EXPECT_TRUE(    (builder.get_edge(InputEdge{ 4 }) != builder.end())
                           && (builder.get_edge(InputEdge{ 4 })->first.id == 4));
        EDU_EXPECT_TRUE(builder.get_edge(InputEdge{ 444 }) == builder.end());
        // Test vertex pull
        builder.push_vertex(InputVertex{ 998 });
        {
            InputVertex vertices[] = { 4, 7, 999 };
            builder.push_vertices(std::begin(vertices), std::end(vertices));
        }
        EDU_EXPECT_TRUE(    (builder.get_vertex(InputVertex{ 7 }) != builder.vend())
                           && (builder.get_vertex(InputVertex{ 7 })->first.id == 7));
        EDU_EXPECT_TRUE(    (builder.get_vertex(InputVertex{ 998 }) != builder.vend())
                           && (builder.get_vertex(InputVertex{ 998 })->first.id == 998));
        EDU_EXPECT_TRUE(    (builder.get_vertex(InputVertex{ 999 }) != builder.vend())
                           && (builder.get_vertex(InputVertex{ 999 })->first.id == 999));
        EDU_EXPECT_TRUE(builder.get_vertex(InputVertex{ 444 }) == builder.vend());

        /// Sort builder entities
        builder.sort_edges([](const InputEdge& l, const InputEdge& r)->bool {return l.id < r.id; });
        builder.sort_vertices([](const InputVertex& l, const InputVertex& r)->bool {return l.id < r.id; });

        // Pull out some information
        std::vector<InputVertex> vertices(builder.vertices());
        std::vector<InputEdge> edges(builder.edges());

        BuilderAdapter adapter(vertices, edges);
        // Declare adapter for builder pulls
        // Pull raw vertices
        logInfo("Pulling indexed vertices from builder ...");
        EDU_EXPECT_TRUE(builder.pull_vertices(adapter));
        logInfo("");
        // Pull raw edges
        logInfo("Pulling indexed edges from builder ...");
        EDU_EXPECT_TRUE(builder.pull_edges(adapter));
        logInfo("");
        // Pull connected vertices
        logInfo("Pulling connected vertex graph from builder ...");
        adapter.pull_vertex_graph = true;
        adapter.graph_pulls = 0;
        EDU_EXPECT_TRUE(builder.pull_connected_graph(adapter, true));
        logInfo("");
        // Pull connected edges
        logInfo("Pulling connected edges graph from builder ...");
        adapter.pull_vertex_graph = false;
        adapter.graph_pulls = 0;
        EDU_EXPECT_TRUE(builder.pull_connected_graph(adapter, false));
        logInfo("");
    }
}


}
}
