///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding Graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.08.07.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_GRAPHS_EROUTING_ADAPTERS_H__
#define __TASKS_GRAPHS_EROUTING_ADAPTERS_H__

#include <limits>
#include <tasks/task.h>
#include <edu/graphs/egraph.h>
#include "graph_data.h"
#include <edu/log.h>
#include <assert.h>

namespace edu
{
namespace graph
{

namespace
{
    /** \brief Structure for edge based routing vertex
      */
    struct ERouteVertex
    {
        // Vertex index
        size_t  id;
        // Direction: forward/backward
        bool    fw;

        bool operator==(const ERouteVertex& other) const
        {
            return (id == other.id && fw == other.fw);
        }
    };
    
    /** \brief Functor for hashing
      */
    struct vertex_hash
    {
        size_t operator()(const ERouteVertex& v) const
        {
            // for backward identifiers set the most significant bit to 1 in order to avoid identical hashes
            return std::hash<size_t>()(v.id |
                (v.fw ? 0 
                      : (std::numeric_limits<decltype(v.id)>::max() ^ (std::numeric_limits<decltype(v.id)>::max() >> 1))));
        }
    };

    /** \brief Structure for route cost
      */
    using ERouteCost = int32_t;
  
    /** \brief Simple route logger for monitorizing path search
      */ 
    template<typename Adapter>
    struct route_logger
    {
        route_logger(const Adapter& adapter_)
        : adapter(adapter_)
        {

        }
        // Notification receiver
        bool operator()(const ERouteVertex& v, 
                        const ERouteVertex& p,
                        const ERouteCost& c,
                        bool fw)
        {
            return adapter(v, p, c, fw);
        }

        const Adapter& adapter;
    };

    /** \brief Function to create route logger for automatic template argument deducation
    */
    template<typename Adapter>
    route_logger<Adapter> make_route_logger(Adapter& adapter)
    {
        return route_logger<Adapter>(adapter);
    }
        
    
    /** \brief Adapter to connect graph content to routing algorithm, using "edge" type vertices (a vertex is a directed edge)
      * The routing adapter is fully implementation (scope) specific. 
      * It is a key component to can keep the graph and routing algorithms abstract, but to can obtain fully customized results. 
      * There is the job of adapter to provide dynamic traversal/turn costs (e.g. traffic jams, turn penalties, A* complementary costs etc). 
      * In order to allow routing between isolated areas, the adapter can force the routing to work for forbidden paths also, adding a specific cost for them. 
      * The same with U-turns - adapter can establish whether U-Turns allowed and with, which cost - whether U-turn allowed in connection points only, or directly 
      * (e.g. adding the same vertex with opposite direction by start/end points)
      */
    template<typename Graph>
    class ERoutingAdapter
    {
        // Container structure for fetched vertices {vertex, cost}
        using _Vertices  = std::deque<std::pair<ERouteVertex, ERouteCost>>;
        // Structure for starting/ending vertices: { vertex(id, side), percentage from reference node }
        using _WayPoints = std::deque<std::pair<ERouteVertex, size_t>>;
    public:
        using iterator = _Vertices::iterator;
        using vertices = std::pair<iterator, iterator>;
        // Structure for fetched connections:
        // pair of: { error flag (true = no error), {iterator to first connected vertex, iterator to last connected vertex } }
        using connections = std::pair<bool, vertices>;

        // Invalid vertex identifier
        static const size_t invalid_vertex = std::numeric_limits<size_t>::max();
        // Cost identifier for disabled turn
        static const int32_t disabled_turn = std::numeric_limits<int32_t>::max();
        // Cost identifier for disabled traversal
        static const int32_t disabled_path = 0;

    public:
        /** \brief Constructor
          * \param[in] graph - input graph
          * \param[in] start_first - iterator pointing to the first elements of start vertices
          * \param[in] start_last - iterator pointing to the last elements of start vertices
          * \param[in] end_first - iterator pointing to the first elements of end vertices
          * \param[in] end_last - iterator pointing to the last elements of end vertices
          * \param[in] cost_uturn - cost of U-Turns in a vertex
          * \param[in] cost_forced_path - cost of traversing a forbidden edge 
          * \param[in] noisy - enable/disable follow-up log messages during path search 
          */
        template<typename Iterator>
        ERoutingAdapter(const Graph& graph,
                        Iterator start_first,
                        Iterator start_last,
                        Iterator end_first,
                        Iterator end_last,
                        int32_t cost_uturn = disabled_turn,
                        int32_t cost_forced_path = disabled_turn,
                        bool noisy = false)
        : m_graph(graph)
        , m_vertices()
        , m_start()
        , m_end()
        , m_cost_uturn(cost_uturn)
        , m_cost_forced_path(cost_forced_path)
        , m_noisy(noisy)
        {
            // Add start vertices { {vertex id, direction}, percentage} 
            for (auto it = start_first; it != start_last; ++it)
            {
                m_start.push_back(std::move(std::make_pair(it->first, it->second)));
            }
            // Add end vertices { {vertex id, direction}, percentage} 
            for (auto it = end_first; it != end_last; ++it)
            {
                // Add destination vertices
                m_end.push_back(std::move(std::make_pair(it->first, it->second)));
            }
        }

        /** \brief Destructor
          */
        virtual ~ERoutingAdapter() = default;
    
    public:
        /** \brief Fetches start and end waypoints
          * \returns pair of { 
          *                   { 
          *                     iterator pointing to first {start vertex, cost}, 
          *                     iterator pointing to last  {start vertex, cost} 
          *                   } 
          *                   { 
          *                     iterator pointing to first {end vertex, cost}, 
          *                     iterator pointing to last  {end vertex, cost} 
          *                   } 
          *                 }
          */
        std::pair<std::forward_list<std::pair<ERouteVertex, ERouteCost>>, 
                  std::forward_list<std::pair<ERouteVertex, ERouteCost>>> 
             operator()()
        {
            size_t vertex_id = 0;
            std::forward_list<std::pair<ERouteVertex, ERouteCost>> start, end;
            // Fetch start points
            for (auto it = std::begin(m_start); it != std::end(m_start); ++it)
            {
                // Get graph vertex object, based on vertex identifier(index)
                auto it_ = m_graph.get_vertex(it->first.id);                
                assert(it_ != std::end(m_graph));

                // Check for shortcuts - shortcut means start edge reaches directly an end edge
                // Shortcuts will be reached specially (via self connections) and need to avoid a double contering for the cost
                auto it__ = std::find_if(std::begin(m_end), std::end(m_end), [it](auto & val)
                {
                    return (val.first.id == it->first.id && it->first.fw == val.first.fw);
                });
                // Push start point        
                start.push_front(std::make_pair(ERouteVertex{ vertex_id++, it->first.fw},
                                ((it__ != std::end(m_end) && it__->second >= it->second) ? 0 : ( // do not add cost for shortcut edges
                                    (it->first.fw
                                    ? ((it_->costFw * (100 - it->second)) / 100)
                                    : ((it_->costBw * (100 - it->second)) / 100))))));
            }
            // Fetch end points
            for (auto it = std::begin(m_end); it != std::end(m_end); ++it)
            {
                auto it_ = m_graph.get_vertex(it->first.id);
                assert(it_ != std::end(m_graph));
                
                // Check for shortcuts - shortcut means start edge reaches directly an end edge
                // Shortcuts will be reached specially (via self connections) and need to avoid a double contering for the cost
                auto it__ = std::find_if(std::begin(m_start), std::end(m_start), [it](auto & val)
                {
                    return (val.first.id == it->first.id && it->first.fw == val.first.fw);
                });
                // Push end point        
                end.push_front(std::make_pair(ERouteVertex{ vertex_id++, it->first.fw}, 
                                ((it__ != std::end(m_start) && it__->second <= it->second) ? 0 : ( // do not add cost for shortcut edges
                                    (it->first.fw 
                                    ? ((it_->costFw * it->second) / 100)
                                    : ((it_->costBw * it->second) / 100))))));
            }
            return make_pair(start, end); 
        }
        
        /** \brief Fetches connection information for a vertex
          * \param[in] route_vertex - input vertex provided by routing engine
          * \param[in] cost - cost of input vertex, as helper information for the adapter
          * \param[in] forward - direction of routing: true = forward routing, false = backward routing
          * \returns pair of { feching succeed(true/false), 
          *                   { iterator pointing to first connections, 
          *                     iterator pointing to last connections} }
          */
        connections operator()(const ERouteVertex& route_vertex,
                               const ERouteCost& cost,
                               bool forward)
        {
            bool res = true;
            m_vertices.clear();
            size_t percent = 100;

            // Determine graph vertex identifier
            size_t graph_id = invalid_vertex;
            // Shortcut means start and end points are directly reachable on the same edge
            size_t shortcut_id = invalid_vertex; 
            size_t shortcut_percent = 0;
            if (route_vertex.id >= m_start.size() + m_end.size())
            {
                graph_id = route_vertex.id - (m_start.size() + m_end.size()); //graph vertex
            }
            // When not found - search in start points
            else if (route_vertex.id < m_start.size())
            {
                graph_id = (std::begin(m_start) + route_vertex.id)->first.id; // route vertex id for the start vertex
                percent = (std::begin(m_start) + route_vertex.id)->second; //percent for the start vertex
                // Forward routing only: check if vertex present in end points - when yes, verify if it is a shortcut
                if (forward)
                {
                    auto it = std::find_if(std::begin(m_end), std::end(m_end), [graph_id, &route_vertex](auto & val)
                    {
                        return (val.first.id == graph_id && route_vertex.fw == val.first.fw);
                    });
                    if (it != std::end(m_end))
                    {
                        // if start vertex is same with end vertex and start-point is before end point -> we have a shortcut
                        // retrieve the route vertex identifer for shortcut
                        shortcut_id = (it->second >= percent ? (it - std::begin(m_end) + (std::end(m_start) - std::begin(m_start))) : invalid_vertex);
                        shortcut_percent = (shortcut_id == invalid_vertex ? 0 : (it->second - percent));
                    }
                }
                percent = 100 - percent; // invert the percentage for start edges cost
            }
            // When not found - search in end points
            else if (route_vertex.id < (m_start.size() + m_end.size()))
            {
                graph_id = (std::begin(m_end) + (route_vertex.id - (std::end(m_start) - 
                            std::begin(m_start))))->first.id; //route vertex id for the end vertex
                percent = (std::begin(m_end) + (route_vertex.id - (std::end(m_start) - 
                            std::begin(m_start))))->second; //percent for the end vertex
                // Bacward routing only: check if vertex present in start points - when yes, verify if it is a shortcut
                if (!forward)
                {
                    // Check if present in start points - when yes, verify if it is a shortcut
                    auto it = std::find_if(std::begin(m_start), std::end(m_start), [graph_id, &route_vertex](auto & val)
                    {
                        return (val.first.id == graph_id && route_vertex.fw == val.first.fw);
                    });
                    if (it != std::end(m_start))
                    {
                        // if start vertex is same with end vertex and start-point is before end point -> we have a shortcut
                        // retrieve the route vertex identifer for shortcut
                        shortcut_id = (it->second <= percent ? ( it - std::begin(m_start) ): invalid_vertex);
                        shortcut_percent = (shortcut_id == invalid_vertex ? 0 : (percent - it->second));
                    }
                }
            }
            
            // Retrieve vertex connection in the direction of routing 
            // Connections are a pair of iterators pointing the to first and last connection
            // - where each connection is:
            //    pair of { {connected vertex (edge) index, fw/bw flag}, turn penalty}
            res = _fetch_connections(route_vertex, graph_id, percent, shortcut_id, shortcut_percent, forward);
            // Return the result
            return res 
                // When data access succeed
                ? connections{ true, vertices{ std::begin(m_vertices),
                                               std::end(m_vertices) } }
                // When errors in data access                               
                : connections{ false, vertices{ std::end(m_vertices),
                                                std::end(m_vertices) }};
        }

        /** \brief Converts routing path into graph sequence
          * \param[in] first - iterator pointing to the first element of route vertices
          * \param[in] last - iterator pointing to the last element of route vertices
          */
        template<typename Iterator>
        void convert_route(Iterator first,
                           Iterator last)
        {
            size_t graph_id = 0;
            Iterator prev_ = last;
            for (auto it = first; it != last; ++it)
            {
                if (it->first.id < m_start.size())
                {
                    graph_id = (std::begin(m_start) + it->first.id)->first.id;
                }
                else if (it->first.id < (m_start.size() + m_end.size()))
                {
                    graph_id = (std::begin(m_end) + (it->first.id -
                                                (std::end(m_start) - std::begin(m_start))))->first.id;
                }
                else
                {
                    graph_id = (it->first.id - (m_start.size() + m_end.size()));
                }
                it->first.id = graph_id;
                if (prev_ != last)
                {
                    if (   prev_->first.id == it->first.id 
                        && prev_->first.fw == it->first.fw
                        && prev_->second == it->second )
                    {
                        // shortcut found - do not encounter cost double
                        it->second = 0;
                    }
                }
                prev_ = it;
            }
        }
        
        /** \brief Dumps routing results
          * \param[in] first - iterator pointing to the first element of graph vertices
          * \param[in] last - iterator pointing to the last element of graph vertices
          */
        template<typename Iterator>
        void dump_route(Iterator first,
                        Iterator last)
        {
            ERouteCost totalCost = 0;
            logInfo("Calculated route:");            
            logInfo_("Edges: ");
            
            size_t graph_id  = 0;
            // Key information of a route record
            size_t graph_id_ = 0;
            bool   fw_ = false;
            int32_t cost_ = 0;
            for (auto it = first; it != last; ++it)
            {
                if (it->first.id < m_start.size())
                {
                    graph_id = (std::begin(m_start) + it->first.id)->first.id;
                    logInfo_((std::begin(m_start) + it->first.id)->second);
                    logInfo_("%>"); // start vertex
                }
                else if (it->first.id < (m_start.size() + m_end.size()))
                {
                    graph_id = (std::begin(m_end) + (it->first.id -
                                                (std::end(m_start) - std::begin(m_start))))->first.id;
                    logInfo_((std::begin(m_end) + (it->first.id -
                                                 (std::end(m_start) - std::begin(m_start))))->second);
                    logInfo_( "%|"); // destination vertex
                }
                else
                {
                    graph_id = (it->first.id - (m_start.size() + m_end.size()));
                }
                logInfo_(m_graph.get_vertex(graph_id)->name);
                logInfo_(it->first.fw ? "<Fw>" : "<Bw>");
                logInfo_("=");
                // Avoid double countering shortcuts
                cost_ = ( it != first 
                          && graph_id_ == graph_id 
                          && fw_ == it->first.fw  
                          && cost_ == it->second ) ? 0 : it->second;
                // Keep current for later compare          
                graph_id_ = graph_id;
                fw_ = it->first.fw;
                totalCost += cost_;

                logInfo_(cost_);
                logInfo_(" -> ");
            }
            logInfo_(" Total = ", totalCost);
            logInfo("");
        }

        /** \brief Method for capturing routing notifications - optional method
          * \param[in] vertex - visited vertex
          * \param[in] parent - parent vertex of visited vertex
          * \param[in] cost - accumulated cost for reachin vertex from parent vertex
          * \param[in] forward - direction of routing: true = forward routing, false = backward routing
          * \returns true, when routing shall aborted, otherwise false
          */
        bool operator()(const ERouteVertex& vertex, 
                        const ERouteVertex& parent,
                        const ERouteCost& cost,
                        bool forward) const
        {
            if (m_noisy)
            {
                logInfo_("  -> path search:");
                if (vertex.id < m_start.size())
                {
                    logInfo_(m_graph.get_vertex((std::begin(m_start) + vertex.id)->first.id)->name);
                }
                else if (vertex.id < (m_start.size() + m_end.size()))
                {
                    logInfo_(m_graph.get_vertex((std::begin(m_end) + (vertex.id -
                        (std::end(m_start) - std::begin(m_start))))->first.id)->name);
                }
                else
                {
                    logInfo_(m_graph.get_vertex(vertex.id - (m_start.size() + m_end.size()))->name);
                }
                logInfo_(" (", vertex.fw ? "Fw" : "Bw", ") <- ");
                if (parent.id < m_start.size())
                {
                    logInfo_(m_graph.get_vertex((std::begin(m_start) + parent.id)->first.id)->name);
                }
                else if (parent.id < (m_start.size() + m_end.size()))
                {
                    logInfo_(m_graph.get_vertex((std::begin(m_end) + (parent.id -
                        (std::end(m_start) - std::begin(m_start))))->first.id)->name);
                }
                else
                {
                    logInfo_(m_graph.get_vertex(parent.id - (m_start.size() + m_end.size()))->name);
                }
                logInfo(" (", parent.fw ? "Fw" : "Bw", ") = ", cost, forward ? " R:Fw" : " R:Bw");
            }
            return false;
        }

    private:
        /** \brief Fills up connection information for a vertex into the vertices container
          * \param[in] vertex - input vertex provided by routing engine
          * \param[in] graph_id - graph vertex identifier
          * \param[in] percent - percentage of vertex (when fully traversed than 100%)
          * \param[in] shortcut_id - shortcut identifier for current vertex - when exists, otherwise is invalid_vertex
          * \param[in] shortcut_percent - percentage of cost to shortcut
          * \param[in] forward - direction of routing: true = forward routing, false = backward routing
          * \returns true, when succeed, otherwise false 
          */
        bool _fetch_connections(const ERouteVertex& vertex,
                                size_t graph_id,
                                size_t percent,
                                size_t shortcut_id,
                                size_t shortcut_percent,
                                bool forward)
        {
            bool res = true;
            // Get graph vertex object, based on vertex identifier(index)
            auto it = m_graph.get_vertex(graph_id);
            if (it == std::end(m_graph))
            {
                // Graph corrupted - vertex not found
                logError("Graph vertex is missing from graph: ", graph_id, "!. Abort route calculation");
                res = false;
            }
            else
            {
                // Determine cost of current vertex - select forward/backward cost
                int32_t cost = (vertex.fw ? it->costFw : it->costBw);
                // Check if graph cost is a disabled path
                if (cost == disabled_path)
                {
                    cost = m_cost_forced_path; // when yes convert it to forced path
                }
                // Fetch connection for non disabled edges only
                if (cost != disabled_turn)
                {
                    // If shortcut is present
                    if (shortcut_id != invalid_vertex)
                    {
                        // Push the shortcut "as is" into the connections table
                        m_vertices.push_back(std::move(std::make_pair(ERouteVertex{ shortcut_id, vertex.fw }, (cost * shortcut_percent) / 100)));
                    }

                    // Apply percentage when necessary
                    if (percent != 100)
                    {
                        cost = (cost * percent) / 100;
                    }
                    // When U-turn allowed, add self-connection with U-Turn cost
                    if (m_cost_uturn != disabled_turn)
                    {
                        // Use non start/end vertex identifier for the opposite direction, adding as reaching cost the original vertex cost and U-Turn cost
                        // - by backward routing direction of connected vertices shall be inverted (graph stores only once each connection, in FW order)
                        _push_connection(vertex, graph_id, !vertex.fw, cost, m_cost_uturn, m_cost_uturn, forward);
                    }

                    // Retrieve vertex connection in the direction of routing, based on the direction of current vertex
                    // Connections are a pair of iterators pointing the to first and last connection
                    // - where each connection is:
                    //    pair of { {connected vertex (edge) index, forward/backward flag}, 
                    //              {FW turn penalty, BW turn penalty} }
                    auto cons_ = m_graph.get_vertex_edges(it, (forward ? vertex.fw : (!vertex.fw)));
                    for (auto it_ = cons_.first; it_ != cons_.second; ++it_)
                    {
                        // Convert connected vertex for routing scope and add to the connected vertices table
                        // - by backward routing direction of connected vertices shall be inverted (graph stores only once each connection, in FW order)
                        _push_connection(vertex, it_->first.first, forward ? it_->first.second : (!it_->first.second), cost, it_->second.first, it_->second.second, forward);
                    }
                }
            }
            return res;
        }
        
        /** \brief Converts a connected vertex for routing scope and enplaces into the connections table
          * \param[in] vertex - input vertex provided by routing engine
          * \param[in] connected_id - connected vertex identifier
          * \param[in] connected_fw - connected vertex direction
          * \param[in] vertex_cost - cost of vertex to reach the transition point for connected vertex
          * \param[in] turn_cost_fw - transition cost between vertex->connected vertex 
          * \param[in] turn_cost_bw - transition cost between connected vertex->vertex
          * \param[in] forward - direction of routing: true = FW routing, false = BW routing
          */
        void _push_connection(const ERouteVertex& vertex,
                              const size_t connected_id,
                              bool connected_fw,
                              const ERouteCost& vertex_cost,
                              const ERouteCost& turn_cost_fw,
                              const ERouteCost& turn_cost_bw,
                              bool forward)
        {
            // Connected routing vertex identifer
            size_t vertex_id = connected_id;
            // Search whether connected vertex is part of destinations 
            // means: part of end vertices by forward routing, or part of start vertices by backward routing 
            auto it = std::find_if(std::begin(forward ? m_end : m_start),
                                   std::end(forward ? m_end : m_start),
                                   [connected_fw, vertex_id](const auto& val)
            {
                return (val.first.id == vertex_id && val.first.fw == connected_fw);
            });
            // Search connected vertex in destinations 
            // - by FW routing destination is an end vertex
            // - by BW routing destination is a start vertex
            if (it == std::end(forward ? m_end : m_start))
            {
                // Connected vertex is not part of destination
                // - vertex identifier will get offseted with routing graph offset
                vertex_id += (m_end.size() + m_start.size());
            }
            else
            {
                // Connected vertex is part of destination
                // - vertex identifier will get transformed into destination id 
                // --> into end vertex by FW routing
                // --> into start vertex by BW routing
                vertex_id = (forward ? (m_start.size() + (it - std::begin(m_end))) : (it - std::begin(m_start)));
            }
            // The cost to the connected vertices(edges):
            //  - edge traversal cost + turn penalty to the given connection
            // --> u-turn penalty is vertex -> connected vertex by forward routing
            // --> u-turn penalty is connected vertex -> vertex by backward routing
            m_vertices.push_back(std::move(
                // Route vertex is graph vertex with offseted index
                // (to can identify, whether start/end get fetched) 
                std::make_pair(ERouteVertex{ vertex_id, connected_fw },
                               (forward ? turn_cost_fw : turn_cost_bw) + vertex_cost)));
        }
        
    private:
        // Instance of routable graph
        const Graph&    m_graph;
        // Vertices container for fetching and providing vertex connections
        _Vertices       m_vertices;
        // Start vertices
        _WayPoints      m_start;
        // Destination vertices
        _WayPoints      m_end;
        // Cost of U-Turns
        int32_t         m_cost_uturn;
        // Cost of forced paths near start/destination points
        int32_t         m_cost_forced_path;
        // Enable noisy logs to can follow-up the routing algorithms
        bool            m_noisy;
    };

}

}
}

#endif //__TASKS_GRAPHS_EROUTING_ADAPTERS_H__
