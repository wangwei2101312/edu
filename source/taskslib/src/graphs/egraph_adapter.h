///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding Graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.08.07.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_GRAPHS_EGRAPH_ADAPTERS_H__
#define __TASKS_GRAPHS_EGRAPH_ADAPTERS_H__

#include <tasks/task.h>
#include <edu/graphs/egraph.h>
#include <edu/graphs/graph_builder.h>
#include "graph_data.h"
#include <edu/log.h>
#include <assert.h>

namespace edu
{
namespace graph
{

namespace
{
    /** \brief Sample vertex structure for edge based graphs
      */
    struct EVertex
    {
        size_t       id;
        std::string  name;
        int32_t      costFw;
        int32_t      costBw;
    };
    
    /** \brief Sample edge structure for edge based graphs
      *  - first element stores transition cost for forward routing
      *  - first element stores transition cost for backward routing
      */
    using EEdge = std::pair< int32_t, int32_t >;

    /** \brief Adapter for filling up edge based graph from graph builder
      */
    template<typename Graph, 
             typename Builder,
             typename PenaltySupplier = std::function<int32_t(size_t, size_t)>>
    class EGraphAdapter
    {
    public:
        /** \brief Constructor
          * \param[in] graph - graph instance
          * \param[in] builder - graph builder instance
          * \param[in] skip_forbiddens - skip forbidden connections
          * \param[in] get_penalty - transition penalty supplier
          */
        EGraphAdapter(Graph& graph,
                      Builder& builder,
                      bool skip_forbiddens = true,
                      PenaltySupplier get_penalty =
                        [](size_t from, size_t to)->int32_t {return 0;})
        : m_graph(graph)
        , m_builder(builder)
        , m_skip_forbiddens(skip_forbiddens)
        , penalty_supplier(get_penalty)
        {
        }

        /** \brief Fills up the graph from builder
          * \returns true, when succeed, otherwise false 
          */
        bool operator()()
        {
            m_graph.resize(m_builder.edges());
            bool res = m_builder.pull_edges(*this);
            if (res)
            {
                res = m_builder.pull_connected_graph(*this, false, *this);
            }
            return res;
        }

        /** \brief Error handler invoked by the builder
          * \param[in] edge - problematic edge
          * \param[in] msg - builder message, the reason of error
          * \param[in] res - reference, or non-reference node of edge
          * \returns false, when pulling shall be aborted, otherwsie true
          */
        bool operator()(const InputEdge& edge, 
                        BuilderMessage msg,
                        bool ref)
        {
            if (BuilderMessage::vertex_is_missing == msg)
            {
                // Missed vertex 
                logError("Missing vertex for ", (ref ? "reference" : "non-reference"), " node for Edge = {id:",
                          edge.id, ", name:", edge.name, "}");
                return true;
            }
            return false;
        }

        /** \brief Pulls an edge from the builder, without connected edges
          * \param[in] edge - pulled edge
          * \param[in] edge_index - index of pulled edge
          * \returns false, when pulling shall be aborted, otherwise true
          */
        bool operator()(const InputEdge& edge,
                        size_t edge_index)
        {
            m_graph.set_vertex(edge_index,
                               std::move(EVertex{edge.id, edge.name, 
                                                 edge.costFw, edge.costBw}));
            return true;
        }

        /** \brief Pulls a graph edge from the builder, with connected edge, or vertex indexes
          * \param[in] edge - pulled edge
          * \param[in] edge_index - index of pulled edge
          * \param[in] ref_first - lower bound of range with connected edges in reference node
          * \param[in] ref_last - upper bound of range with connected edges in reference node
          * \param[in] nref_first - lower bound of range with connected edges in non-reference node
          * \param[in] nref_last - upper bound of range with connected edges in non-reference node
          * \returns false, when pulling shall be aborted, otherwsie true
          */
        template<typename T = bool, typename Iterator>
        typename std::enable_if<Graph::has_edge, T>:: 
        type operator()(const InputEdge& edge,
                        size_t edge_index,
                        Iterator ref_first,
                        Iterator ref_last,
                        Iterator nref_first,
                        Iterator nref_last)
        {
            // Set connected edges in non-reference node
            for (auto it = nref_first; it != nref_last; ++it)
            {
                // edge -> non-reference connected edges: fill-up transition penalties for FW/BW routing
                m_graph.set_edge(edge_index, it->first, it->second, true,
                    EEdge{ std::make_pair(penalty_supplier(edge.id, m_graph.get_vertex(it->first)->id),
                                            penalty_supplier(m_graph.get_vertex(it->first)->id, edge.id)) });
            }
            // Set connected edges in reference node
            for (auto it = ref_first; it != ref_last; ++it)
            {
                // edge -> reference connected edges
                m_graph.set_edge(edge_index, it->first, it->second, false,
                    EEdge{ std::make_pair(penalty_supplier(edge.id, m_graph.get_vertex(it->first)->id),
                                            penalty_supplier(m_graph.get_vertex(it->first)->id, edge.id)) });
            }
            return true;
        }

        /** \brief Pulls a graph edge from the builder, with connected edge, or vertex indexes
          * \param[in] edge - pulled edge
          * \param[in] edge_index - index of pulled edge
          * \param[in] ref_first - lower bound of range with connected edges in reference node
          * \param[in] ref_last - upper bound of range with connected edges in reference node
          * \param[in] nref_first - lower bound of range with connected edges in non-reference node
          * \param[in] nref_last - upper bound of range with connected edges in non-reference node
          * \returns false, when pulling shall be aborted, otherwsie true
          */
        template<typename T = bool, typename Iterator>
        typename std::enable_if<!Graph::has_edge, T>:: 
        type operator()(const InputEdge& edge,
                        size_t edge_index,
                        Iterator ref_first,
                        Iterator ref_last,
                        Iterator nref_first,
                        Iterator nref_last)
        {
            // Set connected edges in non-reference node - only when edge is navigable into FW direction
            if (edge.costFw || (!m_skip_forbiddens))
            {
                for (auto it = nref_first; it != nref_last; ++it)
                {
                    // edge -> non-reference connected edges
                    m_graph.set_edge(edge_index, it->first, it->second, true);
                }
            }
            // Set connected edges in reference node - only when edge is navigable into BW direction
            if (edge.costBw || (!m_skip_forbiddens))
            {
                // Set connected edges in reference node
                for (auto it = ref_first; it != ref_last; ++it)
                {
                    // edge -> reference connected edges
                    m_graph.set_edge(edge_index, it->first, it->second, false);
                }
            }
            return true;
        }
        
    private:
        Graph&          m_graph;
        Builder&        m_builder;
        bool            m_skip_forbiddens;
        PenaltySupplier penalty_supplier;
    };

}

}
}
#endif //__TASKS_GRAPHS_EGRAPH_ADAPTERS_H__
