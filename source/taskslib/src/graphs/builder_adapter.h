///////////////////////////////////////////////////////////////////////////////////////////////////
// Adapter classes and functors for graph builder
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_GRAPHS_BUILDER_ADAPTER_H__
#define __TASKS_GRAPHS_BUILDER_ADAPTER_H__

#include <edu/graphs/graph_builder.h>
#include "graph_data.h"
#include <string>
#include <list>
#include <vector>


namespace edu
{
namespace graph
{
 
namespace
{
    /** \brief Sample builder adapter for pulling data, receiving notifications
      *  The adapter has no functional purpose, it logs only the pulled content from builder
      *  Logs: vertices with their connections, pulled for a vertex based graph
      *        edges with their reference, non-reference connected edges and the direction of transition 
      */
    struct BuilderAdapter
    {
        /** \brief Constructor
          * \param[in] vertices - container for pulled vertices
          * \param[in] edges - container for pulled edges
          */
        BuilderAdapter(std::vector<InputVertex>& vertices,
                       std::vector<InputEdge>& edges)
        : m_vertices(vertices)
        , m_edges(edges)
        , vertex_pulls(0)
        , edge_pulls(0)
        , graph_pulls(0)
        , pull_vertex_graph(true)
        {
        }

        /** \brief Pulls an edge from the builder, without connected edges
          * \param[in] edge - pulled edge
          * \param[in] edge_index - index of pulled edge
          * \returns false, when pulling shall be aborted, otherwsie true
          */
        bool operator()(const InputEdge& edge,
                        size_t edge_index)
        {
            logInfo(++edge_pulls, ". Pulled edge = [", edge_index, "] {id:", edge.id, ", name:", edge.name, 
                    ", R:", edge.startNode, ", NR:", edge.endNode, "}");
            m_edges[edge_index] = edge;
            return true;
        }

        /** \brief Pulls a vertex from the builder, without connected edges
          * \param[in] vertex - pulled vertex
          * \param[in] vertex_index - index of pulled edge
          * \returns false, when pulling shall be aborted, otherwsie true
          */
        bool operator()(const InputVertex& vertex,
                        size_t vertex_index)
        {
            logInfo(++vertex_pulls, ". Pulled vertex = [", vertex_index, "] {id:", vertex.id, "}");
            m_vertices[vertex_index] = vertex;
            return true;
        }

        /** \brief Pulls a graph edge from the builder, with connected edge, or vertex indexes
          * \param[in] edge - pulled edge
          * \param[in] edge_index - index of pulled edge
          * \param[in] ref_first - lower bound of range with connected edges in reference node
          * \param[in] ref_last - upper bound of range with connected edges in reference node
          * \param[in] nref_first - lower bound of range with connected edges in non-reference node
          * \param[in] nref_last - upper bound of range with connected edges in non-reference node
          * \returns false, when pulling shall be aborted, otherwsie true
          */
        template<typename Iterator>
        bool operator()(const InputEdge& edge,
                        size_t edge_index,
                        Iterator ref_first,
                        Iterator ref_last,
                        Iterator nref_first,
                        Iterator nref_last)
        {
            // Log pulled content of edges - connected edges
            logInfo_(++graph_pulls, ". Pulled edge = [", edge_index, "]{id:", edge.id, ", name:", edge.name, 
                     ", R:", edge.startNode, ", NR:", edge.endNode ,"} Connections: ");
            logInfo_("Reference = {");
            for (auto it = ref_first; it != ref_last; ++it)
            {
                logInfo_(((it == ref_first) ? "id:" : ", id:"), 
                         (pull_vertex_graph ? m_vertices[it->first].id : m_edges[it->first].id), " name:",
                         (pull_vertex_graph ? m_vertices[it->first].id : m_edges[it->first].id),
                         (pull_vertex_graph ? "" : (it->second? "<Fw>:" : "<Bw>:")),
                         (pull_vertex_graph ? "" : m_edges[it->first].name));
            }
            logInfo_("} ");
            logInfo_("Non Reference = {");
            for (auto it = nref_first; it != nref_last; ++it)
            {
                logInfo_(((it == nref_first) ? "" : ", "), 
                        (pull_vertex_graph ? m_vertices[it->first].id : m_edges[it->first].id),
                        (pull_vertex_graph ? "" : (it->second? "<Fw>:" : "<Bw>:")),
                        (pull_vertex_graph ? "" : m_edges[it->first].name));
            }
            logInfo("}");
            return true;
        }
        
    private:
        std::vector<InputVertex>&   m_vertices;
        std::vector<InputEdge>&     m_edges;
        
    public:
        size_t                      vertex_pulls;
        size_t                      edge_pulls;
        size_t                      graph_pulls;
        bool                        pull_vertex_graph;
    };

    
}

}
}

#endif //__TASKS_GRAPHS_BUILDER_ADAPTER_H__
