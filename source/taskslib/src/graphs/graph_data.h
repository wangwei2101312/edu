///////////////////////////////////////////////////////////////////////////////////////////////////
// Callback classes and functors for graph builder
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __TASKS_GRAPHS_GRAPH_DATA_H__
#define __TASKS_GRAPHS_GRAPH_DATA_H__

#include <edu/graphs/xgraph.h>
#include <edu/log.h>
#include <string>
#include <list>
#include <vector>
#include <sstream>


namespace edu
{
namespace graph
{

    
namespace
{
    /** \brief Sample edge structure
      */
    struct InputEdge
    {
        size_t       id;
        std::string  name;
        size_t       startNode;
        size_t       endNode;
        int32_t      costFw;
        int32_t      costBw;
    };

    /** \brief Sample vertex structure
      */
    struct InputVertex
    {
        size_t      id;
    };

    /** \brief Sample transition penalty structure
    */
    struct InputPenalty
    {
        size_t   fromEdge;
        size_t   toEdge;
        int32_t  penalty; // transition penalty by accessing fromEdge -> toEdge
    };

    /** \brief Sets of test data
    */
    const InputEdge edge_set_1[] =
    {
        { 1, "E-1",   1,  2,  7, 11 },
        { 2, "E-2",   2,  4,  8,  3 },
        { 3, "E-3",   2,  3,  2,  4 },
        { 4, "E-4",   4,  7, 11,  5 },
        { 5, "E-5",   3,  5, 11,  5 },
        { 6, "E-6",   3,  7,  2,  6 },
        { 7, "E-7",   5,  6, 12,  6 },
        { 8, "E-8",   6,  7,  9,  4 },
        { 9, "E-9",   7, 10,  3,  5 },
        { 10, "E-10",  7, 11,  5,  6 },
        { 11, "E-11", 10, 11, 23, 14 },
        { 12, "E-12", 10, 14, 12,  8 },
        { 13, "E-13", 14, 15, 14, 15 },
        { 14, "E-14", 11, 15,  6,  5 },
        { 15, "E-15", 11, 17,  7,  8 },
        { 16, "E-16", 11, 12,  5,  5 },
        { 17, "E-17",  7,  9,  6,  7 },
        { 18, "E-18",  8,  9,  8,  8 },
        { 19, "E-19",  6,  8,  3,  0 },
        { 20, "E-20",  5, 18, 41,  0 },
        { 21, "E-21", 18, 19, 34,  0 },
        { 22, "E-22", 13, 19,  5,  6 },
        { 23, "E-23",  9, 13,  0,  4 },
        { 24, "E-24", 12, 13,  6,  0 },
        { 25, "E-25", 16, 17,  8,  9 },
        { 26, "E-26", 15, 16,  3,  4 },
        { 27, "E-27", 14, 16,  3,  0 },
        { 28, "E-28",  5,  8,  7,  2 }
    };
    /** \brief Sets of transition penalties between edges
    */
    const InputPenalty penalty_set_1[] =
    {
        { 1, 2, 3 },
        { 2, 1, 5 },
        { 1, 3, 2 },
        { 3, 1, 4 },
        { 7, 8, 2 },
        { 8, 7, 1 },
        { 9, 4, 6 },
        { 4, 9, 6 }
    };

    /** \brief Simple log(N) based transition penalty supplier functor for edge graph adapters
      * \returns true, means any reported error shall be showstopper 
      */ 
    struct penalty_supplier
    {
        int32_t operator()(size_t idFrom,
                           size_t idTo)
        {
            auto it = std::find_if(std::begin(penalty_set_1), std::end(penalty_set_1), [&](const InputPenalty& p)
            {
                return (p.fromEdge == idFrom && p.toEdge == idTo);
            });
            return (it == std::end(penalty_set_1) ? 0 : it->penalty);
        }
    };

    /** \brief Simple error reporter callback.
      * \returns true, means any reported error shall be showstopper 
      */ 
    struct error_observer
    {  
        bool operator()(const InputEdge& edge, 
                        BuilderMessage msg)
        {
            const std::string err[] 
            {
                "unknown",
                "Vertex already exists!",
                "Edge already exists!",
                "Edge is a loop!",
                "Could not find vertex!",
                "Connection is missing!"
            };
            
            if (BuilderMessage::none != msg)
            {
                logError("Could not insert edge: {id:", edge.id, ", name:", edge.name, "} due to:", 
                         err[static_cast<int32_t>(msg)]);
            }
            return BuilderMessage::none != msg;
        }
    };

}


/** \brief Specialized functor for comparing a vertex
  */
template<>
struct is_equal<InputVertex>
{
    bool operator()(const InputVertex& l, 
                    const InputVertex& r) const
    {
        return l.id == r.id;
    }
};
/** \brief Specialized functor for comparing an edge
  */
template<>
struct is_equal<InputEdge>
{
    bool operator()(const InputEdge& l, 
                    const InputEdge& r) const
    {
        return l.id == r.id;
    }
};

/** \brief Specialized functor for hashing a vertex
  */
template <>
struct hash_of<InputVertex>
{
    size_t operator()(const InputVertex& v) const
    {
        return std::hash<int32_t>()(v.id);
    }
};
/** \brief Specialized functor for hashing an edge
  */
template <>
struct hash_of<InputEdge>
{
    size_t operator()(const InputEdge& e) const
    {
        return std::hash<int32_t>()(e.id);
    }
};

/** \brief Specialized functor for retrieving vertex information from edge in reference node
  */
template <>
struct edge_to_vertex<InputVertex, InputEdge, true>
{
    InputVertex operator()(const InputEdge& e) const
    {
        return InputVertex{ e.startNode };
    }
};
/** \brief Specialized functor for retrieving vertex information from edge in non-reference node
  */
template <>
struct edge_to_vertex<InputVertex, InputEdge, false>
{
    InputVertex operator()(const InputEdge& e) const
    {
        return InputVertex{ e.endNode };
    }
};
    
    
}
} 

#endif //__TASKS_GRAPHS_BUILDER_CALLBACKS_H__
