///////////////////////////////////////////////////////////////////////////////////////////////////
// Selection-sort sample implementation 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.19.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_SORTING_SELECTIONSORT_H__
#define __EDU_SORTING_SELECTIONSORT_H__

#include <functional>
#include <algorithm>

namespace edu
{
namespace sorting
{

/** \brief Functor for selection-sort
  */
template<typename Iterator, 
         typename Compare = std::function<bool(Iterator, Iterator)>>
struct selection_sort
{
    /** \brief Launcher for selection-sort
      * \param[in] first - forward iterator, pointing to the lower bound of range to sort
      * \param[in] last - forward iterator, pointing to the upper bound of range to sort
      * \param[in] compare - compare function
      */
    void operator()(Iterator first,
                    Iterator last,
                    Compare compare = 
                    [](Iterator l, Iterator r)->bool
                    {return *l < *r;}) noexcept
    {
        if (first != last)
        {
            // Iterate the range
            for (auto it = first; it != last; ++it)
            {
                // Suppose position for minimum element
                auto min_ = it;
                auto it_ = it; // iterator for next element
                ++it_;
                // Iterate from next lement until the end of range
                for (; it_ != last; ++it_)
                {
                    // Check each element, whether is minimum
                    if (compare(it_, min_))
                    {
                        // New minimum found
                        min_ = it_;
                    }
                }
                // When minimum is not the current element -> swap them
                if (min_ != it)
                {
                    std::swap(*it, *min_);
                }
            }
        }
    }

};


/** \brief Function for selection-sort
  * \param[in] first - forward iterator, pointing to the lower bound of range to sort
  * \param[in] last - forward iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_selection_sort(Iterator first,
                         Iterator last,
                         Compare compare =
                         [](Iterator l, Iterator r)->bool 
                         {return *l < *r; }) noexcept
{
    selection_sort<Iterator, Compare>()(first, last, compare);
}

}
}

#endif //__EDU_SORTING_SELECTIONSORT_H__
