///////////////////////////////////////////////////////////////////////////////////////////////////
// Insertion-sort sample implementation 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.19.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_SORTING_INSERTIONSORT_H__
#define __EDU_SORTING_INSERTIONSORT_H__

#include <functional>
#include <algorithm>

namespace edu
{
namespace sorting
{

/** \brief Functor for insertion-sort
  */
template<typename Iterator, 
         typename Compare = std::function<bool(Iterator, Iterator)>>
struct insertion_sort
{
    /** \brief Launcher for insertion-sort
      * \param[in] first - bi-directional iterator, pointing to the lower bound of range to sort
      * \param[in] last - bi-directional iterator, pointing to the upper bound of range to sort
      * \param[in] compare - compare function
      */
    void operator()(Iterator first,
                    Iterator last,
                    Compare compare = 
                    [](Iterator l, Iterator r)->bool
                    {return *l < *r;}) noexcept
    {
        if (first != last)
        {
            // Start from the 2nd element in the range
            auto it = first;
            ++it;
            // Iterate through the "un-seen" sub-range
            for (; it != last; ++it)
            {
                // Check if current element is on its place taking the "seen" sub-range
                auto it__ = it; // last element of "seen" sub-range
                for (auto it_ = it; it_ != first; --it_)
                {
                    if (compare(it_, --it__))
                    {
                        // Current element is less than previous, swap them and continue
                        std::swap(*it_, *it__);
                        continue;
                    }
                    // the "seen" sub-range got sorted: can stop 
                    break;
                }
            }
        }
    }

};


/** \brief Function for insertion-sort
  * \param[in] first - bi-directional iterator, pointing to the lower bound of range to sort
  * \param[in] last - bi-directional iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_insertion_sort(Iterator first,
                         Iterator last,
                         Compare compare =
                         [](Iterator l, Iterator r)->bool 
                         {return *l < *r; }) noexcept
{
    insertion_sort<Iterator, Compare>()(first, last, compare);
}


}
}

#endif //__EDU_SORTING_INSERTIONSORT_H__
