///////////////////////////////////////////////////////////////////////////////////////////////////
// Merge-sort sample implementation 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.17.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_SORTING_MERGESORT_H__
#define __EDU_SORTING_MERGESORT_H__

#include <functional>
#include <algorithm>
#include <type_traits>
#include <vector>
#include <iterator>
#include <assert.h>

namespace edu
{
namespace sorting
{

    
/** \brief Default merging mode
  */
constexpr bool merge_sort_sort_in_place = false;


/** \brief Functor for merge-sort
  */
template<typename Iterator, 
         typename Compare = std::function<bool(Iterator, Iterator)>,
         bool sort_in_place = merge_sort_sort_in_place>
struct merge_sort
{
    /** \brief Launcher for merge-sort
      * \param[in] first - random access iterator, pointing to the lower bound of range to sort
      * \param[in] last - random access iterator, pointing to the upper bound of range to sort
      * \param[in] compare - compare function
      */
    void operator()(Iterator first,
                    Iterator last,
                    Compare compare = 
                    [](Iterator l, Iterator r)
                    {return *l < *r;}) noexcept(sort_in_place)
    {
        if (first != last)
        {
            sort<Iterator, Compare, Helper>()(first, last, compare);
        }
    }

private:
    // Additional buffer type for merging sorted segments: 
    using Helper = typename std::conditional<sort_in_place,
                   void,
                   typename std::vector<typename std::iterator_traits<Iterator>::value_type>>::type;

                   
    /** \brief Sorting variant with the use of additional storage for merging
      */
    template<typename Iterator_,
             typename Compare_,
             typename Helper_>
    struct sort
    {
        /** \brief Launcher for merge-sort
          * \param[in] first - random access iterator, pointing to the lower bound of range to sort
          * \param[in] last - random access iterator, pointing to the upper bound of range to sort
          * \param[in] compare - compare function
          */
        void operator()(Iterator_ first,
                        Iterator_ last,
                        Compare_ compare)
        {
            Helper_ helper(last - first);
            _sort(first, last, helper, compare);
        }

        /** \brief Sorts a sub-range
          * \param[in] first - random access iterator, pointing to the lower bound of range to sort
          * \param[in] last - random access iterator, pointing to the upper bound of range to sort
          * \param[in] helper - additional storage
          * \param[in] compare - compare function
          */
        void _sort(Iterator_ first,
                   Iterator_ last,
                   Helper_& helper,
                   Compare_ compare) noexcept
        {
            if ((last - first) == 2)
            {
                // When 2 elements only: compare them
                if (!compare(first, first + 1))
                {
                    // Swap when not sorted
                    std::swap(*first, *(first + 1));
                }
            }
            else if ((last - first) > 2)
            {
                // When more than 2 elements, divide them into 2 segments and sort each segment
                _sort(first, first + (last - first) / 2, helper, compare);
                _sort(first + (last - first) / 2, last, helper, compare);
                // Merge the sorted segments
                merge<Iterator_, Compare_, Helper_>
                    ()(first, first + (last - first) / 2, last, helper, compare);
            }
        }
    };

    /** \brief Sorting variant with in-place merge
      */
    template<typename Iterator_,
             typename Compare_>
    struct sort<Iterator_, Compare_, void>
    {
        /** \brief Launcher for merge-sort
          * \param[in] first - random access iterator, pointing to the lower bound of range to sort
          * \param[in] last - random access iterator, pointing to the upper bound of range to sort
          * \param[in] compare - compare function
          */
        void operator()(Iterator_ first,
                        Iterator_ last,
                        Compare compare)
        {
            _sort(first, last, compare);
        }

        /** \brief Sorts a sub-range
          * \param[in] first - random access iterator, pointing to the lower bound of range to sort
          * \param[in] last - random access iterator, pointing to the upper bound of range to sort
          * \param[in] compare - compare function
          */
        void _sort(Iterator_ first,
                   Iterator_ last,
                   Compare_ compare) noexcept
        {
            if ((last - first) == 2)
            {
                // When 2 elements only: compare them
                if (!compare(first, first + 1))
                {
                    // Swap when not sorted
                    std::swap(*first, *(first + 1));
                }
            }
            else if ((last - first) > 2)
            {
                // When more than 2 elements, divide them into 2 segments and sort each segment
                _sort(first, first + (last - first) / 2, compare);
                _sort(first + (last - first) / 2, last, compare);
                // Merge the sorted segments
                merge<Iterator_, Compare_, void>
                    ()(first, first + (last - first) / 2, last, compare);
            }
        }
    };


    /** \brief Functor for merging sorted sub-ranges with the use of additional storage
      */
    template<typename Iterator_,
             typename Compare_,
             typename Helper_>
    struct merge
    {
        /** \brief Merges two sorted sub-ranges
          * \param[in] first - random access iterator, pointing to the lower bound of range to sort
          * \param[in] middle - random access iterator, pointing to the border between the two sub-ranges
          * \param[in] last - random access iterator, pointing to the upper bound of range to sort
          * \param[in] helper - additional storage
          * \param[in] compare - compare function
          */
        void operator()(Iterator_ first,
                        Iterator_ middle,
                        Iterator_ last,
                        Helper_& helper,
                        Compare_ compare) noexcept
        {
            auto it_1 = first;
            auto it_2 = middle;
            auto pos = std::begin(helper);
            // Iterate segments until end of one is not reached
            while ((it_1 != middle) && (it_2 != last))
            {
                // Add the less element into the temporary container
                if (compare(it_1, it_2))
                {
                    *pos++ = *it_1++;
                }
                else
                {
                    *pos++ = *it_2++;
                }
            }
            // Add the remained elements from first segment
            while (it_1 != middle)
            {
                *pos++ = *it_1++;
            }
            // Add the remained elements from second segment
            while (it_2 != last)
            {
                *pos++ = *it_2++;
            }
            // Iterator of helper shall be at the end
            assert(pos == (std::begin(helper) + (last - first)));

            // Re-copy sorted elements from the temporary container
            pos = std::begin(helper);
            for (auto it = first; it != last; ++it)
            {
                *it = *pos++;
            }
        }
    };


    /** \brief Specialized functor for merge sorted sub-ranges inline
      */
    template<typename Iterator_,
             typename Compare_>
    struct merge<Iterator_, Compare_, void>
    {
        /** \brief Merges two sorted sub-ranges
          * \param[in] first - random access iterator, pointing to the lower bound of range to sort
          * \param[in] middle - random access iterator, pointing to the border between the two sub-ranges
          * \param[in] last - random access iterator, pointing to the upper bound of range to sort
          * \param[in] compare - compare function
          */
        void operator()(Iterator_ first,
                        Iterator_ middle,
                        Iterator_ last,
                        Compare_ compare) noexcept
        {
            // Iterate the first segment
            for (auto it = first; it != middle; it++)
            {
                // Check when current element of first segment is greater than first element of second
                if (compare(middle, it))
                {
                    // When second segment's element is less - swap it
                    std::swap(*it, *middle);
                    // Second segment need to remain sorted - move the new element to its place
                    auto pos_ = middle;
                    for (auto pos = middle + 1; pos != last; pos++)
                    {
                        if (compare(pos, pos_))
                        {
                            // Need to move upper
                            std::swap(*pos, *pos_++);
                            continue;
                        }
                        // Next element is not less - we can stop here since segment was sorted
                        break;
                    }
                }
            }
        }
    };
};


/** \brief Function for merge-sort with default merging policy
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_merge_sort(Iterator first,
                     Iterator last,
                     Compare compare =
                     [](Iterator l, Iterator r)->bool 
                     {return *l < *r; }) noexcept
{
    merge_sort<Iterator, Compare, merge_sort_sort_in_place>()(first, last, compare);
}


/** \brief Function for merge-sort with inline merging
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_merge_sort_mi(Iterator first,
                        Iterator last,
                        Compare compare =
                        [](Iterator l, Iterator r)->bool 
                        {return *l < *r; }) noexcept
{
    merge_sort<Iterator, Compare, true>()(first, last, compare);
}


/** \brief Function for merge-sort with merging with the use of additional storage
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_merge_sort_mx(Iterator first,
                        Iterator last,
                        Compare compare =
                        [](Iterator l, Iterator r)->bool 
                        {return *l < *r; }) noexcept
{
    merge_sort<Iterator, Compare, false>()(first, last, compare);
}


}
}

#endif //__EDU_SORTING_MERGESORT_H__
