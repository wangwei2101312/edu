///////////////////////////////////////////////////////////////////////////////////////////////////
// Radix-sort sample implementation 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.17.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_SORTING_RADIXSORT_H__
#define __EDU_SORTING_RADIXSORT_H__

#include <functional>
#include <algorithm>
#include <list>
#include <type_traits>
#include <iterator>

namespace edu
{
namespace sorting
{

/** \brief Functor for buckets by radix-sort
  *  The functor offers default specializations for integral values and ASCII strings
  *  Further extensions: 
  *   - either a user level specialization needed for radix_sort_buckets<MyType, false>
  *   - either a similar functor can be implemented and passed to make_radix_sort as argument
  *  Ex: my_radix_buckets<MyType, bool = is_MyType_an_integral_type>
  *  The radix sort functor detects whether the base type is integral type and sets the 2nd 
  *  template argument to: true, when MyType is integral(signed/unsigned integer), otherwise to false
  */
template<typename T, bool = std::is_integral<T>::value>
struct radix_sort_buckets {};

/** \brief Functor for integral buckets by radix-sort
  */
template<typename T>
struct radix_sort_buckets<T, true>
{
private:
    /** \brief Functor for differentiated handling of signed/unsigned numeric values
      */
    template<typename T_, bool = std::is_signed<T>::value>
    struct numeric_buckets {};

public:

    // Buckets count for numeric values
    static const size_t size = numeric_buckets<T>::size;

    /** \brief Determines the bucket index for a certain value by a certain digit level
      * \param[in] num - the integral value to be inserted into a bucket
      * \param[in] level - the level of digit
	  * \param[in] max_levels - the maximum digits count
	  * \returns bucket index
      */
    static size_t index(const T& num, 
                        size_t level,
						size_t max_levels) noexcept
    {
        return numeric_buckets<T>::index(num, level, max_levels);
    }

    /** \brief Digits count for a numeric value
      * \param[in] num - the numeric value
      * \returns digits count
      */
    static size_t length(const T& num) noexcept
    {
        return numeric_buckets<T>::length(num);
    }

private:
    /** \brief Divider for numerical values
      * \param[in] level - level of digit
      * \returns divider value as (10^level)
      */
    static constexpr size_t divider(size_t level) noexcept
    {
        return (level ? (10 * divider(level - 1)) : 1);
    }

    /** \brief Digits count of numeric value
      * \param[in] num - numeric value
      * \returns divider value as (10^level)
      */
    static constexpr size_t digits(T num) noexcept
    {
        return (num ? (digits(num / 10) + 1) : 0);
    }
    
    /** \brief Functor for signed numbers
      */
    template<typename T_>
    struct numeric_buckets<T_, true>
    {
        // Count of necessary buckets (-9 .. +9)
        static const T_ size = 19;

        // Bucket index on a certain sort level: the digit
        static T_ index(const T_& num, size_t level, size_t) noexcept
        {
            return ( (num < 0) 
                     ? (9 - (((-num) / divider(level)) % 10))
                     : (9 + ((num / divider(level)) % 10)));
        }

        // Number of digits in the numerical value
        static size_t length(const T_& num) noexcept
        {
            return digits((num < 0) ? -num : num);
        }
    };

    /** \brief Functor for unsigned numbers
      */
    template<typename T_>
    struct numeric_buckets<T_, false>
    {
        // Count of necessary buckets (0 .. +9)
        static const T_ size = 10u;

        // Bucket index on a certain sort level: the digit
        static T_ index(const T_& num, size_t level, size_t) noexcept
        {
            return ((num / divider(level)) % 10u);
        }

        // Number of digits in the numerical value
        static size_t length(const T_& num) noexcept
        {
            return digits(num);
        }
    };
};


/** \brief Functor for string buckets by radix-sort
  */
template<>
struct radix_sort_buckets<std::string, false>
{
    // Buckets count for character values
    static const unsigned char size = 255;

    /** \brief Determines the bucket index for a certain caracter for a certain position
      * \param[in] str - the string value to be inserted into a bucket
      * \param[in] level - the level of cacharacter
	  * \param[in] max_levels - the maximum character counts
	  * \returns bucket index
      */
    static unsigned char index(const std::string& str,
                               size_t level,
							   size_t max_levels) noexcept
    {
        return (((max_levels - level - 1) < str.length()) ? str.at(max_levels - level - 1) : 0);
    }

    /** \brief Length of string
      * \param[in] str - the string
      * \returns string length
      */
    static size_t length(const std::string& str) noexcept
    {
        return str.length();
    }
};


/** \brief Functor for radix-sort
  */
template<typename Iterator, 
         template<typename T_,bool = std::is_integral<T_>::value>class bucket_policy = radix_sort_buckets>
struct radix_sort
{
    /** \brief Launcher for radix-sort
      * \param[in] first - forward iterator, pointing to the lower bound of range to sort
      * \param[in] last - forward iterator, pointing to the upper bound of range to sort
      */
    void operator()(Iterator first,
                    Iterator last)
    {
        using T = typename std::iterator_traits<Iterator>::value_type;
        // Buckets container
        Buckets_<T> buckets[bucket_policy<T>::size];
        size_t max_levels = 0;
        // Determine the number of sorting levels to run
        std::for_each(first, last, [&max_levels](T& val)
        {
            max_levels = std::max(max_levels, bucket_policy<T>::length(val));
        });
        // Run each level of sorting
        for (size_t level = 0; level < max_levels; level++)
        {
            sort(first, last, buckets, level, max_levels);
        }
    }

private:
    template<typename T>
    using Buckets_ = typename std::list<T>;
    
    /** \brief Sorts with buckets
      * \param[in] first - forward iterator, pointing to the lower bound of range to sort
      * \param[in] last - forward iterator, pointing to the upper bound of range to sort
      * \param[in] buckets - buckets array
      * \param[in] level - current sorting level
      * \param[in] max_levels - maximum of sort levels to be processed
      */
    template<typename T>
    void sort(Iterator first,
              Iterator last,
              Buckets_<T>* buckets,
              size_t level,
              size_t max_levels)
    {
        // Drop each element into it's bucket, according to the value from current level
        std::for_each(first, last, [&](T& val)
        {
            buckets[bucket_policy<T>::index(val, level, max_levels)].push_back(val);
        });
        // Re-insert into container values from buckets in the order of bucket index and FIFO
        auto it = first;
        for (size_t bck = 0; bck < bucket_policy<T>::size; bck++)
        {
            for (auto& val : buckets[bck])
            {
                *it++ = val;
            }
            buckets[bck].clear();
        }
    }
};


/** \brief Function for radix-sort using implicit buckets handler
  * \param[in] first - forward iterator, pointing to the lower bound of range to sort
  * \param[in] last - forward iterator, pointing to the upper bound of range to sort
  */
template<typename Iterator>
void make_radix_sort(Iterator first,
                     Iterator last) noexcept
{
    radix_sort<Iterator, radix_sort_buckets>()(first, last);
}


/** \brief Function for radix-sort using an explicit buckets handler
  * \param[in] first - forward iterator, pointing to the lower bound of range to sort
  * \param[in] last - forward iterator, pointing to the upper bound of range to sort
  */
template<template<typename, bool = false>class bucket_policy,
         typename Iterator>
void make_radix_sort(Iterator first,
                     Iterator last) noexcept
{
    radix_sort<Iterator, bucket_policy>()(first, last);
}


}
}

#endif //__EDU_SORTING_RADIXSORT_H__
