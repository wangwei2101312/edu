///////////////////////////////////////////////////////////////////////////////////////////////////
// Quick-sort sample implementation 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.17.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_SORTING_QUICKSORT_H__
#define __EDU_SORTING_QUICKSORT_H__

#include <functional>
#include <algorithm>
#include <type_traits>
#include <iterator>

namespace edu
{
namespace sorting
{

    
/** \brief Pivot selection modes
  */
enum class Pivot
{
    rand = 0,
    first,
    last,
    middle
};


/** \brief Functor for quick-sort
  */
template<typename Iterator, 
         typename Compare = std::function<bool(Iterator, Iterator)>,
         Pivot pivot = Pivot::rand>
struct quick_sort
{
    /** \brief Launcher for quick-sort supporting forward iterators. For optimal run: 
      *  - use random access iterators for "random", "middle" and "last" pivot selections
      *  - use forward iterators for "first" pivot selection 
      * \param[in] first - iterator pointing to the lower bound of range to sort
      * \param[in] last - iterator pointing to the upper bound of range to sort
      * \param[in] compare - compare function
      */
    void operator()(Iterator first,
                    Iterator last,
                    Compare compare = 
                    [](Iterator l, Iterator r)->bool
                    {return *l < *r;}) noexcept
    {
        if (first != last)
        {
            sort(first, last, compare);
        }
    }

private:
    /** \brief Sorting in a partition
      * \param[in] first - lower bound of partition
      * \param[in] last - upper bound of partition
      * \param[in] compare - compare function
      */
    void sort(Iterator first,
              Iterator last,
              Compare compare) noexcept
    {
        if (first != last)
        {
            auto pivot_element = pivot_selection<Iterator, Compare, pivot>()(first, last);

            // Resolve the left-side:
            //  - move all less data as pivot in the front
            auto move_bound = first;
            auto it = move_bound;
            for (; it != pivot_element; ++it)
            {
                if (compare(it, pivot_element))
                {
                    if (it != move_bound)
                    {
                        std::swap(*move_bound, *it);
                    }
                    ++move_bound; //increment bound of moved elements
                }
            }
            // Move pivot to the upper bound of moved elements
            if (pivot_element != move_bound)
            {
                std::swap(*move_bound, *pivot_element);
                pivot_element = move_bound;
            }
            
            // Resolve the right-side:
            //  - move all less data as pivot near pivot
            it = move_bound;
            ++it;
            for (; it != last; ++it)
            {
                // When pivot is greater, or equal than element: move element into the lower bound
                if (compare(it, pivot_element))
                {
                    ++move_bound; //increment bound of moved elements
                    if (it != move_bound)
                    {
                        std::swap(*move_bound, *it);
                    }
                }
            }
            // Move pivot to the upper bound of moved elements
            if (pivot_element != move_bound)
            {
                std::swap(*move_bound, *pivot_element);
                pivot_element = move_bound;
            }
            
            if (pivot_element != first)
            {
                // Sort partition before pivot
                sort(first, pivot_element, compare);
            }
            if (pivot_element != last)
            {
                // Sort partition after pivot
                sort(++pivot_element, last, compare);
            }
        }
    }

private:
    /** \brief Functor for pivot selection
    */
    template<typename Iterator_, 
             typename Compare_,
             Pivot pivot_>
    struct pivot_selection
    {
        /** \brief Selecting a pivot element for a partition
          * \param[in] first - lower bound of partition
          * \param[in] last - upper bound of partition
          * \returns iterator to pivot element
          */
        Iterator operator()(Iterator first,
                            Iterator last);
    };

    
    template<typename Iterator_, 
             typename Compare_>
    struct pivot_selection<Iterator_, Compare_, Pivot::rand>
    {
        /** \brief Selecting a random pivot element for a partition by random access iterators
          * \param[in] first - lower bound of partition
          * \param[in] last - upper bound of partition
          * \returns iterator to pivot element
          */
        template<typename Iterator__ = Iterator_>
        typename std::enable_if<
                 std::is_same<typename std::iterator_traits<Iterator__>::iterator_category, 
                              std::random_access_iterator_tag>::value, Iterator_>::
        type operator()(Iterator__ first,
                        Iterator__ last)
        {
            // pivot is a random value between first .. last
            return (first + (std::rand() % (last - first)));
        }

        /** \brief Selecting a random pivot element for a partition by non random access iterators
          * \param[in] first - lower bound of partition
          * \param[in] last - upper bound of partition
          * \returns iterator to pivot element
          */
        template<typename Iterator__ = Iterator_>
        typename std::enable_if<
                 !(std::is_same<typename std::iterator_traits<Iterator__>::iterator_category, 
                                std::random_access_iterator_tag>::value), Iterator_>::
        type operator()(Iterator__ first,
                        Iterator__ last)
        {
            // pivot is a random value between first .. last
            size_t x = 0;
            auto it = first;
            for (; it != last; ++it)
            {
                x++;
            }
            x = (std::rand() % x);
            it = first;
            while(x--)
            {
                ++it;
            }
            return it;
        }
        
    };

    
    template<typename Iterator_, 
             typename Compare_>
    struct pivot_selection<Iterator_, Compare_, Pivot::first>
    {
        /** \brief Selecting first element as pivot element for a partition
          * \param[in] first - lower bound of partition
          * \param[in] last - upper bound of partition
          * \returns iterator to pivot element
          */
        Iterator_ operator()(Iterator_ first,
                             Iterator_)
        {
            // pivot is the first position
            return first;
        }
    };

    
    template<typename Iterator_, 
             typename Compare_>
    struct pivot_selection<Iterator_, Compare_, Pivot::last>
    {
        /** \brief Selecting last element as pivot element for a partition by bidirectional iterators
          * \param[in] first - lower bound of partition
          * \param[in] last - upper bound of partition
          * \returns iterator to pivot element
          */
        template<typename Iterator__ = Iterator_>
        typename std::enable_if<
                 (std::is_same<typename std::iterator_traits<Iterator__>::iterator_category, 
                                std::bidirectional_iterator_tag>::value ||
                  std::is_same<typename std::iterator_traits<Iterator__>::iterator_category, 
                                std::random_access_iterator_tag>::value),
                  Iterator_>::
        type operator()(Iterator__ first,
                        Iterator__ last)
        {
            // pivot is the last position
            return --last;
        }

        /** \brief Selecting last element as pivot element for a partition by forward iterators
          * \param[in] first - lower bound of partition
          * \param[in] last - upper bound of partition
          * \returns iterator to pivot element
          */
        template<typename Iterator__ = Iterator_>
        typename std::enable_if<
                 std::is_same<typename std::iterator_traits<Iterator__>::iterator_category, 
                               std::forward_iterator_tag>::value, Iterator_>::
        type operator()(Iterator__ first,
                        Iterator__ last)
        {
            auto pivot_element = first;
            if (first != last)
            {
                auto it = first;
                ++it;
                for (; it != last; ++it)
                {
                    ++pivot_element;
                }
            }
            // pivot is the last position
            return pivot_element;
        }        
    };

    
    template<typename Iterator_, 
             typename Compare_>
    struct pivot_selection<Iterator_, Compare_, Pivot::middle>
    {
        /** \brief Selecting middle element as pivot element for a partition by random access iterators
          * \param[in] first - lower bound of partition
          * \param[in] last - upper bound of partition
          * \returns iterator to pivot element
          */
        template<typename Iterator__ = Iterator_>
        typename std::enable_if<
                 std::is_same<typename std::iterator_traits<Iterator__>::iterator_category, 
                              std::random_access_iterator_tag>::value, Iterator_>::
        type operator()(Iterator__ first,
                        Iterator__ last)
        {
            // pivot is the middle position
            return first + (last - first) / 2;
        }

        /** \brief Selecting middle element as pivot element for a partition by non random access iterators
          * \param[in] first - lower bound of partition
          * \param[in] last - upper bound of partition
          * \returns iterator to pivot element
          */
        template<typename Iterator__ = Iterator_>
        typename std::enable_if<
                 (!std::is_same<typename std::iterator_traits<Iterator__>::iterator_category, 
                                std::random_access_iterator_tag>::value), Iterator_>::
        type operator()(Iterator__ first,
                        Iterator__ last)
        {
            // pivot is the middle position
            auto pivot_element = first;
            bool odd = false;
            for (auto it = first; it != last; ++it)
            {
                if (odd)
                {
                    ++pivot_element;
                }
                odd = !odd;
            }
            return pivot_element;
        }
        
    };

};


/** \brief Function for quick-sort with default pivot selection
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_quick_sort(Iterator first,
                     Iterator last,
                     Compare compare =
                     [](Iterator l, Iterator r)->bool 
                     {return *l < *r; }) noexcept
{
    // Automatically select the best pivot type based on iterator category
    constexpr Pivot pivot_type = 
        (std::is_same<typename std::iterator_traits<Iterator>::iterator_category,
         std::random_access_iterator_tag>::value
         ? Pivot::rand
         : (std::is_same<typename std::iterator_traits<Iterator>::iterator_category,
            std::bidirectional_iterator_tag>::value
            ? Pivot::last : Pivot::first));
    // Launch quick-sort    
    quick_sort<Iterator, Compare, pivot_type>()(first, last, compare);
}


/** \brief Function for quick-sort with random pivot selection
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_quick_sort_pr(Iterator first,
                        Iterator last,
                        Compare compare =
                        [](Iterator l, Iterator r)->bool 
                        {return *l < *r; }) noexcept
{
    quick_sort<Iterator, Compare, Pivot::rand>()(first, last, compare);
}


/** \brief Function for quick-sort with first element as pivot selection
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_quick_sort_pf(Iterator first,
                        Iterator last,
                        Compare compare =
                        [](Iterator l, Iterator r)->bool 
                        {return *l < *r; }) noexcept
{
    quick_sort<Iterator, Compare, Pivot::first>()(first, last, compare);
}


/** \brief Function for quick-sort with last element as pivot selection
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_quick_sort_pl(Iterator first,
                        Iterator last,
                        Compare compare =
                        [](Iterator l, Iterator r)->bool 
                        {return *l < *r; }) noexcept
{
    quick_sort<Iterator, Compare, Pivot::last>()(first, last, compare);
}


/** \brief Function for quick-sort with middle element as pivot selection
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_quick_sort_pm(Iterator first,
                        Iterator last,
                        Compare compare =
                        [](Iterator l, Iterator r)->bool 
                        {return *l < *r; }) noexcept
{
    quick_sort<Iterator, Compare, Pivot::middle>()(first, last, compare);
}


}
}

#endif //__EDU_SORTING_QUICKSORT_H__
