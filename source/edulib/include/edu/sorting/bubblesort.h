///////////////////////////////////////////////////////////////////////////////////////////////////
// Bubble-sort sample implementation 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.19.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_SORTING_BUBBLESORT_H__
#define __EDU_SORTING_BUBBLESORT_H__

#include <functional>

namespace edu
{
namespace sorting
{

/** \brief Functor for bubble-sort
  */
template<typename Iterator, 
         typename Compare = std::function<bool(Iterator, Iterator)>>
struct bubble_sort
{
    /** \brief Launcher for bubble-sort
      * \param[in] first - forward iterator, pointing to the lower bound of range to sort
      * \param[in] last - forward iterator, pointing to the upper bound of range to sort
      * \param[in] compare - compare function
      */
    void operator()(Iterator first,
                    Iterator last,
                    Compare compare = 
                    [](Iterator l, Iterator r)->bool
                    {return *l < *r;}) noexcept
    {
        if (first != last)
        {
            // Suppose range is unsorted
            bool sorted = false;
            // Repeat the iterations until range is sorted
            while (!sorted)
            {
                // Suppose container is sorted
                sorted = true;
                // Iterate until the pre-last element
                auto it_ = first; // iterator for previous element
                auto it = first;  // iterator for current element
                ++it;
                // Iterate from the 2nd element to end of range
                for (; it != last; ++it, ++it_)
                {
                    // Compare current with previous
                    if (compare(it, it_))
                    {
                        // When current is less than previous: swap them and mark the range as "unsorted"
                        std::swap(*(it), *(it_));
                        sorted = false;
                    }
                }
            }
        }
    }

};


/** \brief Function for bubble-sort
  * \param[in] first - forward iterator, pointing to the lower bound of range to sort
  * \param[in] last - forward iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_bubble_sort(Iterator first,
                      Iterator last,
                      Compare compare =
                      [](Iterator l, Iterator r)->bool
                      {return *l < *r; }) noexcept
{
    bubble_sort<Iterator, Compare>()(first, last, compare);
}


}
}

#endif //__EDU_SORTING_BUBBLESORT_H__
