///////////////////////////////////////////////////////////////////////////////////////////////////
// Heap-sort sample implementation 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.17.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_SORTING_HEAPSORT_H__
#define __EDU_SORTING_HEAPSORT_H__

#include <functional>
#include <algorithm>

namespace edu
{
namespace sorting
{

/** \brief Functor for heap-sort
  */
template<typename Iterator, 
         typename Compare = std::function<bool(Iterator, Iterator)>>
struct heap_sort
{
    /** \brief Launcher for heap-sort
      * \param[in] first - random access iterator, pointing to the lower bound of range to sort
      * \param[in] last - random access iterator, pointing to the upper bound of range to sort
      * \param[in] compare - compare function
      */
    void operator()(Iterator first,
                    Iterator last,
                    Compare compare = 
                    [](Iterator l, Iterator r)->bool
                    {return *l < *r;}) noexcept
    {
        if (first != last)
        {
            // Create a max heap - starting from the pre-last level of the piramyd
            for (auto it = first + (last - first) / 2; it != last; ++it)
            {
                heapify(first, first + (last - it) - 1, last, compare);
            }
            // Until each element placed to its right position
            auto last_ = last;
            while (last_ != first)
            {
                // Remove the top element, add to bottom and decrement size
                std::swap(*(--last_), *first);
                // Heapify - move the new top element into its right place
                heapify(first, first, last_, compare);
            }
        }
    }

private:
    /** \brief Transforms the container's partial content into a heap
      * \param[in] first - random access iterator, pointing to the lower bound of range of heap
      * \param[in] root - random access iterator, pointing to the root node position of heap segment
      * \param[in] last - random access iterator, pointing to the upper bound of range of heap
      * \param[in] compare - compare function
      */
    void heapify(Iterator first,
                 Iterator root,
                 Iterator last, 
                 Compare compare) noexcept  
    {
        // Suppose root node is the greater
        auto greater_ = root;
        // Check whether left child node is valid
        Iterator left_ = ( (2 * (root - first) + 1) < (last - first) ) 
                          ? (first + 2 * (root - first) + 1)
                          : last;
        // Check whether right child node is valid                        
        Iterator right_ = ( (2 * (root - first) + 2) < (last - first) )
                            ? (first + 2 * (root - first) + 2)
                            : last;
        // When left child is valid and is greater as current greater -> mark left child as greater,                     
        if ( (left_ != last) && (compare(greater_, left_)) )
        {
            greater_ = left_;
        }
        // When right child is valid and is greater as current greater -> mark right child as greater,                     
        if ( (right_ != last) && (compare(greater_, right_)))
        {
            greater_ = right_;
        }
        // If greater is not the root, swap it with root and resolve the heap below
        if (greater_ != root)
        {
            std::swap(*root, *greater_);
            // go down on the greater branch
            heapify(first, greater_, last, compare);
        }
    }
};


/** \brief Function for heap-sort
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] compare - compare function
  */
template<typename Iterator,
         typename Compare = std::function<bool(Iterator, Iterator)>>
void make_heap_sort(Iterator first,
                    Iterator last,
                    Compare compare =
                    [](Iterator l, Iterator r)->bool 
                    {return *l < *r; }) noexcept
{
    heap_sort<Iterator, Compare>()(first, last, compare);
}


}
}

#endif //__EDU_SORTING_HEAPSORT_H__
