///////////////////////////////////////////////////////////////////////////////////////////////////
// Various algorithms on strings
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.06.05.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_ALGORITHMS_STRINGS_H__
#define __EDU_ALGORITHMS_STRINGS_H__

#include <stdint.h>
#include <string>

namespace edu
{
namespace algorithms
{
namespace string
{

/** \brief Invert characters in a string words
    * \param[in/out] str - string with multiple words
    * \param[in] whiteSpaces - white-space characters for tokenizing the string in multiple words
    */
void invertWordChars(std::string& str, 
                     const std::string& whiteSpaces);

/** \brief Find longest character sequence in some string words
    * \param[int] str - input string
    * \param[out] pos - position of first character in the longest sequence
    * \param[out] len - length of the lkngest sequence
    */
void findLongestSequence(const std::string& str, 
                         int32_t& pos,
                         size_t& len);

/** \brief Checks whether characters are unique in a string
    * \param[int] str - input string
    * \param[out] pos - position of first duplicated character
    * \returns true, when characters are unique, otherwise false
    */
auto isUniqueChars(const std::string& str,
                   int32_t& pos)->bool;
auto isUniqueChars2(const std::string& str,
                    int32_t& pos)->bool;

}
}
}

#endif //__EDU_ALGORITHMS_STRINGS_H__
