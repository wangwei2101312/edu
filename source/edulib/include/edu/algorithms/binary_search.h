///////////////////////////////////////////////////////////////////////////////////////////////////
// Binary search algorithms
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.23.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_ALGORITHMS_BINARY_SEARCH_H__
#define __EDU_ALGORITHMS_BINARY_SEARCH_H__

#include <stdint.h>

namespace edu
{
namespace algorithms
{

    
/** \brief Functor for binary search returning single match
  */ 
template<typename Iterator,
         typename T,
         typename Compare = std::function<bool(const T&, const T&)>>    
struct binary_search
{
    /** \brief Launching operator
      * \param[in] first - random access iterator, pointing to the lower bound of range to sort
      * \param[in] last - random access iterator, pointing to the upper bound of range to sort
      * \param[in] key - key value to search
      * \param[in] compare - compare function
      * \returns iterator to found item, otherwise iterator pointing to end of range
      */
    Iterator operator()(Iterator first,
                        Iterator last,
                        const T& key,
                        Compare compare = 
                        [](const T& l, const T& r)->bool
                        {return l < r;})
    {
        // Start and points of search domain
        auto from = first;
        auto to = last;
        // Initialize middle point
        auto mid = first + (to - from) / 2;
        // Suppose no results 
        auto res = last;
        while (from < to)
        {
            // Compare middle element with key
            if (!compare(key, *mid))
            {
                // Middle position element is less, or equal than key
                if (!compare(*mid, key))
                {
                    // Key is less, or equal with middle position element -> result found
                    res = mid;
                    break;
                }
                // Middle position element is less than key -> go to the upper segment
                from = mid + 1;
            }
            else
            {   
                // Key is less than middle position element -> go to the lower segment
                to = mid;
            }
            // Take the middle position
            mid = from + (to - from) / 2;
        }
        // Return result
        return res;
    }
};


/** \brief Functor for binary search returning range of matched values
  */ 
template<typename Iterator,
         typename T,
         typename Compare = std::function<bool(const T&, const T&)>>    
struct binary_search_range
{
    /** \brief Launching operator
      * \param[in] first - random access iterator, pointing to the lower bound of range to sort
      * \param[in] last - random access iterator, pointing to the upper bound of range to sort
      * \param[in] key - key value to search
      * \param[in] compare - compare function
      * \returns pair of range with first, last elements
      */
    std::pair<Iterator, Iterator> operator()(Iterator first,
                                             Iterator last,
                                             const T& key,
                                             Compare compare = 
                                             [](const T& l, const T& r)->bool
                                             {return l < r;})
    {
        // Search for single result
        auto lower_bound = binary_search<Iterator, T, Compare>()(first, last, key, compare);
        auto upper_bound = lower_bound;
        if (lower_bound != last)
        {
            // Search lower bound
            while(     (lower_bound != first) 
                    && (!compare(key, *(lower_bound - 1)))
                    && (!compare(*(lower_bound - 1), key)) )
            {                
                lower_bound--;
            }

            // Search upper bound
            while(     (upper_bound != last) 
                    && (!compare(key, *upper_bound))
                    && (!compare(*upper_bound, key)) )
            {                
                upper_bound++;
            }            
        }
        // Return result
        return std::make_pair(lower_bound, upper_bound);
    }
};


/** \brief Function for binary search
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] key - key value to search
  * \param[in] compare - compare function
  * \returns iterator to found item, otherwise iterator pointing to end of range
  */
template<typename Iterator,
         typename T,
         typename Compare = std::function<bool(const T&, const T&)>>
Iterator make_binary_search(Iterator first,
                            Iterator last,
                            const T& key,
                            Compare compare = 
                            [](const T& l, const T& r)->bool
                            {return l < r;})
{
    return binary_search<Iterator, T, Compare>()(first, last, key, compare);
}


/** \brief Function for binary search range
  * \param[in] first - random access iterator, pointing to the lower bound of range to sort
  * \param[in] last - random access iterator, pointing to the upper bound of range to sort
  * \param[in] key - key value to search
  * \param[in] compare - compare function
  * \returns iterator pairs for lower and upper range of found results
  */
template<typename Iterator,
         typename T,
         typename Compare = std::function<bool(const T&, const T&)>>
std::pair<Iterator, Iterator> make_binary_search_range(Iterator first,
                                                       Iterator last,
                                                       const T& key,
                                                       Compare compare = 
                                                       [](const T& l, const T& r)->bool
                                                       {return l < r;})
{
    return binary_search_range<Iterator, T, Compare>()(first, last, key, compare);
}


}
}

#endif //__EDU_ALGORITHMS_BINARY_SEARCH_H__
