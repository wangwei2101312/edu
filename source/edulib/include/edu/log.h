///////////////////////////////////////////////////////////////////////////////////////////////////
// Educational sample applications
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.17.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_LOG_H__
#define __EDU_LOG_H__

#include <iostream>
#include <forward_list>
#include <algorithm>

namespace edu
{

/** \brief function returning log channels
  * \returns instance of log channels
  */
class LogChannels& logchannels();

/** \brief template function to log events into generic stream
  * \param[in] stream - stream where to write the event
  * \returns stream 
  */
template<typename S>
S& log_(S& stream)
{
    return stream;
}

/** \brief template function to log events into generic stream
  * \param[in] stream - stream where to write the event
  * \param[in] first - first event to log
  * \param[in] args - variadic events to log further, after first has been logged
  * \returns stream 
  */
template<typename S, typename T, typename ... Args>
S& log_(S& stream, T first, Args ... args)
{
    stream << first;
    return log_(stream, args ...);
}

/** \brief template function to print an information to the standard output
  * \param[in] first - first event to log
  * \param[in] args - variadic events to log further, after first has been logged
  * \returns standard output 
  */
template<typename T, typename ... Args>
LogChannels& logInfo_(T first, Args ... args)
{
    return log_(logchannels(), first, args ...);
}
template<typename T, typename ... Args>
LogChannels& logInfo(T first, Args ... args)
{
    return (logInfo_(first, args ...) << '\n');
}

/** \brief template function to print an error to the standard error console
  * \param[in] first - first event to log
  * \param[in] args - variadic events to log further, after first has been logged
  * \returns standard error console 
  */
template<typename T, typename ... Args>
LogChannels& logError_(T first, Args ... args)
{
    return log_(logchannels(), "Error: ", first, args ...);
}
template<typename T, typename ... Args>
LogChannels& logError(T first, Args ... args)
{
    return (logError_(first, args ...) << '\n');
}

/** \brief template function to print a warning to the standard error console
  * \param[in] first - first event to log
  * \param[in] args - variadic events to log further, after first has been logged
  * \returns standard error console 
  */
template<typename T, typename ... Args>
LogChannels& logWarning_(T first, Args ... args)
{
    return log_(logchannels(), "Warning: ", first, args ...);
}
template<typename T, typename ... Args>
LogChannels& logWarning(T first, Args ... args)
{
    return (logWarning_(first, args ...) << '\n');
}


/** \brief Log channels manager
  */
class LogChannels
{
    // Channel structure
    struct channel
    {
        channel(std::ostream& stream_);

        template <typename T>
        void operator << (const T& message)
        {
            stream << message;
        }
        template <typename T>
        void  operator << (const T* message)
        {
            stream << message;
        }
        std::ostream& stream;
    };
public:
    /** \brief Constructor
      */
    LogChannels();

public:
    /** \brief Remove all channels
      */
    void clear();

    /** \brief Add a stream type channel
      * \param[in] output - stream object instance
      */
    void add_channel(std::ostream& stream);

    /** \brief Operator for output
      * \param[in] message - message to log
      * \returns instance of current object
      */
    template <typename T>
    LogChannels& operator << (const T& message)
    {
        std::for_each(std::begin(channels), std::end(channels), [&message](auto& channel)
        {
            channel << message;
        });
        return *this;
    }
    template <typename T>
    LogChannels& operator << (const T* message)
    {
        std::for_each(std::begin(channels), std::end(channels), [&message](auto& channel)
        {
            channel << message;
        });
        return *this;
    }

private:
    std::forward_list<channel> channels;
};

}

#endif //__EDU_LOG_H__
