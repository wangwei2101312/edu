///////////////////////////////////////////////////////////////////////////////////////////////////
// Sample hash-multiset implementation
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.29.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_CONTAINERS_HASH_MULTISET_H__
#define __EDU_CONTAINERS_HASH_MULTISET_H__

#include <edu/containers/xhash.h>

namespace edu
{
namespace containers
{

/** \brief Sample hash multi-set implementation
  */
template <typename Key,
          typename HashFunc = hash_of<Key>,
          typename EqualToFunc = is_equal<Key>>
class HashMultiSet : public XHash<Key, void, HashFunc, EqualToFunc>
{
    using _XHash = XHash<Key, void, HashFunc, EqualToFunc>;
    using _Elem = typename _XHash::_Elem;
public:
    using const_iterator = typename _XHash::const_iterator;

public:
    /** \brief Constructor
      */
    HashMultiSet() = default;

    /** \brief Copy constructor
      * \param[in] other - instance where from copy
      */
    HashMultiSet(const HashMultiSet& other)
    : _XHash(other)
    {
    }

    /** \brief Move constructor
      * \param[in] other - instance where from move  
      */
    HashMultiSet(HashMultiSet&& other)
    : _XHash(std::move(other))
    {
    }

    /** \brief Destructor
      */
    virtual ~HashMultiSet() = default;

    // Disabled copy and move operators
    HashMultiSet& operator=(const HashMultiSet&) = delete;
    HashMultiSet& operator=(HashMultiSet&&) = delete;

public:
    /** \brief Insert a new key, value
      * \param[in] key - key to insert
      * \returns iterator pointing to the new key, value pair when succeeed, otherwise invalid iterator
      */
    const_iterator insert(const Key& key)
    {
        // Insert key, value after the iterator
        return this->_insert(key, this->_find(key));
    }

    /** \brief Erase element by key
      * \param[in] key - key for deletion
      * \returns iterator pointing to the previous element
      */
    const_iterator erase(const Key& key) noexcept
    {
        auto it = this->_find(key);
        while (it != this->_invalid() && (EqualToFunc()(key, *it)))
        {
            it = this->_erase(it);
        }
        return it;
    }

    /** \brief Erase element by iterator
      * \param[in] iterator - iterator pointing to the element to be deleted
      * \returns iterator pointing to the previous element
      */
    const_iterator erase(const const_iterator& it) noexcept
    {
        return this->_erase(it);
    }

    /** \brief Associative operator
      * \param[in] key - key for value search
      * \returns reference to value paired with key
      */
    const_iterator operator[](const Key& key)
    {
        // Insert to key and return iteratpr
        auto it = this->_find(key);
        if (it == this->_invalid())
        {
            it = this->_insert(key, this->_find(key));
        }
        return it;
    }

};

}
}

#endif //__EDU_CONTAINERS_HASH_MULTISET_H__
