///////////////////////////////////////////////////////////////////////////////////////////////////
// Abstract hash table implementation
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.29.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_CONTAINERS_XHASH_H__
#define __EDU_CONTAINERS_XHASH_H__

#include <vector>
#include <list>
#include <assert.h>
#include <algorithm>
#include <math.h>

namespace edu
{
namespace containers
{

/** \brief Sample compare function implementations
  */
template <typename Key>
struct is_equal
{
    /** \brief Functor for hashing
      * \param[in] key - key value
      * \returns a hash value
      */
    bool operator()(const Key& left, const Key& right) const noexcept
    {
        return std::equal_to<Key>()(left, right);
    }
};

/** \brief Sample hash function implementations
  */
template <typename Key>
struct hash_of
{
    /** \brief Functor for hashing
      * \param[in] key - key value
      * \returns a hash value
      */
    size_t operator()(const Key& key) const
    {
        return std::hash<Key>()(key);
    }
};

/** \brief Sample hash function for strings
  */
template <>
struct hash_of<std::string>
{
    size_t operator()(const std::string& key) const noexcept
    {
        size_t prime_a = 54059u;
        const size_t prime_b = 76963u;
        size_t res = 29u;
        std::for_each(key.begin(), key.end(), [&](const char val)
        {
            res = (res * prime_a) + val;
            prime_a *= prime_b;
        });
        return res;
    }
};

/** \brief Sample hash function for unsigned integers
  */
template <>
struct hash_of<size_t>
{
    size_t operator()(size_t key) const noexcept
    {
        size_t prime_a = 54059u;
        const size_t prime_b = 76963u;
        size_t res = 29u;
        while (key)
        {
            res = (res * prime_a) + (key & 0xff);
            key >>= 8;
            prime_a *= prime_b;
        }
        return res;
    }
};

/** \brief Sample hash table implementation
  */
template <typename Key,
          typename Value,  
          typename HashFunc,
          typename EqualToFunc>
class XHash
{
protected:
    using _Elem = typename std::conditional<std::is_void<Value>::value,
                                            Key,
                                            std::pair<const Key, Value>>::type;
    using _List = std::list<_Elem>;
    using _Elem_Ref = std::pair<typename _List::iterator, typename _List::iterator>;
    using _Vector = std::vector<_Elem_Ref>;

    using iterator = typename _List::iterator;
    using const_iterator = typename _List::const_iterator;
    using reverse_iterator = typename _List::reverse_iterator;
    using const_reverse_iterator = typename _List::const_reverse_iterator;

private:
    // Key extractor functor for the Elem_ structure
    template <typename Key_,
              typename Value_,
              typename Elem_,
              typename HashFunc_,
              typename EqualToFunc_>
    struct get_key
    {
        const Key_& operator()(const Elem_& elem)
        {
            return elem.first;
        }
    };
    // When Value_ is missing
    template <typename Key_,
              typename Elem_,
              typename HashFunc_,
              typename EqualToFunc_>
    struct get_key<Key_, void, Elem_, HashFunc_, EqualToFunc_>
    {
        const Key_& operator()(const Elem_& elem)
        {
            return elem;
        }
    };
    // Functor alias
    using _get_key = get_key<Key, Value, _Elem, HashFunc, EqualToFunc>;

protected:
    /** \brief Constructor
      */
    XHash()
    : m_lst()
    , m_vec()
    {
    }

    /** \brief Copy constructor
      * \param[in] other - hash table where from to move
      */
    XHash(const XHash& other)
    : m_lst(other.m_lst)
    , m_vec(other.m_vec)
    {
    }

    /** \brief Move constructor
      * \param[in] other - hash table where from to move 
      */
    XHash(XHash&& other)
    : m_lst(std::move(other.m_lst))
    , m_vec(std::move(other.m_vec))
    {
    }

    /** \brief Destructor
      */
    ~XHash() = default;

    // Disabled copy and move operators
    XHash& operator=(const XHash&) = delete;
    XHash& operator=(XHash&&) = delete;

public:
    /** \brief Clears the container
      */
    void clear() noexcept
    {
        m_lst.clear();
        for (auto& ref : m_vec)
        {
            ref = std::move(_Elem_Ref(m_lst.end(), m_lst.end()));
        }
    }

    /** \brief Finds a key
      * \param[in] key - key to find
      * \returns valid iterator when key is found, otherwise invalid iterator
      */
    iterator find(const Key& key) noexcept
    {
        return _find(key);
    }
    const_iterator find(const Key& key) const noexcept
    {
        return _find(key);
    }

    /** \brief Iterator to the begin of container
      * \returns iterator pointing to the first element
      */
    iterator begin() noexcept
    {
        return m_lst.begin();
    }
    const_iterator begin() const noexcept
    {
        return m_lst.begin();
    }

    /** \brief Iterator to the end of container
      * \returns invalid iterator
      */
    iterator end() noexcept
    {
        return m_lst.end();
    }
    const_iterator end() const noexcept
    {
        return m_lst.end();
    }

    /** \brief Reverse iterator to the begin of container
      * \returns iterator pointing to the first element
      */
    reverse_iterator rbegin() noexcept
    {
        return m_lst.rbegin();
    }
    const_iterator rbegin() const noexcept
    {
        return m_lst.rbegin();
    }

    /** \brief Reverse Iterator to the end of container
      * \returns invalid iterator
      */
    reverse_iterator rend() noexcept
    {
        return m_lst.rend();
    }
    const_reverse_iterator rend() const noexcept
    {
        return m_lst.rend();
    }

    /** \brief Query the size of container
      * \returns elements count
      */
    size_t size() const noexcept
    {
        return m_lst.size();
    }

    /** \brief Capacity of container
      * \returns buckets count for hash values
      */
    size_t capacity() const noexcept
    {
        return m_vec.size();
    }

    /** \brief Checks whther container has valid elements
      * \returns true, when container has no valid elements, otherwise false
      */
    bool empty() const noexcept
    {
        return (m_lst.size() == 0);
    }

protected:
    /** \brief Determine the next upper prime number for the new size
      * \param[in] size - new size
      * \returns new size as prime number
      */
    auto _getprim(size_t size) noexcept->decltype(size)
    {
        assert(size > 0);
        size_t res = (size - 1);
        bool isPrim;
        do
        {
            isPrim = true;
            res++;
            for (size_t div = 2; div < sqrt(res); div++)
            {
                if ((res % div) == 0)
                {
                    isPrim = false;
                    break;
                }
            }
        } while (!isPrim);

        return res;
    }

    /** \brief Reserve space for the hash table
      * \param[in] size - new size
      */
    void _reserve(size_t size)
    {
        // Ensure the vector size is enough for the number of elements considering the fill factor
        if (size > m_vec.size())
        {
            // Determine capacity according to new size and fill-up factor
            size_t capacity = _getprim(std::max<size_t>(30, size));
            // Re-insert elements into the buckets and data containers (hashing changes due to changed capacity)
            _Vector vec(capacity, _Elem_Ref(m_lst.end(), m_lst.end()));
            _reinsert(vec);
            // Exchange bucket containers
            std::swap(m_vec, vec);
        }
    }

    /** \brief Determine the corresponding bucket in the hash table for a key
      * \param[in] vec - buckets container
      * \param[in] key - key for the bucket
      * \returns bucket iterator
      */
    _Elem_Ref& _bucket(_Vector& vec, 
                       const Key& key) noexcept
    {
        assert(vec.size() > 0);
        // Retrieve hash key
        size_t hash_ = HashFunc()(key);
        // Return bucket element for the hash key
        return vec[(hash_ % vec.size())];
    }

    /** \brief Finds a key
      * \param[in] key - key to find
      * \returns valid iterator when key is found, otherwise invalid iterator
      */
    iterator _find(const Key& key) noexcept
    {
        iterator res = m_lst.end();
        if (m_lst.size())
        {
            auto& elem_ = _bucket(m_vec, key);
            for (auto it = elem_.first; it != m_lst.end(); ++it)
            {
                if (EqualToFunc()(_get_key()(*it), key))
                {
                    res = it;
                    break;
                }
                if (it == elem_.second)
                {
                    break;
                }
            }
        }
        return res;
    }

    /** \brief Insert
      * \param[in] element - element to insert
      * \returns iterator pointing to the new key, value pair when succeeed, otherwise invalid iterator
      */
    iterator _insert(const _Elem& elem)
    {
        return _insert(elem, m_lst.end() );
    }

    /** \brief Insert
      * \param[in] element - element to insert
      * \param[in] pos - iterator to position where to insert
      * \returns iterator pointing to the new key, value pair when succeeed, otherwise invalid iterator
      */
    iterator _insert(const _Elem& elem,
                     const_iterator pos)
    {
        // Check if buckets container need to be resized
        if (m_lst.size() >= m_vec.size())
        {
            // Reserve double
            _reserve(m_vec.size() ? (m_vec.size() * 2) : 1);
        }
        auto& elem_ = _bucket(m_vec, _get_key()(elem));
        if (pos == m_lst.end())
        {
            elem_.first = m_lst.insert(elem_.first, elem);
        }
        else
        {
            auto it = m_lst.insert(pos, elem);
            if (pos == elem_.first)
            {
                elem_.first = it;
            }
        }

        if (elem_.second == m_lst.end())
        {
            elem_.second = elem_.first;
        }
        return elem_.first;
    }

    /** \brief Erase element by iterator
      * \param[in] iterator - iterator pointing to the element to be deleted
      * \returns iterator pointing to the previous element
      */
    iterator _erase(const const_iterator& it) noexcept
    {
        // Determine element to delete from bucket
        auto& elem_ = _bucket(m_vec, _get_key()(*it));
        // Resolve bucket's iterators
        if (elem_.first == elem_.second)
        {
            // When one iterator only - invalidate both
            elem_.first = m_lst.end();
            elem_.second = m_lst.end();
        }
        else
        {
            // When different iterators - check whether any of them matched
            if (elem_.first == it)
            {
                // When first iterator is matched - increment it
                elem_.first++; 
            }
            else if (elem_.second == it)
            {
                // When second iterator is matched - decrement it
                elem_.second--;
            }
        }
        // Delete the element form the storage list
        auto it_ = m_lst.erase(it);
        return it_;
    }

    /** \brief Reinsert elements into a bucket container
      * \param[in] vec - buckets container
      */
    void _reinsert(_Vector& vec)
    {
        // Take the original list and reinsert elements into the new data and bucket containers
        for (auto it = m_lst.begin(); it != m_lst.end(); ++it)
        {
            // Get the new bucket for element
            auto& elem_ = _bucket(vec, _get_key()(*it));
            if (elem_.second == m_lst.end())
            {
                // Add first element into the bucket
                elem_.first = it;
                elem_.second = it;
            }
            else
            {
                // Add next element into the bucket
                // Keep the next position in the list for later use
                auto it_(++it);
                // Move the element into the front of buckets first element, than mark it as "first"
                m_lst.splice(elem_.first, m_lst, (--it));
                elem_.first = it;
                // Reposition iterator to the original position
                it = (--it_);
            }
        }
    }
    
    
    /** \brief Invalid iterator
      * \returns upper bound of stored data
      */
    iterator _invalid()
    {
        return m_lst.end();
        
    }
    const_iterator _invalid() const
    {
        return m_lst.end();
        
    }

private:
    _List    m_lst;  // list for storing valid elements
    _Vector  m_vec;  // vector for hash buckets
};

}
}

#endif //__EDU_CONTAINERS_XHASH_H__
