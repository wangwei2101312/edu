///////////////////////////////////////////////////////////////////////////////////////////////////
// Sample hash-map implementation
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.17.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_CONTAINERS_HASH_MAP_H__
#define __EDU_CONTAINERS_HASH_MAP_H__

#include <edu/containers/xhash.h>

namespace edu
{
namespace containers
{

/** \brief Sample hash map implementation
  */
template <typename Key,
          typename Value,
          typename HashFunc = hash_of<Key>,
          typename EqualToFunc = is_equal<Key>>
class HashMap : public XHash<Key, Value, HashFunc, EqualToFunc>
{
    using _XHash = XHash<Key, Value, HashFunc, EqualToFunc>;
    using _Elem = typename _XHash::_Elem;
public:
    using iterator = typename _XHash::iterator;

public:
    /** \brief Constructor
      */
    HashMap() = default;

    /** \brief Copy constructor
      * \param[in] other - instance where from copy
      */
    HashMap(const HashMap& other)
    : _XHash(other)
    {
    }

    /** \brief Move constructor
      * \param[in] other - instance where from move  
      */
    HashMap(HashMap&& other)
    : _XHash(std::move(other))
    {
    }

    /** \brief Destructor
      */
    virtual ~HashMap() = default;

    // Disabled copy and move operators
    HashMap& operator=(const HashMap&) = delete;
    HashMap& operator=(HashMap&&) = delete;

public:
    /** \brief Insert a new key, value
      * \param[in] key - key to insert
      * \param[in] val - value to insert
      * \returns iterator pointing to the new key, value pair when succeeed, otherwise invalid iterator
      */
    iterator insert(const Key& key,
                    const Value& val)
    {
        auto it_ = this->_invalid();
        if (this->_find(key) == this->_invalid())
        {
            it_ = this->_insert(_Elem(key, val));
        }
        return it_;
    }

    /** \brief Erase element by key
      * \param[in] key - key for deletion
      * \returns iterator pointing to the previous element
      */
    iterator erase(const Key& key) noexcept
    {
        auto it = this->_find(key);
        if (it != this->_invalid())
        {
            it = this->_erase(it);
        }
        return it;
    }

    /** \brief Erase element by iterator
      * \param[in] iterator - iterator pointing to the element to be deleted
      * \returns iterator pointing to the previous element
      */
    iterator erase(const iterator& it) noexcept
    {
        return this->_erase(it);
    }

    /** \brief Associative operator
      * \param[in] key - key for value search
      * \returns reference to value paired with key
      */
    iterator operator[](const Key& key)
    {
        // Search for key
        auto it = this->_find(key);
        if (it == this->_invalid())
        {
            // When key not found, insert it with empty value
            it = this->_insert( _Elem(key, Value()) );
        }
        // Return iterator
        return it;
    }

};

}
}

#endif //__EDU_CONTAINERS_HASH_MAP_H__
