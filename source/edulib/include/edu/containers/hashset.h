///////////////////////////////////////////////////////////////////////////////////////////////////
// Sample hash-set implementation
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.17.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_CONTAINERS_HASH_SET_H__
#define __EDU_CONTAINERS_HASH_SET_H__

#include <edu/containers/xhash.h>

namespace edu
{
namespace containers
{

/** \brief Sample hash-set implementation
  */
template <typename Key,
          typename HashFunc = hash_of<Key>,
          typename EqualToFunc = is_equal<Key>>
class HashSet : public XHash<Key, void, HashFunc, EqualToFunc>
{
    using _XHash = XHash<Key, void, HashFunc, EqualToFunc>;
    using _Elem = typename _XHash::_Elem;
public:
    using const_iterator = typename _XHash::const_iterator;

public:
    /** \brief Constructor
      */
    HashSet() = default;

    /** \brief Copy constructor
      * \param[in] other - instance where from copy
      */
    HashSet(const HashSet& other)
    : _XHash(other)
    {
    }

    /** \brief Move constructor
      * \param[in] other - instance where from move  
      */
    HashSet(HashSet&& other)
    : _XHash(std::move(other))
    {
    }

    /** \brief Destructor
      */
    virtual ~HashSet() = default;

    // Disabled copy and move operators
    HashSet& operator=(const HashSet&) = delete;
    HashSet& operator=(HashSet&&) = delete;

public:
    /** \brief Insert a new key, value
      * \param[in] key - key to insert
      * \returns iterator pointing to the new key, value pair when succeeed, otherwise invalid iterator
      */
    const_iterator insert(const Key& key)
    {
        auto it_ = this->_invalid();
        if (this->_find(key) == this->_invalid())
        {
            it_ = this->_insert(key);
        }
        return it_;
    }

    /** \brief Erase element by key
      * \param[in] key - key for deletion
      * \returns iterator pointing to the previous element
      */
    const_iterator erase(const Key& key) noexcept
    {
        auto it = this->_find(key);
        if (it != this->_invalid())
        {
            it = this->_erase(it);
        }
        return it;
    }

    /** \brief Erase element by iterator
      * \param[in] it - iterator pointing to the element to be deleted
      * \returns iterator pointing to the previous element
      */
    const_iterator erase(const const_iterator& it) noexcept
    {
        return this->_erase(it);
    }

    /** \brief Associative operator
      * \param[in] key - key for value search
      * \returns reference to value paired with key
      */
    const_iterator operator[](const Key& key)
    {
        // Search for key
        auto it = this->_find(key);
        if (it == this->_invalid())
        {
            // When key not found, insert it with empty value
            it = this->_insert(key);
        }
        // Return iterator
        return it;
    }

};

}
}

#endif //__EDU_CONTAINERS_HASH_SET_H__
