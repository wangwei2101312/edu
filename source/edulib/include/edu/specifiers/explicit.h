///////////////////////////////////////////////////////////////////////////////////////////////////
// Finding a sequence in a list with dynamic programming
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.17.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_SPECIFIERS_EXPLICIT_H__
#define __EDU_SPECIFIERS_EXPLICIT_H__

#include <edu/log.h>

namespace edu
{
namespace specifiers
{

/** \brief Template class for compositing with implicit/explicit conversion
 */
template<typename Composite>
class Explicit
{
public:
    /** \brief Copy constructor
      * \param[in] other - instance wehre from copy
      */
    Explicit(const Explicit& other)
    : m_comp(other.m_comp)
    {
        logInfo("Copy constructor Explicit(const Explicit&)");
    }

    /** \brief Constructor, enabled for implicit conversion
      * \param[in] comp - implementation
      */
    Explicit(Composite comp) 
    : m_comp(comp)
    {
        logInfo("Constructor with implicit conversion Explicit(", typeid(Composite).name(), ")");
    }

    /** \brief Constructor, enabled for implicit conversion
    * \param[in] s - string value
    */
    template<typename Something>
    Explicit(const Something& s) 
    : m_comp(s)
    {
        logInfo("Constructor with implicit conversion const Explicit(", typeid(Something).name(), "&)");
    }

    /** \brief Constructor, disabled for implicit conversion
    * \param[in] s - pointer value
    */
    template<class Something>
    explicit Explicit(const Something* s) 
    : m_comp(*s)
    {
        logInfo("Constructor with implicit conversion Explicit(", typeid(s).name(), ")");
    }

    /** \brief Destructor, disabled for implicit conversion
    * \param[in] a - character array value
    */
    ~Explicit() = default;

    // Disabled constructors and operators
    Explicit& operator=(const Explicit&) = delete;
    Explicit& operator=(Explicit&&) = delete;

public:
    /** \brief Dumps the value in A
    * \returns an integer value
    */
    auto dump()->Composite const
    {
        return m_comp;
    }

private:
    Composite m_comp; // composited value
};

}
}

#endif //__EDU_SPECIFIERS_EXPLICIT_H__
