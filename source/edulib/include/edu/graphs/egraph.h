///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_GRAPHS_EGRAPH_H__
#define __EDU_GRAPHS_EGRAPH_H__

#include <edu/graphs/xgraph.h>


namespace edu
{
namespace graph
{

/** \brief Edge graph: a vertex is a directed edge
  *  Useful for modelling real world edge based graphs, where each vertex->edges->vertices are modelled 
  *  as: source vertex = edge-end, 
  *      connection edges = the start of connected edges in the another end 
  *      target vertices = start of connected edges in the another end of the current edge
  *  See further details in the description of _XGraph
  */
template <typename Vertex,
          typename Edge = void>
class EGraph : public XGraph<Vertex, Edge, false, true>
{
    using _XGraph = XGraph<Vertex, Edge, false, true>; 
public:
    /** \brief Constructor
      */
    EGraph() = default;

    /** \brief Constructor
      * \param[in] vertices - number of vertices to be supported
      */
    EGraph(size_t vertices)
    : _XGraph(vertices)
    {
    }
        
public:
    /** \brief Adds a connection into the graph
      * The vertex indexes must fit
      * \param[in] vertex - reference vertex index
      * \param[in] connected_vertex - connected vertex index
      * \param[in] connect_is_forward - connected vertex goes in forward direction
      * \param[in] forward - when true, sets forward connections, otherwise backward connections
      * \param[in] edge - connection edge object
      */
    template<typename T = void, 
             typename Edge_ = Edge>
    typename std::enable_if<!std::is_void<Edge>::value, T>::
    type set_edge(size_t vertex,
                  size_t connected_vertex,
                  bool connect_is_forward,
                  bool forward,
                  const Edge_& edge)
    {
        _set_edge(vertex, connected_vertex, connect_is_forward, forward, edge);
    }

    /** \brief Adds a connection into the graph
      * The vertex indexes must fit
      * \param[in] vertex - reference vertex index
      * \param[in] connected_vertex - connected vertex index
      * \param[in] connect_is_forward - connected vertex goes in forward direction
      * \param[in] forward - when true, sets forward connections, otherwise backward connections
      */
    template<typename T = void> 
    typename std::enable_if<std::is_void<Edge>::value, T>::
    type set_edge(size_t vertex,
                  size_t connected_vertex,
                  bool connect_is_forward,
                  bool forward)
    {
        _set_edge(vertex, connected_vertex, connect_is_forward, forward);
    }

    /** \brief Get connected edges for a given vertex
      * \param[in] vertex - iterator to vertex
      * \param[in] forward - when true, returns forward connections, otherwise backward connections
      * \returns pair of (first, last) iterators to vertex edges
      */
    typename _XGraph::_Vertex_Edges
        get_vertex_edges(typename _XGraph::iterator vertex,
                         bool forward) const noexcept
    {
        return _get_vertex_edges(vertex, forward);
    }

private:
    /** \brief Adds a connection into the graph
      * The vertex indexes must fit
      * \param[in] vertex - reference vertex index
      * \param[in] connected_vertex - connected vertex index
      * \param[in] connect_is_forward - connected vertex goes in forward direction
      * \param[in] forward - when true, sets forward connections, otherwise backward connections
      * \param[in] edge - connection edge object
      */
    template<typename T = void, 
             typename Edge_ = Edge>
    typename std::enable_if<!std::is_void<Edge>::value, T>::
    type _set_edge(size_t vertex,
                   size_t connected_vertex,
                   bool connect_is_forward,
                   bool forward,
                   const Edge_& edge)
    {
        assert((vertex < this->m_vertices.size()) &&
               (connected_vertex < this->m_vertices.size()));
        if (forward)
        {
            // Add forward (non-reference node) connection for vertex 
            // without additional edge information (e.g, transition penalties)
            std::get<0>(this->m_connections[vertex]).push_front(
                std::move(typename _XGraph::_Edge
                    (std::make_pair(connected_vertex, connect_is_forward), edge)));
        }
        else
        {
            // Add backward (reference node) connection for vertex 
            // without additional edge information (e.g, transition penalties)
            std::get<1>(this->m_connections[vertex]).push_front(
                std::move(typename _XGraph::_Edge
                    (std::make_pair(connected_vertex, connect_is_forward), edge)));
        }
    }
    
    /** \brief Adds a connection into the graph
      * The vertex indexes must fit
      * \param[in] vertex - reference vertex index
      * \param[in] connected_vertex - connected vertex index
      * \param[in] connect_is_forward - connected vertex goes in forward direction
      * \param[in] forward - when true, sets forward connections, otherwise backward connections
      */
    template<typename T = void>
    typename std::enable_if<std::is_void<Edge>::value, T>::
    type _set_edge(size_t vertex,
                   size_t connected_vertex,
                   bool connect_is_forward,
                   bool forward)
    {
        assert((vertex < this->m_vertices.size()) &&
               (connected_vertex < this->m_vertices.size()));
        if (forward)
        {
            // Add forward (non-reference node) connection for vertex 
            // with additional edge information (e.g, transition penalties)
            std::get<0>(this->m_connections[vertex]).push_front(
                std::move(typename _XGraph::_Edge
                (std::make_pair(connected_vertex, connect_is_forward))));
        }
        else
        {
            // Add backward (reference node) connection for vertex 
            // with additional edge information (e.g, transition penalties)
            std::get<1>(this->m_connections[vertex]).push_front(
                std::move(typename _XGraph::_Edge
                (std::make_pair(connected_vertex, connect_is_forward))));
        }
    }

    /** \brief Get connected edges for a given vertex
      * \param[in] vertex - iterator to vertex
      * \param[in] forward - when true, returns outgoing edges, otherwise incoming edges
      * \returns pair of (first, last) iterators to vertex edges
      */
    typename _XGraph::_Vertex_Edges
            _get_vertex_edges(typename _XGraph::iterator vertex,
                              bool forward ) const noexcept
    {
        return forward 
            ? // return iterator to first and last elements of connections in non-reference node
            typename _XGraph::_Vertex_Edges
            (
                std::begin(std::get<0>(this->m_connections[vertex - std::begin(this->m_vertices)])),
                std::end(std::get<0>(this->m_connections[vertex - std::begin(this->m_vertices)]))
            )
            : // return iterator to first and last elements of connections in reference node
            typename _XGraph::_Vertex_Edges
            ( 
                std::begin(std::get<1>(this->m_connections[vertex - std::begin(this->m_vertices)])),
                std::end(std::get<1>(this->m_connections[vertex - std::begin(this->m_vertices)]))
            );
    }

};

}
}

#endif //__EDU_GRAPHS_EGRAPH_H__
