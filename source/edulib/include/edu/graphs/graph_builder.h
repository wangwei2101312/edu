///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_GRAPHS_GRAPH_BUILDER_H__
#define __EDU_GRAPHS_GRAPH_BUILDER_H__

#include <edu/graphs/graph_traits.h>
#include <functional>
#include <algorithm>
#include <deque>
#include <unordered_map>

namespace edu
{
namespace graph
{

/** \brief List of builder message types for notifying
  */
enum class BuilderMessage
{
    none = 0,
    vertex_exists,
    edge_exists,
    edge_is_loop,
    vertex_is_missing,
    no_connection
};


/** \brief Abstract graph builder: collects edges and vertices, builds a connected graph 
  * by indexing edges and vertices, providing back indexed edges, with their connected edge indexes
  * and indexed vertices with their connected edge indexes.
  * Adding vertices explicitly is not necessary - the builder can generate vertices from edges by option 
  */
template <typename Vertex,
          typename Edge,
          template<typename, typename, bool>class EdgeToVertex = edge_to_vertex,
          template<typename>class HashOf = hash_of,
          template<typename>class IsEqual = is_equal>
class GraphBuilder
{
    // Edges container
    using _Edges = std::unordered_map<Edge, size_t, HashOf<Edge>, IsEqual<Edge>>;
    // Vertices container
    using _Vertices = std::unordered_map<Vertex, size_t, HashOf<Vertex>, IsEqual<Vertex>>;
    // Edge-Edge index pair
    using _Indexed_Edge = std::pair<Edge, size_t>;
    // Vertex-Vertex index pair
    using _Indexed_Vertex = std::pair<Vertex, size_t>;
    // Edge pointer-Vertex index pair
    using _Edge_Vertex = std::pair<typename _Edges::iterator, size_t>;
    // Connection table of vertices -> edges
    using _EdgeVertices = std::deque<_Edge_Vertex>;
    // Container for connection indexes by edges
    using _Edge_Connection = std::pair<size_t, bool>;
    using _Edge_Connections = std::deque<_Edge_Connection>;
    // Container type for sorting builder entities
    template<typename T>
    using _Entity_Sorter = std::deque<T>;

public:
    /** \brief Constructor
      */
    GraphBuilder() = default;
    
    /** \brief Destructor
      */
    virtual ~GraphBuilder() = default;
    
public:
    /** \brief Appends a vertex to the builder
      * \param[in] vertex - vertex object
      * \param[in] notifier - function pointer to notify about insertion failure.
      *                       The arguments are the vertex and message type
      *                       by returning true -> the insertion stops
      * \returns true, when insertion succeed, otherwise false
      */
    template<typename Notifier = std::function<bool(const Vertex&, BuilderMessage)>>
    bool push_vertex(const Vertex& vertex,
                     Notifier notifier = 
                         [](const Vertex&, BuilderMessage)->bool{return false;})
    {
        bool res = true;
        // Add vertex to the collection
        BuilderMessage msg = _push_vertex(vertex);
        // When insertion failed -> notify vertex and reason
        if (BuilderMessage::none != msg)
        {
            notifier(vertex, msg);
            res = false;
        }
        return res;
    }

    /** \brief Appends a list of vertices to the builder
      * \param[in] first - start of range
      * \param[in] last - end of of range
      * \param[in] notifier - function pointer to notify about insertion failure.
      *                       The arguments are the vertex and message type
      *                       by returning true -> the insertion stops
      * \returns number of inserted vertices
      */
    template<typename Iterator,
             typename Notifier = std::function<bool(const Vertex&, BuilderMessage)>>
    size_t push_vertices(Iterator first, 
                         Iterator last,
                         Notifier notifier = 
                             [](const Vertex&, BuilderMessage)->bool{return false;}) 
    {
        size_t res = 0;
        for( auto it = first; it != last; ++it)
        {
            // Add vertex to the collection
            BuilderMessage msg = _push_vertex(*it);
            // When insertion failed -> notify vertex and reason
            if (BuilderMessage::none != msg)
            {
                if (notifier(*it, msg))
                {
                    // Notifier returned true -> abort
                    break; 
                }
                // Avoid to increment counter
                continue;
            }
            // Increment count of inserted vertices
            res++; 
        };
        // Returns the inserted vertex count
        return res;
    }

    /** \brief Appends an edge to the builder
      * \param[in] edge - edge object
      * \param[in] generate_vertex - when true, generates vertexes from edge and stores them
      * \param[in] notifier - function pointer to notify about insertion failure.
      *                       The arguments are the edge and message type
      *                       by returning true -> the insertion stops
      * \returns true, when insertion succeed, otherwise false
      */
    template<typename Notifier = std::function<bool(const Edge&, BuilderMessage)>>
    bool push_edge(const Edge& edge,
                   bool generate_vertex = true,  
                   Notifier notifier = 
                       [](const Edge&, BuilderMessage)->bool{return false;})
    {
        bool res = true;
        // Add edge to the collection
        BuilderMessage msg = _push_edge(edge, generate_vertex);
        // When insertion failed -> notify edge and reason
        if (BuilderMessage::none != msg)
        {
            notifier(edge, msg);
            res = false;
        }
        return res;
    }

    /** \brief Appends a list of edges to the builder
      * \param[in] first - start of range
      * \param[in] last - end of of range
      * \param[in] generate_vertex - when true, generates vertexes from edge and stores them
      * \param[in] notifier - function pointer to notify about insertion failure.
      *                       The arguments are the edge and message type
      *                       by returning true -> the insertion stops
      * \returns number of inserted edges
      */
    template<typename Iterator,
             typename Notifier = std::function<bool(const Edge&, BuilderMessage)>>
    size_t push_edges(Iterator first, 
                      Iterator last,
                      bool generate_vertex = true,  
                      Notifier notifier = 
                          [](const Edge&, BuilderMessage)->bool{return false;}) 
    {
        size_t res = 0;
        for( auto it = first; it != last; ++it)
        {
            // Add edge to the collection
            BuilderMessage msg = _push_edge(*it, generate_vertex);
            // When insertion failed -> notify edge and reason
            if (BuilderMessage::none != msg)
            {
                if (notifier(*it, msg))
                {
                    // Notifier returned true -> abort
                    break; 
                }
                // Avoid to increment counter
                continue;
            }
            // Increment count of inserted edges
            res++; 
        };
        // Returns the inserted edge count
        return res;
    }

    /** \brief Sort builder vertices by an external criteria
      * \param[in] compare - function for external criteria
      */
    template<typename vertex_compare = std::function<bool(const Vertex&, const Vertex&)>>
    void sort_vertices(vertex_compare compare = [](const Vertex& l, const Vertex& r)->bool {return l < r;})
    {
        _sort_entity(m_vertices, compare);
    }

    /** \brief Sort builder edges by an external criteria
      * \param[in] compare - function for external criteria
      */
    template<typename edge_compare = std::function<bool(const Edge&, const Edge&)>>
    void sort_edges(edge_compare compare = [](const Edge& l, const Edge& r)->bool {return l < r; })
    {
        _sort_entity(m_edges, compare);
    }

    /** \brief Pulls the raw vertices
      * \param[in] adapter - adapter to convert builder vertices into graph vertices and adding it to a graph
      * \returns true, when succeed, otherwise false
      */
    template <typename Adapter = graph_adapter<Vertex, Edge>>
    bool pull_vertices(Adapter& adapter)
    {
        return _pull_vertices(adapter);
    }

    /** \brief Pulls the raw edges
      * \param[in] adapter - adapter to convert builder edges into graph edges and adding it to a graph
      * \returns true, when succeed, otherwise false
      */
    template <typename Adapter = graph_adapter<Vertex, Edge>>
    bool pull_edges(Adapter& adapter)
    {
        return _pull_edges(adapter);
    }

    /** \brief Pulls the connected edges, or vertex graph, built from the actual edges and vertices of the builder
      * \param[in] adapter - adapter to convert builder edges into graph edges and adding it to a graph
      * \param[in] vertex_graph - option to specify, whether connections shall be vertex, or edge indexes
      * \param[in] notifier - function pointer to notify about missing connections
      *                       The arguments are the edge, message type and node type ("true" means reference node), 
      *                       by returning true -> the pulling process is cancelled
      * \returns true, when succeed, otherwise false
      */
    template <template <typename, typename, bool>class _EdgeToVertex = EdgeToVertex,
                        typename Notifier = std::function<bool(const Edge&, BuilderMessage, bool)>,
                        typename Adapter = graph_adapter<Vertex, Edge>>
    bool pull_connected_graph(Adapter& adapter,
                              bool vertex_graph,
                              Notifier notifier = 
                                  [](const Edge&, BuilderMessage, bool)->bool 
                                     {return false;})
    {
        return (vertex_graph
            ? // client is a vertex based graph, wants vertex->edge->vertex information
            _pull_connected_vertices<_EdgeToVertex, Adapter, Notifier>(adapter, notifier)
            : // client is an edge based graph, wants edge with forward/backward edges as connections
            _pull_connected_edges<_EdgeToVertex, Adapter, Notifier>(adapter, notifier));
    }
    
    /** \brief Cleans up the builder
      */
    void clear() noexcept
    {
        m_edges.clear();
        m_vertices.clear();
    }

    /** \brief First iterator of vertices
      * \returns iterator to indexed vertices container for first position
      */
    typename _Vertices::const_iterator vbegin() const noexcept
    {
        return std::begin(m_vertices);
    }

    /** \brief Last iterator of vertices
      * \returns iterator to indexed vertices container for last position
      */
    typename _Vertices::const_iterator vend() const noexcept
    {
        return std::end(m_vertices);
    }
    
    /** \brief First iterator of edges
      * \returns iterator to indexed edges container for first position
      */
    typename _Edges::const_iterator begin() const noexcept
    {
        return std::begin(m_edges);
    }

    /** \brief Last iterator of edges
      * \returns iterator to indexed edges container for last position
      */
    typename _Edges::const_iterator end() const noexcept
    {
        return std::end(m_edges);
    }

    /** \brief Retrieves an vertex
      * \param[in] key - vertex key, having the identifier filled up 
      * \returns iterator to found vertex, or to last
      */
    typename _Vertices::const_iterator get_vertex(const Vertex& key) const noexcept
    {
        return m_vertices.find(key);
    }
    
    /** \brief Retrieves an edge
      * \param[in] key - edge key, having the identifier filled up 
      * \returns iterator to found edge, or to last
      */
    typename _Edges::const_iterator get_edge(const Edge& key) const noexcept
    {
        return m_edges.find(key);
    }
    
    /** \brief Vertices count
      * \returns number of vertices in the builder
      */
    size_t vertices() const noexcept
    {
        return m_vertices.size();
    }
    
    /** \brief Edges count
      * \returns number of edges in the builder
      */
    size_t edges() const noexcept
    {
        return m_edges.size();
    }

private:
    /** \brief Appends a vertex to the builder
      * \param[in] vertex - vertex object
      * \returns none, when insertion succeed, otherwise the failure message
      */
    BuilderMessage _push_vertex(const Vertex& vertex)
    {
        // Insert vertex
        return (m_vertices.insert(std::move(_Indexed_Vertex(vertex, m_vertices.size()))).second
                ? BuilderMessage::none
                : BuilderMessage::vertex_exists);
    }

    /** \brief Appends an edge to the builder
      * \param[in] edge - edge object
      * \param[in] generate_vertex - when true, generates vertexes from edge and stores them
      * \returns none, when insertion succeed, otherwise the failure message
      */
    BuilderMessage _push_edge(const Edge& edge, 
                              bool generate_vertex)
    {
        // Extract vertexes from edge for reference/non-reference nodes
        Vertex vertex_r = EdgeToVertex<Vertex, Edge, true>()(edge);
        Vertex vertex_nr = EdgeToVertex<Vertex, Edge, false>()(edge);
        // Check if edge is loop
        BuilderMessage res = (is_equal<Vertex>()(vertex_r, vertex_nr)
                              ? BuilderMessage::edge_is_loop 
                              : BuilderMessage::none);
        // When edge is valid -> try to insert
        if (BuilderMessage::none == res)
        {
            // Insert edge
            res = (m_edges.insert(std::move(_Indexed_Edge(edge, m_edges.size()))).second
                                  ? BuilderMessage::none
                                  : BuilderMessage::edge_exists);
            // When insertion succeed -> insert the generated vertices also
            if ((BuilderMessage::none == res) && (generate_vertex))
            {
                // Insert vertices
                m_vertices.insert(std::move(_Indexed_Vertex(vertex_r, m_vertices.size())));
                m_vertices.insert(std::move(_Indexed_Vertex(vertex_nr, m_vertices.size())));
            }
        }
        return res;
    }
    
    /** \brief Pulls the raw vertices
      * \param[in] adapter - adapter to convert builder vertices into graph vertices 
      * \returns true, when succeed, otherwise false
      */
    template <typename Adapter>
    bool _pull_vertices(Adapter& adapter)
    {
        bool res = true;
        for (auto it = std::begin(m_vertices); it != std::end(m_vertices); ++it)
        {
            if (!adapter(it->first, it->second))
            {
                res = false;
                break;
            }
        }
        return res;
    }

    /** \brief Pulls the raw edges
      * \param[in] adapter - adapter to convert builder edges into graph edges 
      * \returns true, when succeed, otherwise false
      */
    template <typename Adapter>
    bool _pull_edges(Adapter& adapter)
    {
        bool res = true;
        for (auto it = std::begin(m_edges); it != std::end(m_edges); ++it)
        {
            if (!adapter(it->first, it->second))
            {
                res = false;
                break;
            }
        }
        return res;
    }

    /** \brief Pulls the connected vertex graph, built from the actual edges and vertices of the builder
      * \param[in] adapter - adapter to convert builder edges into graph edges and adding it to a graph
      * \param[in] notifier - function pointer to notify about missing connections
      *                       The arguments are the edge, message type and node type ("true" means reference node), 
      *                       by returning true -> the pulling process is cancelled
      * \returns true, when succeed, otherwise false
      */
    template <template <typename, typename, bool>class _EdgeToVertex,
              typename Adapter,
              typename Notifier>
    bool _pull_connected_vertices(Adapter& adapter,
                                  Notifier& notifier)
    {
        bool res = true;
        _Edge_Connections connections(2);
        // Iterate edges
        for (auto it = std::begin(m_edges); it != std::end(m_edges); ++it)
        {
            uint8_t ref_vertex = 0;
            uint8_t nref_vertex = 0;
            // Find reference vertex
            auto v = m_vertices.find(_EdgeToVertex<Vertex, Edge, true>()(it->first));
            if (v != std::end(m_vertices))
            {
                // First element is the edge index
                connections[ref_vertex].first = v->second;
                // Second element is the node type (redundant information)
                connections[ref_vertex].second = true;
                ref_vertex++;
            }
            // Vertex not found -> shall notify
            else if (notifier(it->first, BuilderMessage::no_connection, true))
            {
                // Notifier returned true -> abort
                res = false;
                break;
            }

            // Find non-reference vertex
            v = m_vertices.find(_EdgeToVertex<Vertex, Edge, false>()(it->first));
            if (v != std::end(m_vertices))
            {
                // First element is the edge index
                connections[ref_vertex + nref_vertex].first = v->second;
                // Second element is the node type (redundant informatioon)
                connections[ref_vertex + nref_vertex].second = false;
                nref_vertex++;
            }
            // Vertex not found -> shall notify
            else if (notifier(it->first, BuilderMessage::no_connection, false))
            {
                // Notifier returned true -> abort
                res = false;
                break;
            }
            // When no error -> send the connected edge to adapter
            if (!adapter(it->first, 
                            it->second, 
                            std::begin(connections), 
                            std::begin(connections) + ref_vertex, 
                            std::begin(connections) + ref_vertex,
                            std::begin(connections) + ref_vertex + nref_vertex))
            {
                res = false;
                break;
            }
        }
        return res;
    }
    
    /** \brief Pulls the connected edges vertex graph, built from the actual edges and vertices of the builder
      * \param[in] adapter - adapter to convert builder edges into graph edges and adding it to a graph
      * \param[in] notifier - function pointer to notify about missing connections
      *                       The arguments are the edge, message type and node type ("true" means reference node), 
      *                       by returning true -> the pulling process is cancelled
      * \returns true, when succeed, otherwise false
      */
    template <template <typename, typename, bool>class _EdgeToVertex,
              typename Adapter,
              typename Notifier>
    bool _pull_connected_edges(Adapter& adapter,
                               Notifier& notifier)
    {
        _EdgeVertices edge_vertices;
        _Edge_Connections connections;
        // Build connection table for edges
        bool res = _build_connections<_EdgeToVertex>(edge_vertices, notifier);
        if (res)
        {
            size_t ref_cons = 0;
            size_t nref_cons = 0;
            // Iterate edges
            for (auto it = std::begin(m_edges); it != std::end(m_edges); ++it)
            {
                // Retrieve connection information for current edge
                res = _get_edge_connections<_EdgeToVertex>(_Indexed_Edge(it->first, it->second),
                                                           edge_vertices, 
                                                           connections, 
                                                           ref_cons, 
                                                           nref_cons, 
                                                           notifier);
                // When no error -> send the connected edge to adapter
                if (res)
                {
                    if (!adapter(it->first, 
                                 it->second, 
                                 std::begin(connections), 
                                 std::begin(connections) + ref_cons, 
                                 std::begin(connections) + ref_cons,
                                 std::begin(connections) + ref_cons + nref_cons))
                    {
                        res = false;
                        break;
                    }
                    // When everything fine -> continue with next edge
                    continue;
                }
                // Whe error observed -> abort the pulling process
                break;
            }
        }
        return res;
    }

    /** \brief Get connected edge indexes for a given edge
      * \param[in] edge - indexed edge object
      * \param[in] edge_vertices - the connection table
      * \param[out] connections - container for the connections of given edge
      * \param[out] ref_cons - count of reference connections
      * \param[out] nref_cons - count of non-reference connections
      * \param[in] notifier - function pointer to notify about missing connections
      *                       The arguments are the edge, message type and node type ("true" means reference node),
      *                       by returning true -> the pulling process is cancelled
      * \returns true, when succeed, otherwise false
      */
    template <template <typename, typename, bool>class _EdgeToVertex,
              typename Notifier>
    bool _get_edge_connections(const _Indexed_Edge& edge,
                               const _EdgeVertices& edge_vertices,
                               _Edge_Connections& connections,
                               size_t& ref_cons,
                               size_t& nref_cons,
                               Notifier& notifier)
    {
        bool res = true;
        connections.clear();

        // Get reference connections
        if (!_get_edge_vertex_connections<_EdgeToVertex, true>(edge, edge_vertices, connections, ref_cons))
        {
            // No connections in reference node -> notify about
            if (notifier(edge.first, BuilderMessage::no_connection, true))
            {
                // Notifier returned true -> abort
                res = false;
            }
        }
        // Get non-reference connections
        if (res && (!_get_edge_vertex_connections<_EdgeToVertex, false>(edge, edge_vertices, connections, nref_cons)))
        {
            // No connections in non-reference node -> notify about
            if (notifier(edge.first, BuilderMessage::no_connection, false))
            {
                // Notifier returned true -> abort
                res = false;
            }
        }
        return res;
    }

    /** \brief Get connected edge indexes for a given edge in a given node
      * \param[in] edge - indexed edge object
      * \param[in] edge_vertices - the connection table
      * \param[out] connections - container for the connections of given edge
      * \param[out] cons - count of connections in the given node 
      * \returns true, when succeed, otherwise false
      */
    template <template <typename, typename, bool>class _EdgeToVertex,
              bool is_ref>
    bool _get_edge_vertex_connections(const _Indexed_Edge& edge,
                                      const _EdgeVertices& edge_vertices,
                                      _Edge_Connections& connections,
                                      size_t& cons)
    {
        bool res = false;
        cons = 0;
        // Extract the correspondent vertex from edge
        auto _edge_node = EdgeToVertex<Vertex, Edge, is_ref>()(edge.first);
        // Search for it in the vertices
        auto v = m_vertices.find(_edge_node);

        // When found
        if (v != std::end(m_vertices))
        {
            // Search for the lower bound of connected edges
            // com is pair of: { pair{edge, edge index}, vertex index }
            auto con = std::lower_bound(std::begin(edge_vertices), std::end(edge_vertices), 
                                        _Edge_Vertex(std::end(m_edges), v->second),
                                        [](const _Edge_Vertex& l, const _Edge_Vertex& r)
                                           {return l.second < r.second;});
            // Store the connected edge indexes
            while ((con != std::end(edge_vertices)) && (con->second == v->second))
            {
                // Skip the self connection
                if (con->first->second != edge.second)
                {
                    // Check if connection is in the reference, or non-reference node of the connected edge
                    auto ref_con_node = _EdgeToVertex<Vertex, Edge, true>()(con->first->first);
                    
                    // When current edge's current node is identical with connected edge's reference node:
                    //  -> connected edge will be traversed into backward direction from here 
                    //     e.g. non-ref -> ref 
                    // When current edge's current node is NOT identical with connected edge's reference node:
                    //  -> connected edge will be traversed into forward direction from here 
                    //     e.g. ref -> non-ref 

                    connections.push_back(_Edge_Connection(con->first->second, 
                                          (IsEqual<Vertex>()(ref_con_node, _edge_node))));
                    cons++;
                }
                con++;
            }
            res = true;
        }
        return res;
    }

    /** \brief Build the connection table of vertex-edge index pairs
      * \param[out] edge_vertices - the connection table 
      * \param[in] notifier - function pointer to notify about missing connections
      *                       The arguments are the edge, message type and node type ("true" means reference node), 
      *                       by returning true -> the build process is aborted
      * \returns true, when succeed, otherwise false
      */
    template <template <typename, typename, bool>class _EdgeToVertex,
              typename Notifier>
    bool _build_connections(_EdgeVertices& edge_vertices,
                            Notifier& notifier)
    {
        bool res = true;
        // Iterate edges
        for (auto it = std::begin(m_edges); it != std::end(m_edges); ++it)
        {
            // Insert pair of edge index <-> reference vertex index, by refernce node
            if (!_add_vertex_edge_connection<_EdgeToVertex, Notifier, true>
                    (edge_vertices, it, notifier))
            {
                res = false;
                break;
            }
            // Insert pair of edge index <-> reference vertex index, by non-refernce node
            if (!_add_vertex_edge_connection<_EdgeToVertex, Notifier, false>
                    (edge_vertices, it, notifier))
            {
                res = false;
                break;
            }
        }
        // When no error - sort the connections table by vertex->edge
        if (res)
        {
            _sort_vertex_edge_connections(edge_vertices);
        }
        return res;
    }

    /** \brief Adds an vertex-edge pair into the connection false ("is_ref" tells which edge node to use)
      * \param[out] edge_vertices - the connection table 
      * \param[in] edge - indexed edge object
      * \param[in] notifier - function pointer to notify about missing connections
      *                       The arguments are the edge, message type and node type ("true" means reference node),
      *                       by returning true -> the build process is aborted
      * \returns true, when succeed, otherwise false
      */
    template <template <typename, typename, bool>class _EdgeToVertex,
              typename Notifier,
              bool is_ref>
    bool _add_vertex_edge_connection(_EdgeVertices& edge_vertices,
                                     typename _Edges::iterator edge,
                                     Notifier& notifier)
    {
        bool res = true;
        // Insert pair of edge index <-> reference vertex index
        auto it = m_vertices.find(_EdgeToVertex<Vertex, Edge, is_ref>()(edge->first));
        // Search for the vertex
        if (it != std::end(m_vertices))
        {
            // if found -> add to connections table
            edge_vertices.push_back(_Edge_Vertex(edge, it->second));
        }
        else 
        {
            // If vertex not found -> notify about
            res = !notifier(edge->first, BuilderMessage::vertex_is_missing, is_ref);
        }
        return res;
    }

    /** \brief Sorts vertex-edge connections
      * \param[out] edge_vertices - the connection table 
      */
    void _sort_vertex_edge_connections(_EdgeVertices& edge_vertices) noexcept
    {
        // Sort the connections by vertex->edge
        std::sort(std::begin(edge_vertices), std::end(edge_vertices), 
                  [](_Edge_Vertex& l,
                     _Edge_Vertex& r)
        {
            // Sort first by vertex, than by edge
            return ((l.second < r.second) ||
                    ((l.second == r.second) && (l.first->second < r.first->second)));
        });
    }

    /** \brief Sort builder vertices, or edges by an external criteria
      * \param[in] compare - function for external crtieria
      */
    template<typename Entity, 
             typename entity_compare>
    void _sort_entity(Entity& entity, 
                      entity_compare compare)
    {
        // Reserve space
        _Entity_Sorter<typename Entity::iterator> entity_sorter(entity.size());
        auto it_ = std::begin(entity_sorter);
        // Fill-up sorter container with entity iterators
        for (auto it = std::begin(entity); it != std::end(entity); ++it, ++it_)
        {
            *it_ = it;
        }
        // Sort by external criteria
        std::sort(std::begin(entity_sorter), std::end(entity_sorter),
                  [compare](const typename Entity::iterator& l, const typename Entity::iterator& r)->bool
                    { return compare(l->first, r->first); });
        // Re-index by sorted order
        size_t pos = 0;
        for (auto it = std::begin(entity_sorter); it != std::end(entity_sorter); ++it)
        {
            (*it)->second = pos++;
        }
    }

private:
    _Edges     m_edges;    // Indexed edges
    _Vertices  m_vertices; // Indexed vertices
};

}
}

#endif //__EDU_GRAPHS_GRAPH_BUILDER_H__
