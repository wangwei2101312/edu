///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.08.07.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_GRAPHS_BFS_H__
#define __EDU_GRAPHS_BFS_H__

#include <list>

namespace edu
{
namespace graph
{

/** \brief Breadth First Search routing algorithm
  */
template <typename Vertex,
          typename Cost,
          typename bi_directional,
          typename cost_is_less = std::less<Cost>,
          typename cost_plus = std::plus<Cost>>
class Bfs
{
    // Pair of {vertex, cost} -> cost accumulated from the start point
    using _Vertex = std::pair<Vertex, Cost>;
    // Queue for pending routes - breadth first search is using a FIFO list
    using _Queue = std::list<_Vertex>;

protected:
    /** \brief Constructor
      */
    Bfs()
    : m_queue()
    {
    }

    /** \brief Destructor
      */
    virtual ~Bfs() = default;
        
protected:
    /** \brief Enqueues a list of vertices for path search
      * \param[in] vertex - vertex to get fetched
      * \param[in] cost - cost
      * \param[in] forward - direction of routing (forward/backward)
      */
    void enqueue(const Vertex& vertex, 
                 const Cost& cost,
                 bool forward)
    {
        // Eqnueue the {vertex, cost} pairs
        (forward ? *std::begin(m_queue) 
                 : *std::rbegin(m_queue)).push_back(
            std::move(_Vertex{vertex, cost}));
    }
    
    /** \brief Clean-up the enqueued vertices
      */
    void reset() noexcept
    {
        std::for_each(std::begin(m_queue),
                      std::end(m_queue), 
                      [](auto& it){it.clear();});
    }

    /** \brief Selects an enqueued vertex to get processed 
      * \param[in] forward - direction of routing (forward/backward)
      * \returns pair of: {true/false, { Vertex, Cost } }
      *  where "true" tells whether vertex is valid, "false" tells there are no more valid enqueued vertices
      */
    std::pair<bool, _Vertex> search(bool forward) noexcept
    {
        // Start with first queue
        auto& queue = (forward ? *std::begin(m_queue) : *std::rbegin(m_queue));
        // Iterate the pending vertices
        std::pair<bool, _Vertex> vertex{ !queue.empty(), _Vertex{}};
        if (!queue.empty())
        {
            // Extract first element
            vertex.second = queue.front();
            queue.pop_front();
        }
        // Return the result 
        return std::move(vertex);
    }
    
    /** \brief Verify whether pending routes may result still cheaper as a given cost
      * \param[in] cost - total cost of vertex
      * \returns true, when current vertex is path, or path candidate, otherwsie false
      */
    bool verify(const Cost&) const noexcept
    {
        // Depth First Search wants to visit all vertices
        return false;
    }

    
private:
    _Queue  m_queue[bi_directional::value ? 2 : 1];
};


/** \brief Breadth First Search routing algorithm for first route
  */
template <typename Vertex,
          typename Cost,
          typename bi_directional,
          typename cost_is_less = std::less<Cost>,
          typename cost_plus = std::plus<Cost>>
class BfsFirst : public Bfs<Vertex, Cost, bi_directional, cost_is_less, cost_plus>
{
public:
    /** \brief Verify whether pending routes may result still cheaper as a given cost
      * \param[in] cost - total cost of vertex
      * \returns true, when current vertex is path, or path candidate, otherwsie false
      */
    bool verify(const Cost&) const noexcept
    {
        // Breadth First Search wants to stop by the first route - doesn't matter how it is
        return true;
    }
};

}
}

#endif //__EDU_GRAPHS_BFS_H__
