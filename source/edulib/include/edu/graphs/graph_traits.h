///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_GRAPHS_GRAPH_TRAITS_H__
#define __EDU_GRAPHS_GRAPH_TRAITS_H__


namespace edu
{
namespace graph
{

/** \brief Functor trait for checking equality
  */
template <typename T>
struct is_equal
{
    /** \brief Compare 2 elements
      * \param[in] l - left element
      * \param[in] r - right element
      * \returns true, when they are equal, otherwise false
      */
    bool operator()(const T& l, const T& r) const;
};

/** \brief Functor trait for checking whether an element is less than other
  */
template <typename T>
struct is_less
{
    /** \brief Compare 2 elements
      * \param[in] l - left element
      * \param[in] r - right element
      * \returns true, when left is less than right, otherwise false
      */
    bool operator()(const T& l, const T& r) const;
};

/** \brief Functor trait for calculating hash value for an element
  */
template <typename T>
struct hash_of
{
    /** \brief Caclulates hash value for an element
      * \param[in] e - instance of element
      * \returns numeric hash value
      */
    size_t operator()(const T& e) const;
};

/** \brief Functor trait for extracting vertex information from an edge
  */
template <typename Vertex,
          typename Edge,
          bool is_ref>
struct edge_to_vertex
{
    /** \brief Creates a vertex from an edge 
      * - when "is_ref" is true -> vertex is created for the reference node of the edge
      * - when "is_ref" is false -> vertex is created for the non-reference node of the edge
      * \param[in] edge - instance of edge 
      * \returns a vertex object
      */
    Vertex operator()(const Edge&) const;
};


/** \brief Functor trait for adapters, connecting graph builders with graphs
  */
template<typename Vertex,
         typename Edge>
struct graph_adapter
{
    /** \brief Pulls an edge from the builder, without connected edges
      * \param[in] edge - pulled edge
      * \param[in] edge_index - index of pulled edge
      * \returns false, when pulling shall be aborted, otherwise true
      */
    bool operator()(const Edge& edge, 
                    size_t edge_index);

    /** \brief Pulls a vertex from the builder, without connected edges
      * \param[in] vertex - pulled vertex
      * \param[in] vertex_index - index of pulled vertex
      * \returns false, when pulling shall be aborted, otherwise true
      */
    bool operator()(const Vertex& vertex, 
                    size_t vertex_index);

    /** \brief Pulls an edge from the builder, with connected edge, or vertex indexes
      * \param[in] edge - pulled edge
      * \param[in] edge_index - index of pulled edge
      * \param[in] ref_first - lower bound of range with connected edges, or vertices in reference node
      * \param[in] ref_last - upper bound of range with connected edges, or vertices in reference node
      * \param[in] nref_first - lower bound of range with connected edges, or vertices in non-reference node
      * \param[in] nref_last - upper bound of range with connected edges, or vertices in non-reference node
      * \returns false, when pulling shall be aborted, otherwise true
      */
    template<typename Iterator>
    bool operator()(const Edge& edge, 
                    size_t edge_index, 
                    Iterator ref_first, 
                    Iterator ref_last, 
                    Iterator nref_first, 
                    Iterator nref_last);

};

}
}

#endif //__EDU_GRAPHS_GRAPH_TRAITS_H__
