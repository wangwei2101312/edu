///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.08.07.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_GRAPHS_DIJKSTRA_H__
#define __EDU_GRAPHS_DIJKSTRA_H__

#include <queue>

namespace edu
{
namespace graph
{

/** \brief Dijkstra routing algorithm
  */
template <typename Vertex,
          typename Cost,
          typename bi_directional,
          typename cost_is_less = std::less<Cost>,
          typename cost_plus = std::plus<Cost>>
class Dijkstra
{
    // Pair of {vertex, cost} -> cost accumulated from the start point
    using _Vertex = std::pair<Vertex, Cost>;
    // Functor for priority qeueue - the most cheap vertex is the first
    struct _vertex_compare
    {
        bool operator()(const _Vertex& l, const _Vertex& r)
        {
            return r.second < l.second;
        }
    };
    // Queue for pending routes - breadth first search is using a FIFO list
    using _Queue = std::priority_queue<_Vertex, std::vector<_Vertex>, _vertex_compare>;

protected:
    /** \brief Constructor
      */
    Dijkstra()
    : m_queue()
    {
    }

    /** \brief Destructor
      */
    virtual ~Dijkstra() = default;
        
protected:
    /** \brief Enqueues a list of vertices for path search
      * \param[in] vertex - vertex to get fetched
      * \param[in] cost - cost
      * \param[in] forward - direction of routing (forward/backward)
      */
    void enqueue(const Vertex& vertex, 
                 const Cost& cost,
                 bool forward)
    {
        // Eqnueue the {vertex, cost} pairs
        (forward ? *std::begin(m_queue) 
                 : *std::rbegin(m_queue)).push(
            std::move(_Vertex{vertex, cost}));
    }
    
    /** \brief Clean-up the enqueued vertices
      */
    void reset() noexcept
    {
        std::for_each(std::begin(m_queue),
                      std::end(m_queue), 
                      [](auto& it)
                      {
                          // Clean up priority queue
                          _Queue q(std::move(it));
                      });
    }

    /** \brief Selects an enqueued vertex to get processed 
      * \param[in] forward - direction of routing (forward/backward)
      * \returns pair of: {true/false, { Vertex, Cost } }
      *  where "true" tells whether vertex is valid, "false" tells there are no more valid enqueued vertices
      */
    std::pair<bool, _Vertex> search(bool forward) noexcept
    {
        // Start with first queue
        auto& queue = (forward ? *std::begin(m_queue) : *std::rbegin(m_queue));
        // Iterate the pending vertices
        std::pair<bool, _Vertex> vertex{ !queue.empty(), _Vertex{}};
        if (!queue.empty())
        {
            // Extract first element
            vertex.second = queue.top();
            queue.pop();
        }
        // Return the result 
        return std::move(vertex);
    }
    
    /** \brief Verify whether pending routes may result still cheaper as a given cost
      * \param[in] cost - total cost of vertex
      * \returns true, when current vertex is path, or path candidate, otherwsie false
      */
    template <typename T = bool>
    typename std::enable_if<bi_directional::value, T>::          
    type verify(const Cost& cost) const noexcept
    {
        // check if priority queues have still better pending routes
        return !cost_is_less()(
            cost_plus()(std::begin(m_queue)->empty() ? Cost{} : std::begin(m_queue)->top().second,
                        std::rbegin(m_queue)->empty() ? Cost{} : std::rbegin(m_queue)->top().second),
            cost);
    }

    /** \brief Verify whether pending routes may result still cheaper as a given cost
      * \param[in] cost - total cost of vertex
      * \returns true, when current vertex is path, or path candidate, otherwsie false
      */
    template <typename T = bool>
    typename std::enable_if<!bi_directional::value, T>::          
    type verify(const Cost& cost)const noexcept
    {
        // check if priority queues have still better pending routes
        return !cost_is_less()(
            std::begin(m_queue)->empty() ? Cost{} : std::begin(m_queue)->top().second,
            cost);
    }

private:
    _Queue  m_queue[bi_directional::value ? 2 : 1];
};


}
}

#endif //__EDU_GRAPHS_DIJKSTRA_H__
