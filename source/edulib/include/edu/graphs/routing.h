///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.08.07.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_GRAPHS_ROUTING_H__
#define __EDU_GRAPHS_ROUTING_H__

#include <unordered_map>
#include <forward_list>
#include <tuple>

namespace edu
{
namespace graph
{

/** \brief Routing modes
  */
enum class RoutingMode
{
    forward = 0,   //!< route is searched in one direction only: from start vertices to end vertices
    backward,      //!< route is searched in one direction only: from end vertices to to vertices
    bidirectional  //!< route is searched in two directions in the same time (forward and backward)
};


/** \brief Routing adapter trait - the adapter connects the routing engine to an existent graph
  * 
  * Adapters are implementation (use-case) specific
  * It is a key component to can keep the graph and routing algorithms abstract, 
  * but to can obtain fully customized solutions. 
  * There is the job of adapter to provide dynamic traversal/turn costs 
  * (e.g. traffic jams, turn penalties, A* complementary costs etc). 
  * In order to allow routing between isolated areas, the adapter can force the routing 
  * to work for forbidden paths also, adding a specific cost for them. 
  * 
  * The same with U-turns - adapter can establish whether U-Turns allowed and with, which cost 
  * - whether U-turn allowed in connection points only, or directly 
  * (e.g. adding the same vertex with opposite direction by start/end points)
  */
template<typename Vertex, //!< Vertex data structure, used to identify a vertex uniquely
         typename Cost>   //!< Cost data structure, used to calculate the cost of routes
struct RoutingAdapter
{
public:
    /** \brief Fetches start and end waypoints
        * \returns pair of { 
        *                   { 
        *                     iterator pointing to first start {Vertex, Cost}, 
        *                     iterator pointing to last  start {Vertex, Cost} 
        *                   } 
        *                   { 
        *                     iterator pointing to first end {Vertex, Cost}, 
        *                     iterator pointing to last  end {Vertex, Cost} 
        *                   } 
        *                 }
        */
    std::pair<std::forward_list<std::pair<Vertex, Cost>>, 
              std::forward_list<std::pair<Vertex, Cost>>> 
    operator()();
        
    /** \brief Fetches connection information for a vertex
        * \param[in] vertex - input vertex provided by routing engine
        * \param[in] cost - cost of input vertex provided by the routing engine (as helper information for the adapter)
        *  (Example: analysing the costs the adapter is able to recognize, whether the routing algorithm is trying to exit/enter 
        *   a forbidden zone in order to not fail, when start/end points are set into a pedestrian zone and we want to find 
        *   the best near accessible location with a certain vehicle type)
        * \param[in] forward - direction of routing: true = forward routing, false = backward routing
        * \returns pair of { bool for feching succeed = (true/false), 
        *                   { iterator pointing to first connected {Vertex, Cost} pair, 
        *                     iterator pointing to last connected  {Vertex, Cost} pair }
        */
    template <typename Iterator>
    std::pair<bool, std::pair<Iterator, Iterator>> 
    operator()(const Vertex& vertex,
               const Cost& cost,
               bool forward);
};


/** \brief Abstract routing algorithm
  *  Able to execute forward, backward and bi-directional path search, according to the argument of "routing_direction"
  *  Stores visited nodes with their cost and parent vertex in a hashmap, launches the specialized algorithm for:
  *   -  vertex selection: which vertex to get processed next
  *   - "good enough" decision: whether a found route is good enough to can stop the search
  *  The algorithm is fully abstract - it knows only about vertex,parent vertex and costs - all the specializations is done
  *  by routing adapters and eventually by specific algorithm 
  *   (e.g. transition penalties, dynamic traffic jams, U-turns, exiting/entering from/into an isolated area etc ...)
  *  The routing algorithm is able to work with multiple start and end-points, in this case the winner route will be selected (no direct support for multiple routes)
  */
template <typename Adapter,                             //!< Adapter type connecting the router to a graph - see the RoutingAdapter type trait above
          typename Vertex,                              //!< Vertex data structure, used to identify a vertex uniquely
          typename Cost,                                //!< Cost data structure, used to calculate the cost of routes
          RoutingMode routing_direction,                //!< Direction of path search - see RoutingMode
          template<typename...>class Algorithm,         //!< Path search algorithm, for selecting vertex and decide about candidate paths
          typename vertex_hash = std::hash<Vertex>,     //!< Functor for hashing a vertex
          typename vertex_equal = std::equal_to<Vertex>,//!< Functor for comparing two vertices, whether equals
          typename cost_is_less = std::less<Cost>,      //!< Functor for comparing two costs, whether left is less than right
          typename cost_plus = std::plus<Cost>>         //!< Functor for sum two costs
class Routing : std::conditional<routing_direction == RoutingMode::bidirectional,
                                 Algorithm<Vertex, Cost, std::true_type, 
                                           cost_is_less, cost_plus>,
                                 Algorithm<Vertex, Cost, std::false_type, 
                                           cost_is_less, cost_plus>>::type
{
    // Algorithm type, used for vertex selection
    using  _Algorithm = typename std::conditional<routing_direction == RoutingMode::bidirectional,
                                                  Algorithm<Vertex, Cost, std::true_type, 
                                                            cost_is_less, cost_plus>,
                                                  Algorithm<Vertex, Cost, std::false_type, 
                                                            cost_is_less, cost_plus>>::type;
    // Vertex with cost
    using _Vertex = std::pair<Vertex, Cost>;
    // The visited verices shall store the own cost also - for the final cost calculation
    // (the total cost might be outdated, when visited nodes are updated by concurrent route branches)
    // there is needed to store the own cost of vertices, which might be route dependent 
    // e.g. traffic jams, dynamic penalties
    using _Visited = std::unordered_map<Vertex, std::tuple<Vertex, Cost, Cost>, vertex_hash, vertex_equal>;
    // Container for found path : {vertex, total cost, vertex cost}
    using _Path =  std::forward_list<std::tuple<Vertex, Cost, Cost>>;

public:
    /** \brief Constructor
      * \param[in] adapter - adapter for connecting to a graph. Can be a specialized instance of RoutingAdapter
      */
    Routing(Adapter& adapter)
    : _Algorithm()
    , m_adapter(adapter)
    , m_visiteds()
    , m_path()
    {
    }

    /** \brief Destructor
      */
    virtual ~Routing() = default;
        
public:
    /** \brief Searches for a route between two vertices
      * \param[in] notifier - callback for notifications, to can follow-up how routing searches the path
      * \returns true, when succeed, otherwise false
      */
    template<typename Notifier = std::function<bool(const Vertex&, const Vertex&, const Cost&, bool)>>
    bool operator()(Notifier notifier = nullptr)
    {
        // Reset routing
        _reset();
        // Fetch start-end way_points: result is
        // {
        //   {iterator to first start vertex, iterator to last start vertex}
        //   {iterator to first end vertex, iterator to last end vertex}
        // }
        auto way_points(m_adapter());
        // Enqueue start/end vertices
        bool res = _enqueue(std::begin(way_points.first), std::end(way_points.first),
                            std::begin(way_points.second), std::end(way_points.second),
                            notifier);
        if (res)
        {
            // Start path search
            res = _search(std::begin(way_points.first), std::end(way_points.first),
                          std::begin(way_points.second), std::end(way_points.second),
                          notifier);
        }
        return res;        
    }
    
    /** \brief Retrieve the path as a list of vertices with their costs
      * \param[in] inserter - inserter iterator
      *  - front inserter - used by forward routing
      *  - back inserter - used by backward routing
      * return true, when there is valid path, otherwise false
      */
    template<typename T = bool,
             typename Iterator>
    typename std::enable_if<RoutingMode::bidirectional != routing_direction, T>::
    type  pull(Iterator inserter) const
    {
        return _pull(inserter);
    }

    /** \brief Retrieve the path as a list of vertices with their costs
      * \param[in] front_inserter - front inserter iterator
      * \param[in] back_inserter - back inserter iterator
      * return true, when there is valid path, otherwise false
      */
    template<typename T = bool,
             typename Iterator_1,
             typename Iterator_2>
    typename std::enable_if<RoutingMode::bidirectional == routing_direction, T>::
    type pull(Iterator_1 front_inserter, 
              Iterator_2 back_inserter) const
    {
        return _pull(front_inserter, back_inserter);
    }

    /** \brief Retrieve the count of visited vertices
      * return count of visited vertices
      */
    size_t visiteds() const noexcept
    {
        return _visiteds();
    }

    /** \brief Reset routing algorithm
      */
    void reset() noexcept
    {
        _reset();
    }
    
private:
    /** \brief Searches for path with forward, or backward routing 
      * \param[in] start_first - iterator pointing to the first elements of start vertices
      * \param[in] start_last - iterator pointing to the last elements of start vertices
      * \param[in] end_first - iterator pointing to the first elements of end vertices
      * \param[in] end_last - iterator pointing to the last elements of end vertices
      * \param[in] notifier - callback for notifications
      * \returns true, when succeed, otherwsie false
      */
    template<typename T = bool,
             typename Iterator,
             typename Notifier>
    typename std::enable_if<RoutingMode::bidirectional != routing_direction, T>::
    type _search(Iterator start_first,
                 Iterator start_last,
                 Iterator end_first,
                 Iterator end_last,
                 Notifier& notifier)
    {
        const bool fw = (RoutingMode::forward == routing_direction);

        // Start path search until algorithm has enqeued vertices
        std::pair<bool, _Vertex> res;
        do 
        {
            // Launch algorithm for vertex selection
            res = std::move(_Algorithm::search(RoutingMode::forward == routing_direction));
            if (res.first)
            {
                // When algorithm returned a vertex
                if (!_fetch(res.second.first,
                        res.second.second,
                        fw,
                        (fw ? end_first : start_first),
                        (fw ? end_last : start_last),
                        notifier))
                {
                    // Critical error
                    reset();
                    break;
                }
            }       
        } while(res.first);      
        // Return true, when there is a found path, otherwise false
        return (!m_path.empty());
    }

    /** \brief Searches for path with bi-directional routing 
      * \param[in] start_first - iterator pointing to the first elements of start vertices
      * \param[in] start_last - iterator pointing to the last elements of start vertices
      * \param[in] end_first - iterator pointing to the first elements of end vertices
      * \param[in] end_last - iterator pointing to the last elements of end vertices
      * \param[in] notifier - callback for notifications
      * \returns true, when succeed, otherwsie false
      */
    template<typename T = bool,
             typename Iterator,
             typename Notifier>
    typename std::enable_if<RoutingMode::bidirectional == routing_direction, T>::
    type _search(Iterator start_first,
                 Iterator start_last,
                 Iterator end_first,
                 Iterator end_last,
                 Notifier& notifier)
    {
        // Start path search until algorithm has eqneued vertices
        std::pair<bool, _Vertex> res[2];
        do 
        {
            // Check in forward direction
            // Launch algorithm for vertex selection
            *std::begin(res) = std::move(_Algorithm::search(true));
            if ((*std::begin(res)).first)
            {
                // When algorithm returned a vertex
                if (!_fetch((*std::begin(res)).second.first,
                            (*std::begin(res)).second.second,
                            true,
                            start_first,
                            start_last,
                            notifier))
                {
                    // Critical error
                    reset();
                    break;
                }
            }
            // Check in backward direction
            // Launch algorithm for vertex selection
            *std::rbegin(res) = std::move(_Algorithm::search(false));
            if ((*std::rbegin(res)).first)
            {
                // When algorithm returned a vertex
                if (!_fetch((*std::rbegin(res)).second.first,
                        (*std::rbegin(res)).second.second,
                        false,
                        end_first,
                        end_last,
                        notifier))
                {
                    // Critical error
                    reset();
                    break;
                }
            }            
        } while( (*std::begin(res)).first || (*std::rbegin(res)).first);   
        // Return true, when there is a found path, otherwise false
        return (!m_path.empty());
    }
    
    /** \brief Enqueues first vertices for forward, or backward routing
      * \param[in] start_first - iterator pointing to the first elements of start vertices
      * \param[in] start_last - iterator pointing to the last elements of start vertices
      * \param[in] end_first - iterator pointing to the first elements of end vertices
      * \param[in] end_last - iterator pointing to the last elements of end vertices
      * \param[in] notifier - callback for notifications
      * \returns true, when succeed, otherwsie false
      */
    template<typename T = bool,
             typename Iterator,
             typename Notifier>
    typename std::enable_if<RoutingMode::bidirectional != routing_direction, T>::
    type _enqueue(Iterator start_first,
                 Iterator start_last,
                 Iterator end_first,
                 Iterator end_last,
                 Notifier& notifier)
    {
        const bool fw = (RoutingMode::forward == routing_direction);
        bool res = true;       
        // Add start/end vertices into the forward/backward visiteds table
        for (auto it = (fw ? start_first : end_first); it != (fw ? start_last : end_last); ++it)
        {
            std::begin(m_visiteds)->insert(std::move(std::make_pair(it->first, 
                                            std::make_tuple(it->first, Cost{}, Cost{}))));
        }
        // Enqueue the connections of start/end vertices into algorithms
        for (auto it = (fw ? start_first : end_first); (res && (it != (fw ? start_last : end_last))); ++it)
        {
            res = _fetch(it->first, Cost{}, 
                         fw, 
                         (fw ? end_first : start_first), 
                         (fw ? end_last : start_last), 
                         notifier);
        }
        return res;
    }

    /** \brief Enqueues first vertices for bidirectional routing
      * \param[in] start_first - iterator pointing to the first elements of start vertices
      * \param[in] start_last - iterator pointing to the last elements of start vertices
      * \param[in] end_first - iterator pointing to the first elements of end vertices
      * \param[in] end_last - iterator pointing to the last elements of end vertices
      * \param[in] notifier - callback for notifications
      * \returns true, when succeed, otherwsie false
      */
    template<typename T = bool,
             typename Iterator,
             typename Notifier>
    typename std::enable_if<RoutingMode::bidirectional == routing_direction, T>::
    type _enqueue(Iterator start_first,
                 Iterator start_last,
                 Iterator end_first,
                 Iterator end_last,
                 Notifier& notifier)
    {
        bool res = true;      
        // Add start vertices into the forward visiteds table
        for (auto it = start_first; it != start_last; ++it)
        {
            std::begin(m_visiteds)->insert(std::move(std::make_pair(it->first, 
                                           std::make_tuple(it->first, Cost{},Cost{}))));
        }
        // Enqueue the connections of start vertices into the algorithm
        for (auto it = start_first; (res && (it != start_last)); ++it)
        {
            res = _fetch(it->first, Cost{}, true, end_first, end_last, notifier);
        }
        
        // Add end vertices into the backward visiteds table
        for (auto it = end_first; (res && (it != end_last)); ++it)
        {
            std::rbegin(m_visiteds)->insert(std::move(std::make_pair(it->first, 
                                            std::make_tuple(it->first, Cost{}, Cost{}))));
        }
        // Enqueue the connections of end vertices into the algorithm
        for (auto it = end_first; (res && (it != end_last)); ++it)
        {
            res = _fetch(it->first, Cost{}, false, start_first, start_last, notifier);
        }    
        return res;
    }
    
    /** \brief Verify if vertex is in the target list, when bi-drectional routing
      * \param[in] vertex - vertex to get verified
      * \param[in] cost_vertex - cost of vertex
      * \param[in] parent - parent vertex where from vertex comes
      * \param[in] cost_parent - total cost of parent
      * \param[in] target_first - iterator pointing to the first elements of target vertices
      * \param[in] target_last - iterator pointing to the last elements of target vertices
      * \param[in] forward - direction of routing (forward/backward)
      * \returns true, when vertex is path, otherwise false
      */
    template<typename T = bool,
             typename Iterator>
    typename std::enable_if<RoutingMode::bidirectional == routing_direction, T>::
    type _verify(const Vertex& vertex,
                 const Cost& cost_vertex,
                 const Vertex& parent,
                 const Cost& cost_parent,
                 Iterator target_first,
                 Iterator target_last,
                 bool forward)
    {
        // First check the opposite visiteds for parent vertex: x---x----<x---x>----x
        // when the segment <x---x> is matched: it is a path having full cost confirmation
        bool res = false;
        {
            _Visited& visiteds = forward ? *std::rbegin(m_visiteds) : *std::begin(m_visiteds);
            auto it = visiteds.find(parent);
            if (it != visiteds.end())
            {
                // Parent found in the opposite table - determine total cost as: Fw + Bw
                //  -> visited = {vertex, { {parent vertex, total cost}, cost}
                 Cost cost_total{ (cost_plus()( cost_plus()(cost_parent, cost_vertex), std::get<1>(it->second))) };
                // Check if there is a candidate already
                if (!m_path.empty())
                {
                    // Verify if the new path candidate is better
                    if (cost_is_less()(cost_total, std::get<1>(m_path.front())))
                    {
                        // When final path comes from the same direction - add the destination cost also, otherwise nothing
                        auto tuple_{std::make_tuple(parent, cost_total, cost_vertex)};
                        std::swap(m_path.front(), tuple_);
                    }
                }
                else
                {
                    // When final path comes from the same direction - add the destination cost also, otherwise nothing
                    m_path.push_front(std::move(std::make_tuple(parent, cost_total, cost_vertex)));
                }
            }
        }
        // When there is path candidate - ask algorithm when there still might result something better
        if (!m_path.empty())
        {
            res = _Algorithm::verify(std::get<1>(m_path.front()));
        }
        return res;    
    }

    /** \brief Verify if vertex is in the target list when uni-drectional routing
      * \param[in] vertex - vertex to get verified
      * \param[in] cost_vertex - cost of vertex
      * \param[in] parent - parent vertex where from vertex comes
      * \param[in] cost_parent - total cost of parent
      * \param[in] target_first - iterator pointing to the first elements of target vertices
      * \param[in] target_last - iterator pointing to the last elements of target vertices
      * \param[in] forward - direction of routing (forward/backward)
      * \returns true, when vertex is path, otherwise false
      */
    template<typename T = bool,
             typename Iterator>
    typename std::enable_if<RoutingMode::bidirectional != routing_direction, T>::
    type _verify(const Vertex& vertex,
                 const Cost& cost_vertex,
                 const Vertex& parent,
                 const Cost& cost_parent,
                 Iterator target_first,
                 Iterator target_last,
                 bool forward)
    {
        // First check if vertex is present in targets list
        auto it = (std::find_if(target_first, target_last, [&vertex](const auto& val)
        {
            return vertex_equal()(vertex, val.first);
        }));
        bool res = (it != target_last);
        Cost cost_total{ res 
                         // When vertex in targets list: total cost = vertex cost + parent cost + cost on target
                         ? cost_plus()( cost_plus()(cost_vertex, cost_parent), it->second) 
                         // When vertex NOT in targets list: total cost = vertex cost + parent cost
                         : cost_plus()(cost_vertex, cost_parent) };
        if (res)
        {
            // Check if there is a candidate already
            if (!m_path.empty())
            {
                // Verify if the new path candidate is better
                if (cost_is_less()(cost_total, std::get<1>(m_path.front())))
                {
                    auto tuple_{std::make_tuple(vertex, cost_total, it->second)};
                    std::swap(m_path.front(), tuple_);
                }
            }
            else
            {
                m_path.push_front(std::move(std::make_tuple(vertex, cost_total, it->second)));
            }
        }
        // When there is path candidate - ask algorithm when there still might result something better
        if (!m_path.empty())
        {
            res = _Algorithm::verify(std::get<1>(m_path.front()));
        }
        return res;    
    }
    
    /** \brief Fetch connections for vertex can be enqueued
      * \param[in] vertex - vertex to get fetched
      * \param[in] cost - cost of vertex
      * \param[in] forward - direction of routing (forward/backward)
      * \param[in] target_first - iterator pointing to the first elements of target vertices
      * \param[in] target_last - iterator pointing to the last elements of target vertices
      * \param[in] notifier - callback for notifications, to can follow-up how routing searches the path
      * \returns true, when succeed, otherwise false
      */
    template<typename Iterator,
             typename Notifier>
    bool _fetch(const Vertex& vertex, 
                const Cost& cost,
                bool forward,
                Iterator target_first,
                Iterator target_last,
                Notifier& notifier)
    {
        bool res = true;
        // When notifier is valid -> notify about rouing
        auto& visiteds = (forward ? *std::begin(m_visiteds) : *std::rbegin(m_visiteds));
        if (std::function<bool(const Vertex&, const Vertex&, const Cost&, bool)>(notifier))
        {
            auto it = visiteds.find(vertex);
            if (it != visiteds.end())
            {
                // Visited vertex is: {vertex, {vertex, cost, cost} }
                notifier(vertex, std::get<0>(it->second), std::get<1>(it->second), forward);
            }
            else
            {
                // Notify the first vertex
                notifier(vertex, vertex, Cost{}, forward);
            }
        }
        
        // Get connections - connections are the folloowing trait:
        // -> pair::first = result of data access: true = succeed, false = failed (abort)
        // -> pair::second = iterator pairs to first, last elements of connected vertices
        // ---> connected vertex: pair of {Vertex, Cost}
        auto vertices = m_adapter(vertex, cost, forward);
        // When no error
        res = vertices.first;
        if (res)
        {
            // Iterate the { vertex, cost} connection pairs 
            for (auto it = vertices.second.first; it != vertices.second.second; ++it)
            {
                // Try to add the vertex into the visiteds table
                // -> it won't succeed when already seen, or worse like the earlier visit
                if (_visit(visiteds, it->first, it->second, vertex, cost))
                {
                    // Before enqueueing - check if the connected vertex is a route already
                    if (_verify(it->first, it->second, vertex, cost, target_first, target_last, forward))
                    {
                        // Best route found -> can stop
                        _Algorithm::reset();
                        break;
                    }
                    // Enqueue the vertex for algorithm
                    _Algorithm::enqueue(it->first, cost_plus()(cost, it->second), forward);
                }
            }
        }
        return res;
    }

    /** \brief Add a vertex into the visiteds list
      * \param[in] visiteds - visited vertices table
      * \param[in] vertex - vertex to get enqueued
      * \param[in] cost_vertex - own cost of vertex
      * \param[in] parent - parent vertex
      * \param[in] cost_parent - total cost of parent vertex
      * \returns true, when vertex could be added into the visiteds table, otherwise false
      */
    bool _visit(_Visited& visiteds,
                const Vertex& vertex, 
                const Cost& cost_vertex,
                const Vertex& parent, 
                const Cost& cost_parent)
    {
        bool res = false;
        // Check if already in the visited list
        auto it = visiteds.find(vertex);
        if (it != visiteds.end())
        {
            // When yes - check if earlier visit was more expensive
            if (cost_is_less()(cost_plus()(cost_vertex, cost_parent), std::get<1>(it->second)))
            {
                // when yes -> update with current cost and with the actual parent connection
                // when no -> this route can be aborted
               auto tuple_{ std::make_tuple(parent, 
                                             cost_plus()(cost_vertex, cost_parent),
                                             cost_vertex) };
                std::swap(it->second, tuple_);
                // Connection can be enqueued for re-processing
                res = true;
            }
        }
        else
        {
            // when not - add the (connected vertex, {{ parent vertex, cost, edge cost}} 
            // information into the visiteds list 
            visiteds.insert(std::move(
                            std::make_pair(vertex, 
                            std::make_tuple(parent, 
                                            cost_plus()(cost_vertex, cost_parent),
                                            cost_vertex))));
            // Connection can be enqueued for processing
            res = true;
        }
        return res;
    }
    
    /** \brief Retrieve the path as a list of vertices with their costs
      * \param[in] inserter - front/back inserter iterator
      *  - front inserter - used by forward routing
      *  - back inserter - used by backward routing
      * return true, when there is valid path, otherwise false
      */
    template<typename T = bool,
             typename Iterator>
    typename std::enable_if<RoutingMode::bidirectional != routing_direction, T>::
    type _pull(Iterator inserter) const
    {
        bool res = (!m_path.empty());
        if (res)
        {
            // Insert target vertex
            *inserter = std::make_pair(std::get<0>(m_path.front()), std::get<2>(m_path.front()));

            // Iterate the visiteds table, going back to parent connections
            auto it = std::begin(m_visiteds)->find(std::get<0>(m_path.front()));
            while ((it != std::begin(m_visiteds)->end()) && (!vertex_equal()(it->first, std::get<0>(it->second))))
            {
                // Now use the vertex own dynamic cost, applied during the path search
                // e.g. dynamic cost = static cost + traffic jams, dynamic penalties
                // visited vertex = {vertex, {parent vertex, total cost, own cost}
                // Insert parent vertex and cost of parent -> vertex
                *inserter = std::make_pair(std::get<0>(it->second), std::get<2>(it->second));
                // Fetch parent vertex
                it = std::begin(m_visiteds)->find(std::get<0>(it->second));
            }
        }
        return res;
    }

    /** \brief Retrieve the path as a list of vertices with their costs
      * \param[in] front_inserter - front inserter iterator
      * \param[in] back_inserter - back inserter iterator
      * return true, when there is valid path, otherwise false
      */
    template<typename T = bool,
             typename Iterator_1,
             typename Iterator_2>
    typename std::enable_if<RoutingMode::bidirectional == routing_direction, T>::
    type _pull(Iterator_1 front_inserter, 
               Iterator_2 back_inserter) const
    {
        bool res = (!m_path.empty());
        if (res)
        {
            // Insert the first meeting point with its own cost
            *front_inserter = std::make_pair(std::get<0>(m_path.front()), std::get<2>(m_path.front()));
            // Start with front inserter
            {
                // Localize the meeting point in the forward visiteds table
                auto it = std::begin(m_visiteds)->find(std::get<0>(m_path.front()));
                while ((it != std::begin(m_visiteds)->end()) && (!vertex_equal()(it->first, std::get<0>(it->second))))
                {
                    // Now use the vertex own dynamic cost, applied during the path search
                    // e.g. dynamic cost = static cost + traffic jams, dynamic penalties
                    // visited vertex = {vertex, {parent vertex, total cost, own cost}
                    // insert parent vertex and cost of parent -> vertex
                    *front_inserter = std::make_pair(std::get<0>(it->second), std::get<2>(it->second));

                    // Fetch parent vertex
                    it = std::begin(m_visiteds)->find(std::get<0>(it->second));
                }
            }
            // Finish with back inserter
            {
                // Localize the meeting point in the backward visiteds table
                auto it = std::rbegin(m_visiteds)->find(std::get<0>(m_path.front()));
                while ((it != std::rbegin(m_visiteds)->end()) && (!vertex_equal()(it->first, std::get<0>(it->second))))
                {
                    // Now use the vertex own dynamic cost, applied during the path search
                    // e.g. dynamic cost = static cost + traffic jams, dynamic penalties
                    // visited vertex = {vertex, {parent vertex, total cost, own cost}
                    // insert parent vertex and cost of parent -> vertex
                    *back_inserter = std::make_pair(std::get<0>(it->second), std::get<2>(it->second));

                    // Fetch parent vertex
                    it = std::rbegin(m_visiteds)->find(std::get<0>(it->second));
                }
            }
        }
        return res;
    }
    
    /** \brief Retrieve the count of visited vertices
      * return count of visited vertices
      */
    template<typename T = size_t>
    typename std::enable_if<RoutingMode::bidirectional != routing_direction, T>::
    type _visiteds() const noexcept
    {
        return std::begin(m_visiteds)->size();
    }

    /** \brief Retrieve the count of visited vertices
      * return count of visited vertices
      */
    template<typename T = size_t>
    typename std::enable_if<RoutingMode::bidirectional == routing_direction, T>::
    type _visiteds() const noexcept
    {
        return (std::begin(m_visiteds)->size() + std::rbegin(m_visiteds)->size());
    }

    /** \brief Reset routing algorithm
      */
    void _reset() noexcept
    {
        _Algorithm::reset();
        m_path.clear();
        std::for_each(std::begin(m_visiteds),
                      std::end(m_visiteds), 
                      [](auto& it){it.clear();});
    }
    
private:
    Adapter&    m_adapter;
    _Visited    m_visiteds[RoutingMode::bidirectional == routing_direction ? 2 : 1];
   _Path        m_path;
};

}
}

#endif //__EDU_ROUTING_DIJKSTRA_H__
