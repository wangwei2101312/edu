///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.30.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_GRAPHS_VGRAPH_H__
#define __EDU_GRAPHS_VGRAPH_H__

#include <edu/graphs/xgraph.h>

namespace edu
{
namespace graph
{

/** \brief Vertex graph: vertices are undirected, are points
  *  Useful for modelling point based vertex-connection edge graphs, 
  *   (eg. ISP nodes, one-way only road networks, theoretical graphs) 
  *  See further details in the description of _XGraph
  */
template <typename Vertex,
          typename Edge,
          bool is_forward_only = false>
class VGraph : public XGraph<Vertex, Edge, is_forward_only, false>
{
    using _XGraph = XGraph<Vertex, Edge, is_forward_only, false>; 
public:
    /** \brief Constructor
      */
    VGraph()  = default;

    /** \brief Constructor
      * \param[in] vertices - number of vertices to be supported
      */
    VGraph(size_t vertices)
    : _XGraph(vertices)
    {
    }

public:
    /** \brief Adds an edge into the graph, as connection between two vertices
      * The vertex indexes must fit
      * \param[in] vertex - reference vertex index
      * \param[in] connected_vertex - connected vertex index
      * \param[in] edge - edge object
      */
    void set_edge(size_t vertex,
                  size_t connected_vertex,
                  const Edge& edge)
    {
        _set_edge(vertex, connected_vertex, edge);
    }

    /** \brief Get connected edges for a given vertex
      * \param[in] vertex - iterator to vertex
      * \returns pair of (first, last) iterators to vertex edges
      */
    template<typename T = typename _XGraph::_Vertex_Edges>
    typename std::enable_if<is_forward_only, T>::
    type get_vertex_edges(typename _XGraph::iterator vertex) const noexcept
    {
        return _get_vertex_edges(vertex);
    }

    /** \brief Get connected edges for a given vertex
      * \param[in] vertex - iterator to vertex
      * \param[in] forward - when true, returns outgoing edges, otherwise incoming edges
      * \returns pair of (first, last) iterators to vertex edges
      */
    template<typename T = typename _XGraph::_Vertex_Edges>
    typename std::enable_if<!is_forward_only, T>::
    type get_vertex_edges(typename _XGraph::iterator vertex,
                          bool forward) const noexcept
    {
        return _get_vertex_edges(vertex, forward);
    }

private:
    /** \brief Adds an edge into the graph, as connection between two vertices
      * The vertex indexes must fit
      * \param[in] vertex - reference vertex index
      * \param[in] connected_vertex - connected vertex index
      * \param[in] edge - edge object
      */
    template<typename T = void>
    typename std::enable_if<is_forward_only, T>::
    type _set_edge(size_t vertex,
                   size_t connected_vertex,
                   const Edge& edge)
    {
        assert((vertex < this->m_vertices.size()) &&
               (connected_vertex < this->m_vertices.size()));
        // Add forward connection for vertex
        this->m_connections[vertex].push_front(
                std::move(typename _XGraph::_Edge(connected_vertex, edge)));
    }

    /** \brief Adds an edge into the graph, as forward/backward connection between two vertices
      * The vertex indexes must fit
      * \param[in] vertex - reference vertex index
      * \param[in] connected_vertex - connected vertex index
      * \param[in] edge - edge object
      */
    template<typename T = void>
    typename std::enable_if<!is_forward_only, T>::
    type _set_edge(size_t vertex,
                   size_t connected_vertex,
                   const Edge& edge)
    {
        assert((vertex < this->m_vertices.size()) &&
               (connected_vertex < this->m_vertices.size()));
        
        // Add forward connection for vertex
        std::get<0>(this->m_connections[vertex]).push_front(
                std::move(typename _XGraph::_Edge(connected_vertex, edge)));
        
        // Add backward connection for connected vertex
        std::get<1>(this->m_connections[connected_vertex]).push_front(
                std::move(typename _XGraph::_Edge(vertex, edge)));
    }
    
    /** \brief Get connected vertices for a given vertex, when graph supports uni-directional routing
      * \param[in] vertex - iterator to vertex
      * \returns pair of (first, last) iterators to vertex edges
      */
    template<typename T = typename _XGraph::_Vertex_Edges>
    typename std::enable_if<is_forward_only, T>::
    type _get_vertex_edges(typename _XGraph::iterator vertex ) const noexcept
    {
        return typename _XGraph::_Vertex_Edges
        { 
            // // return iterator to first and last elements of outgoing connections
            std::begin(this->m_connections[vertex - std::begin(this->m_vertices)]),
            std::end(this->m_connections[vertex - std::begin(this->m_vertices)])
        };
    }
    
    /** \brief Get connected vertices for a given vertex, when graph supports bi-directional routing
      * \param[in] vertex - iterator to vertex
      * \param[in] forward - when true, returns outgoing edges, otherwise incoming edges
      * \returns pair of (first, last) iterators to vertex edges
      */
    template<typename T = typename _XGraph::_Vertex_Edges>
    typename std::enable_if<!is_forward_only, T>::
    type _get_vertex_edges(typename _XGraph::iterator vertex,
                           bool forward ) const noexcept
    {
        return forward 
            ? // return iterator to first and last elements of outgoing connections 
            typename _XGraph::_Vertex_Edges
            ( 
                std::begin(std::get<0>(this->m_connections[vertex - std::begin(this->m_vertices)])),
                std::end(std::get<0>(this->m_connections[vertex - std::begin(this->m_vertices)]))
            )
            : // return iterator to first and last elements of incoming connections
            typename _XGraph::_Vertex_Edges
            ( 
                std::begin(std::get<1>(this->m_connections[vertex - std::begin(this->m_vertices)])),
                std::end(std::get<1>(this->m_connections[vertex - std::begin(this->m_vertices)]))
            );
    }

};

}
}

#endif //__EDU_GRAPHS_VGRAPH_H__
