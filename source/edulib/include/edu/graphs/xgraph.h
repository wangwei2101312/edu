///////////////////////////////////////////////////////////////////////////////////////////////////
// Understanding graph algorithms 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.26.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_GRAPHS_XGRAPH_H__
#define __EDU_GRAPHS_XGRAPH_H__

#include <edu/graphs/graph_traits.h>
#include <functional>
#include <algorithm>
#include <deque>
#include <forward_list>
#include <assert.h>

namespace edu
{
namespace graph
{
    
/** \brief Abstract graph for storing vertices and their connections
  *  Contains:
  *   -> vertices: a list of vertex objects, indexed from 0 to N by an external builder
  *   -> edges: a list of vertex connections, for each vertex the connection table contains:
  *   ---> forward connections: 
  *          a list of connected vertices in forward direction
  *   ---> backward connections - ONLY when graph is bi-directonal ("is_forward_only" = false)   
  *          a list of connected vertices in backward direction
  *   ------> each connection information contains a pair of:
  *   ---------> connected vertex index (from the 0 .. N index range of graph vertices) 
  *   ---------> direction of connected vertex - ONLY when vertex is bi-directional ("directed_connections" = true)
  *   ---------> connecting edge information (e.g. costs, length etc.)             
  */
template <typename Vertex,
          typename Edge,
          bool is_forward_only,
          bool directed_connections = false>
class XGraph
{
protected:
    // Vertex container
    using _Vertices = std::deque<Vertex>;
    using iterator = typename _Vertices::const_iterator;
    
    // Graph edge: edge -> connected vertex index
    using _ConEdge = typename std::conditional<directed_connections,
                              std::pair<size_t, bool>,
                              size_t>::type;
    using _Edge = typename std::conditional<std::is_void<Edge>::value, 
                           _ConEdge,
                           std::pair<_ConEdge, Edge>>::type;
    // Graph edges list for a vertex
    using _Edges = typename std::conditional<is_forward_only,
                            std::forward_list<_Edge>,
                            std::tuple<std::forward_list<_Edge>,std::forward_list<_Edge>>>::type;
    using edge_iterator = typename std::forward_list<_Edge>::const_iterator;
    
    // Vertex edges
    using _Vertex_Edges = std::pair<edge_iterator,
                                    edge_iterator>;
    // Connections container
    using _Connections = std::deque<_Edges>;

protected:
    /** \brief Constructor
      */
    XGraph() = default;

    /** \brief Constructor
      * \param[in] vertices - number of vertices to be supported
      */
    XGraph(size_t vertices)
    : m_vertices(vertices)
    , m_connections(vertices)
    {
    }
        
    /** \brief Destructor
      */
    virtual ~XGraph() = default;
    
public:
    /** \brief Exported trait information
      */ 
    static const bool has_edge = !std::is_void<Edge>::value;
    static const bool forward_only = is_forward_only;
    
    /** \brief Resizes the graphs
      * \param[in] vertices - number of vertices to be supported
      */
    void resize(size_t vertices)
    {
        _resize(vertices);
    }

    /** \brief Cleans up the graph
      */
    void clear() noexcept
    {
        m_vertices.clear();
        m_connections.clear();
    }
    
    /** \brief Adds a vertex into the graph
      *        When the vertex index doesn't fit, the graph is resized
      * \param[in] vertex_index - index of vertex
      * \param[in] vertex - vertex object
      */
    void set_vertex(size_t vertex_index,
                    const Vertex& vertex)
    {
        _set_vertex(vertex_index, vertex);
    }
    
    /** \brief First iterator of vertices
      * \returns iterator to vertices container for first position
      */
    iterator begin() const noexcept
    {
        return std::begin(m_vertices);
    }

    /** \brief Last iterator of vertices
      * \returns iterator to vertices container for last position
      */
    iterator end() const noexcept
    {
        return std::end(m_vertices);
    }

    /** \brief Retrieve the the graph size 
      * \returns vertex count
      */
    size_t size() const noexcept
    {
        return m_vertices.size();
    }

    /** \brief Get vertex object
      * \param[in] pos - position of vertex (index) in the graph
      * \returns iterator to vertexc object
      */
    const iterator get_vertex(size_t pos) const noexcept
    {
        return _get_vertex(pos);
    }

protected:
    /** \brief Resizes the graphs
      * \param[in] vertices - number of vertices to be supported
      */
    void _resize(size_t vertices)
    {
        m_vertices.resize(vertices);
        m_connections.resize(vertices);
    }

    /** \brief Adds a vertex into the graph
      *        When the vertex index doesn't fit, the graph is resized
      * \param[in] pos - position of vertex (index) in the graph
      * \param[in] vertex - vertex object
      */
    void _set_vertex(size_t pos,
                     const Vertex& vertex)
    {
        if (pos >= m_vertices.size())
        {
            _resize(pos + 1);
        }
        m_vertices[pos] = vertex;
    }
    
    /** \brief Get vertex object
      * \param[in] pos - position of vertex (index) in the graph
      * \returns iterator to vertex object
      */
    iterator _get_vertex(size_t pos) const noexcept
    {
        return (std::begin(m_vertices) + pos);
    }

protected:
    // Vertex container
    _Vertices     m_vertices;
    // Connections
    _Connections  m_connections;
    
};


}
}

#endif //__EDU_GRAPHS_XGRAPH_H__
