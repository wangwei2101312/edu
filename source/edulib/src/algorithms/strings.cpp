///////////////////////////////////////////////////////////////////////////////////////////////////
// Algorithms on strings
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.05.25.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <edu/algorithms/strings.h>
#include <algorithm>

namespace edu
{
namespace algorithms
{
namespace string
{

///////////////////////////////////////////////////////////////////////////////////////////////////
void invertWordChars(std::string& str, const std::string& whiteSpaces)
{
    auto it = str.begin();
    auto from = 0;
    do
    {
        // Search string until character in whitespaces
        it = std::find_if(it, str.end(), [&whiteSpaces](const char ch)
        {
            // stop search when character in white-spaces
            return (std::find(whiteSpaces.begin(), whiteSpaces.end(), ch) != whiteSpaces.end());
        });

        // Swap characters in the found word
        for (auto ch = 0; ch < ( ( (it - str.begin()) - from) / 2); ch++)
        {
            std::swap(str[from + ch], str[(it - str.begin()) - ch - 1]);
        }

        // Jump over white-spaces - if any
        it = std::find_if(it, str.end(), [&whiteSpaces](const char ch)
        {
            // stop search when character NOT in white-spaces
            return (std::find(whiteSpaces.begin(), whiteSpaces.end(), ch) == whiteSpaces.end());
        });

        // Move "from" position forward
        from = it - str.begin();
    } while(it != str.end());
}


///////////////////////////////////////////////////////////////////////////////////////////////////
void findLongestSequence(const std::string& str, int32_t& pos, size_t& len)
{
    // Initialize length and position
    len = (str.length() ? 1 : 0);
    pos = (str.length() ? 0 : -1);
    // Initialize max length as 1
    auto max = len;
    for (decltype(str.length()) i = 1; i < str.length(); i++)
    {
        if (str[i] == str[i - 1])
        {
            // When consecutive characters - increase the max length
            max++;
            if (max > len)
            {
                // When max length exceeds the candidate length
                // - update the candidate length and position
                len = max;
                pos = i - len + 1;
            }
        }
        else
        {
            // When NON consecutive characters - reset max length to 1
            max = 1;
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
auto isUniqueChars(const std::string& str, int32_t& pos)->bool
{
    pos = -1;
    for (auto it_bs = str.begin(); ((-1 == pos) && (it_bs != str.end())); ++it_bs)
    {
        for (auto it_fw = it_bs + 1; ((-1 == pos) && (it_fw != str.end())); ++it_fw)
        {
            if (*it_bs == *it_fw)
            {
                pos = it_fw - str.begin();
            }
        }
    }
    return (-1 == pos);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
auto isUniqueChars2(const std::string& str, int32_t& pos)->bool
{
    pos = -1;
    for (auto it = str.begin(); ((-1 == pos) && (it != str.end())); ++it)
    {
        auto found = std::find(it + 1, str.end(), *it);
        if (found != str.end())
        {
            pos = found - str.begin();
        }
    }
    return (-1 == pos);
}


}
}
}
