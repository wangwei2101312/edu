################################################################################
#  EDU library
#
#  Static library containing:
#   1. Educational samples
#
################################################################################

# Entry point in EDU library build 
message("Generating EDU library")

# Project name 
project(edulib)

# Header files
set(header_files
    include/edu/log.h
    include/edu/specifiers/explicit.h
    include/edu/dynamic/sequence.h
    include/edu/sorting/heapsort.h
    include/edu/sorting/quicksort.h
    include/edu/sorting/mergesort.h
    include/edu/sorting/radixsort.h
    include/edu/sorting/insertionsort.h
    include/edu/sorting/selectionsort.h
    include/edu/sorting/bubblesort.h
    include/edu/graphs/graph_traits.h
    include/edu/graphs/xgraph.h
    include/edu/graphs/vgraph.h
    include/edu/graphs/egraph.h
    include/edu/graphs/graph_builder.h
    include/edu/graphs/routing.h
    include/edu/graphs/bfs.h
    include/edu/graphs/dfs.h
    include/edu/graphs/dijkstra.h
    include/edu/algorithms/binary_search.h
    include/edu/algorithms/strings.h
    include/edu/containers/xhash.h
    include/edu/containers/hashmap.h
    include/edu/containers/hashset.h
    include/edu/containers/hashmultimap.h
    include/edu/containers/hashmultiset.h
)

# Source files
set(source_files src/
    src/log.cpp
    src/algorithms/strings.cpp
)

# Generate static library
add_library(edulib STATIC ${source_files} ${header_files})

if(MSVC)
    # Include in solution group 
    set_property(TARGET edulib PROPERTY FOLDER "Modules/Libraries")
    # Group source files
    source_group("edu" FILES 
        include/edu/log.h
        src/log.cpp
     )
    source_group("edu\\dynamic" FILES 
        include/edu/dynamic/sequence.h
     )
    source_group("edu\\specifiers" FILES 
        include/edu/specifiers/explicit.h
     )
    source_group("edu\\sorting" FILES 
        include/edu/sorting/heapsort.h
        include/edu/sorting/quicksort.h
        include/edu/sorting/mergesort.h
        include/edu/sorting/radixsort.h
        include/edu/sorting/insertionsort.h
        include/edu/sorting/selectionsort.h
        include/edu/sorting/bubblesort.h
     )
    source_group("edu\\graphs" FILES 
        include/edu/graphs/graph_traits.h
        include/edu/graphs/xgraph.h
        include/edu/graphs/vgraph.h
        include/edu/graphs/egraph.h
        include/edu/graphs/graph_builder.h
        include/edu/graphs/routing.h
        include/edu/graphs/bfs.h
        include/edu/graphs/dfs.h
        include/edu/graphs/dijkstra.h
     )
    source_group("edu\\algorithms" FILES 
        include/edu/algorithms/binary_search.h
        include/edu/algorithms/strings.h src/algorithms/strings.cpp
     )
    source_group("edu\\containers" FILES 
        include/edu/containers/xhash.h
        include/edu/containers/hashmap.h
        include/edu/containers/hashset.h
        include/edu/containers/hashmultimap.h
        include/edu/containers/hashmultiset.h
     )

endif()

# Export include folder
target_include_directories(edulib
    PUBLIC
        include/
)
 
################################################################################
