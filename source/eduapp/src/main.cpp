///////////////////////////////////////////////////////////////////////////////////////////////////
// Source file for educational sample tasks terminal application 
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.08.24.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <tasks/app.h>


/** \brief Entry point in EDU application
  * 
  *  Accepts command line arguments:
  * 
  *         -h = display help
  * 
  *         -d [data folder] = data folder for samples
  * 
  *         ... 
  * 
  *         [group i:sample j] .. [group m][[sample n] = explicit filtering for samples to run,
  *         by specifiying the sample name
  * \param[in] argc - command line parameters count
  * \param[in] argv - command line parameters array
  * \returns 0 when succeed, otherwise 2
  */ 
int main(int argc, 
         char *argv[])
{
    // Add options into a list
    std::list<std::string> options;

    for (int a = 1; a < argc; a++)
    {
        options.push_back(argv[a]);
    }

    // Invoke application with options
    return (edu::App().run(options) ? 0 : 2);
}
