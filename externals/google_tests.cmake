################################################################################

# Include  GTest into the build process
set( GTEST_DIR "externals/gmock-1.7.0")
add_subdirectory( ${GTEST_DIR} )

# Include Gtest into Externals group
set_target_properties( gtest gtest_main gmock gmock_main PROPERTIES FOLDER "Externals/Google Test" )

if(MSVC)
    set_property(TARGET gtest PROPERTY COMPILE_FLAGS /W0)
else()
    set_property(TARGET gtest PROPERTY COMPILE_FLAGS -w)
endif()

# Export GMock headers
target_include_directories(gmock
    INTERFACE
        "${GTEST_DIR}/include"
)
# Export GTest headers
target_include_directories(gtest
    INTERFACE
        "${GTEST_DIR}/gtest/include"
)

################################################################################
