################################################################################
#
#  EDU - Educational sample applications
#
#  build_linux.sh: Generates CMake based build on Linux, using make
#  - Solution is generated into the build/linux sub-folder
#  - Command line parameters: add cmake command line options, when default build
#    parameters need to be changed (see cmake -h for options)
#     Options:
#      -DCMAKE_BUILD_TYPE="Debug" , generates debug build
#      -DEDU_ENABLE_GTEST=ON      , enables the Google Test framework for unit tests
#
###############################################################################

# Create the default build folder as ./build/linux, when not exists
if ! [ -d build ]
then
    mkdir build
fi
if ! [ -d build/linux ]
then
    mkdir build/linux
fi

# Set the build folder as current
cd build/linux

# Launch the cmake build generator, passing the received command line arguments
cmake "$@" ../../

# Build with make
make
