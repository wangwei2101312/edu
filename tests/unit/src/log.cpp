///////////////////////////////////////////////////////////////////////////////////////////////////
// Unit test for log
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.18.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include <edu/log.h>
#include <sstream>


///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Testing Logging mechanism
  */
TEST(log, log) 
{
    {
        std::stringstream stream("");
        ASSERT_STREQ(edu::log_(stream, 
            "One ", 1, ", ", 4.56, 0, true, false).str().c_str(), 
            "One 1, 4.56010");
    }
    {
        std::stringstream stream;
        stream << ("zero");
        std::string str("Two");
        ASSERT_STREQ(edu::log_(stream, 
            "One ", 1, ", ", 4.56, 0, true, false, "->", str).str().c_str(), 
            "zeroOne 1, 4.56010->Two");
    }
    {
        std::stringstream stream("--");
        std::string str("1");
        ASSERT_STREQ(edu::log_(stream,
            0, str, str, std::string(), std::string(), str, 0).str().c_str(),
            "01110");
        ASSERT_STRNE(edu::log_(stream,
            0, str, str, std::string(), std::string(), str, 0).str().c_str(),
            "--01110");
    }
}


