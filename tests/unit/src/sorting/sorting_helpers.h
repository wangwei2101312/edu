///////////////////////////////////////////////////////////////////////////////////////////////////
// Helper data and function for unit tests of sort functions
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.28.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EDU_UNIT_TESTS_SORTING_HELPERS_H__
#define __EDU_UNIT_TESTS_SORTING_HELPERS_H__

#include <stdint.h>
#include <vector>
#include <deque>
#include <list>
#include <forward_list>


namespace
{
    // Test containers for numeric values
    const std::vector<char> vecc_0({});
    const std::vector<char> vecc_00({'c' , 'b', 'a', '\0', '-'});
    const std::vector<int32_t> veci_1({ 8 });
    const std::vector<double> vecd_2({ 8., 6. });
    const std::vector<int32_t> veci_3({ 6, 8, -11, 13, -24, -78, 210, 213, -7 });
    const std::vector<uint32_t> vecu_4({ 6u, 8u, 2u });
    const std::vector<int8_t> veci_5({ 2, 9, 6 });
    const std::vector<int16_t> veci_6({ 2, 7, 8, 9, 1 });
    const std::vector<float> vecf_7({ 2.f, 7.f, 8.f, 9.f, 1.f });
    const std::vector<size_t> vecu_8({ 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 13, 1, 0 });

    // Test containers for string values
    const std::vector<std::string> vecstr_1({ "a", "b", "z", "k" });
    const std::vector<std::string> vecstr_2({ "z", "u" });
    const std::vector<std::string> vecstr_3({ "u", "z" });
    const std::vector<std::string> vecstr_4({ "z", "m", "a" });
    const std::vector<std::string> vecstr_5({ "a", "z", "m" });
    const std::deque<std::string> deqstr_6({ "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "" });
    const std::list<int32_t> lsti_1({ 120, -11, -10, 9, -8, 76, -6, 5, 4, 3, 2, 1, 0 });
    const std::forward_list<uint32_t> lstu_2({ 5, 2, 4, 6, 7, 9, 8, 10, 1, 34, 2, 4, 3, 4, 9, 4, 2 });

    /** \brief Functor iterator based generic "less" compare
      */
    struct is_less
    {
        /** \brief Launching operator
          * \param[in] l - left side iterator
          * \param[in] r - right side iterator
          * \returns true, when left side is less than right side, otherwise false
          */
        template<typename Iterator>
        bool operator()(Iterator l, Iterator r) const
        {
            return *l < *r;
        }
    };

    /** \brief Functor iterator based generic "greater" compare
      */
    struct is_greater
    {
        /** \brief Launching operator
          * \param[in] l - left side iterator
          * \param[in] r - right side iterator
          * \returns true, when left side is greater than right side, otherwise false
          */
        template<typename Iterator>
        bool operator()(Iterator l, Iterator r) const
        {
            return *l > *r;
        }        
    };

    // Test class
    struct Human
    {
        std::string name;
        int32_t     height;

        bool operator < (const Human& other) const
        {
            return (height < other.height);
        }

    };

    // Operator for equal
    bool operator ==(const Human& l, const Human& r)
    {
        return (l.height == r.height) && (l.name == r.name);
    }

    // Functor for sorting child objects ascendant by name
    struct human_by_name
    {
        template<typename Iterator>
        bool operator()(Iterator l, Iterator r)
        {
            return (l->name < r->name);
        }
    };

    // Functor for sorting child objects descendant by height
    struct human_by_tallest
    {
        template<typename Iterator>
        bool operator()(Iterator l, Iterator r) const
        {
            return (l->height > r->height);
        }
    };
    
    // Test containers for Humans
    const std::vector<Human> vech_1({ { "John", 160 }, { "Eduard", 154 }, { "Lionel", 161 }, { "Lionel", 157 }, { "Ed", 141 }, { "Sarah", 151 } });

}

#endif //__EDU_UNIT_TESTS_SORTING_HELPERS_H__
