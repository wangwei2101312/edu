///////////////////////////////////////////////////////////////////////////////////////////////////
// Unit test for heap-sort
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.18.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include <edu/sorting/radixsort.h>
#include "sorting_helpers.h"
#include <stdint.h>
#include <vector>
#include <deque>


namespace
{
        
	/** \brief Functor for human buckets by name for radix-sort
	  */
	template<typename T = Human, bool = false>
	struct buckets_human_by_name
	{
		// Buckets count for character values
		static const unsigned char size = 255;

		// Determines the bucket index for a certain character on a certain sort levels
		static unsigned char index(const T& human,
								   size_t level,
								   size_t max_levels)
		{
            return (((max_levels - level - 1) < human.name.length()) ? human.name.at(max_levels - level - 1) : 0);
		}

		// Length of string
		static size_t length(const T& human)
		{
			return human.name.length();
		}
	};


	// Divider
	static constexpr size_t divider(size_t level) noexcept
	{
		return (level ? (10 * divider(level - 1)) : 1);
	}

	// Digits
	static constexpr size_t digits(size_t num) noexcept
	{
		return (num ? (digits(num / 10) + 1) : 0);
	}

	/** \brief Functor for child buckets by shortest height for radix-sort
	  */
	template<typename T = Human, bool = false>
	struct buckets_human_by_shortest
	{
		// Buckets count for unsigned integer values
		static const size_t size = 10;

		// Determines the bucket index for a certain digit
		static size_t index(const T& human,
						    size_t level,
							size_t)
		{
			return ((human.height / divider(level)) % 10);
		}

		// Digits of human height
		static size_t length(const T& human)
		{
			return digits(human.height);
		}
	};

    
	/** \brief Functor for human buckets by tallest height for radix-sort
	  */
	template<typename T = Human, bool = false>
	struct buckets_human_by_tallest
	{
        // Buckets count for unsigned integer values
        static const size_t size = 10;

        // Determines the bucket index for a certain digit
        static size_t index(const T& human,
							size_t level,
							size_t)
		{
			return (9 - ((human.height / divider(level)) % 10));
		}

        // Digits of human height
        static size_t length(const T& human)
		{
			return digits(human.height);
		}
	};
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Testing radixsort for numeric values
  */
TEST(radixsort, numeric) 
{
    {// Test with empty char vector
        auto& cvec(vecc_0);
        std::vector<char> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific char vector
        auto& cvec(vecc_00);
        std::vector<char> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific integer vector
        auto& cvec(veci_1);
        std::vector<int32_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific integer vector
        auto& cvec(veci_3);
        std::vector<int32_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific unsigned integer vector
        auto& cvec(vecu_4);
        std::vector<uint32_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific integer vector
        auto& cvec(veci_5);
        std::vector<int8_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific integer vector
        auto& cvec(veci_6);
        std::vector<int16_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific unsigned integer vector
        auto& cvec(vecu_8);
        std::vector<size_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific integer list
        auto& clst(lsti_1);
        std::list<int32_t> lst(clst);
        EXPECT_EQ(lst, clst);
        edu::sorting::make_radix_sort(std::begin(lst), std::end(lst));
        for (auto it = std::begin(lst), _it = std::begin(lst); it != std::end(lst); ++it)
        {
            if (it != _it)
            {
                ASSERT_LE(*_it++, *it);
            }
        }
    }
    {// Test with specific forward list
        auto& clst(lstu_2);
        std::forward_list<uint32_t> lst(clst);
        EXPECT_EQ(lst, clst);
        edu::sorting::make_radix_sort(std::begin(lst), std::end(lst));
        for (auto it = std::begin(lst), _it = std::begin(lst); it != std::end(lst); ++it)
        {
            if (it != _it)
            {
                ASSERT_LE(*_it++, *it);
            }
        }
    }
    {// Test with large vector
        const size_t size = 400;
        std::vector<int32_t> vec;
        vec.reserve(2 * size);
        for (size_t i = 0; i < size; i++)
        {
            vec.push_back(i);
            vec.push_back(size - i);
        }
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with large deque
        const int32_t size = 500;
        std::deque<int32_t> deq;
        for (int32_t i = 0; i < size; i++)
        {
            deq.push_back(size - i);
            deq.push_back(i - 300);
            deq.push_back(i + 300);
        }
        edu::sorting::make_radix_sort(std::begin(deq), std::end(deq));
        for (size_t it = 1; it < deq.size(); ++it)
        {
            ASSERT_LE(deq[it - 1], deq[it]);
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Testing radixsort for string values
  */
TEST(radixsort, string)
{
    {// Test with specific string vector
        auto& cvec(vecstr_1);
        std::vector<std::string> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific string vector
        auto& cvec(vecstr_2);
        std::vector<std::string> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific string vector
        auto& cvec(vecstr_3);
        std::vector<std::string> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific string vector
        auto& cvec(vecstr_4);
        std::vector<std::string> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific string vector
        auto& cvec(vecstr_5);
        std::vector<std::string> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific string deque
        auto& cdeq(deqstr_6);
        std::deque<std::string> deq(cdeq);
        EXPECT_EQ(deq, cdeq);
        edu::sorting::make_radix_sort(std::begin(deq), std::end(deq));
        for (size_t it = 1; it < deq.size(); ++it)
        {
            ASSERT_LE(deq[it - 1], deq[it]);
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Testing radixsort for personalized class values
  */
TEST(radixsort, class_human)
{
    {// Test with specific Human vector - by name
        auto& cvec(vech_1);
        std::vector<Human> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort<buckets_human_by_name>(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1].name, vec[it].name);
        }
    }
    {// Test with specific Human vector - by tallest
        auto& cvec(vech_1);
        std::vector<Human> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort<buckets_human_by_tallest>(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_GE(vec[it - 1].height, vec[it].height);
        }
    }
    {// Test with specific Human vector - by shortest
        auto& cvec(vech_1);
        std::vector<Human> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_radix_sort<buckets_human_by_shortest>(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1].height, vec[it].height);
        }
    }
}


