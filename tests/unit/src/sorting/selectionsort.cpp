///////////////////////////////////////////////////////////////////////////////////////////////////
// Unit test for heap-sort
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2018.07.18.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include <edu/sorting/selectionsort.h>
#include "sorting_helpers.h"
#include <stdint.h>
#include <vector>
#include <deque>


///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Testing selectionsort for numeric values
  */
TEST(selectionsort, numeric) 
{
    {// Test with empty char vector
        auto& cvec(vecc_0);
        std::vector<char> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific char vector
        auto& cvec(vecc_00);
        std::vector<char> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific integer vector
        auto& cvec(veci_1);
        std::vector<int32_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific double vector
        auto& cvec(vecd_2);
        std::vector<double> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific integer vector
        auto& cvec(veci_3);
        std::vector<int32_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific unsigned integer vector
        auto& cvec(vecu_4);
        std::vector<uint32_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific integer vector
        auto& cvec(veci_5);
        std::vector<int8_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific integer vector
        auto& cvec(veci_6);
        std::vector<int16_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific float vector
        auto& cvec(vecf_7);
        std::vector<float> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific unsigned integer vector
        auto& cvec(vecu_8);
        std::vector<size_t> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific integer list
        auto& clst(lsti_1);
        std::list<int32_t> lst(clst);
        EXPECT_EQ(lst, clst);
        edu::sorting::make_selection_sort(std::begin(lst), std::end(lst));
        for (auto it = std::begin(lst), _it = std::begin(lst); it != std::end(lst); ++it)
        {
            if (it != _it)
            {
                ASSERT_LE(*_it++, *it);
            }
        }
    }
    {// Test with specific forward list
        auto& clst(lstu_2);
        std::forward_list<uint32_t> lst(clst);
        EXPECT_EQ(lst, clst);
        edu::sorting::make_selection_sort(std::begin(lst), std::end(lst));
        for (auto it = std::begin(lst), _it = std::begin(lst); it != std::end(lst); ++it)
        {
            if (it != _it)
            {
                ASSERT_LE(*_it++, *it);
            }
        }
    }
    {// Test with large vector
        const size_t size = 400;
        std::vector<int32_t> vec;
        vec.reserve(2 * size);
        for (size_t i = 0; i < size; i++)
        {
            vec.push_back(i);
            vec.push_back(size - i);
        }
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec), is_greater());
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_GE(vec[it - 1], vec[it]);
        }
    }
    {// Test with large deque
        const int32_t size = 500;
        std::deque<int32_t> deq;
        for (int32_t i = 0; i < size; i++)
        {
            deq.push_back(size - i);
            deq.push_back(i - 300);
            deq.push_back(i + 300);
        }
        edu::sorting::make_selection_sort(std::begin(deq), std::end(deq), is_less());
        for (size_t it = 1; it < deq.size(); ++it)
        {
            ASSERT_LE(deq[it - 1], deq[it]);
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Testing selectionsort for string values
*/
TEST(selectionsort, string)
{
    {// Test with specific string vector
        auto& cvec(vecstr_1);
        std::vector<std::string> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific string vector
        auto& cvec(vecstr_2);
        std::vector<std::string> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific string vector
        auto& cvec(vecstr_3);
        std::vector<std::string> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec), is_less());
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific string vector
        auto& cvec(vecstr_4);
        std::vector<std::string> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific string vector
        auto& cvec(vecstr_5);
        std::vector<std::string> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec), is_greater());
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_GE(vec[it - 1], vec[it]);
        }
    }
    {// Test with specific string deque
        auto& cdeq(deqstr_6);
        std::deque<std::string> deq(cdeq);
        EXPECT_EQ(deq, cdeq);
        edu::sorting::make_selection_sort(std::begin(deq), std::end(deq));
        for (size_t it = 1; it < deq.size(); ++it)
        {
            ASSERT_LE(deq[it - 1], deq[it]);
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Testing selectionsort for personalized class values
*/
TEST(selectionsort, class_human)
{
    {// Test with specific Human vector - by name
        auto& cvec(vech_1);
        std::vector<Human> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec), human_by_name());
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1].name, vec[it].name);
        }
    }
    {// Test with specific Human vector - by tallest
        auto& cvec(vech_1);
        std::vector<Human> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec), human_by_tallest());
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_GE(vec[it - 1].height, vec[it].height);
        }
    }
    {// Test with specific Human vector - by shortest
        auto& cvec(vech_1);
        std::vector<Human> vec(cvec);
        EXPECT_EQ(vec, cvec);
        edu::sorting::make_selection_sort(std::begin(vec), std::end(vec));
        for (size_t it = 1; it < vec.size(); ++it)
        {
            ASSERT_LE(vec[it - 1].height, vec[it].height);
        }
    }
}
