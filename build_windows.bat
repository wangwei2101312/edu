:: ################################################################################
:: #
:: #  EDU - Educational sample applications
:: #
:: #  build_windows.bat: Generates CMake based build on Windows, using make
:: #  - Solution is generated into the build/windows sub-folder
:: #  - Command line parameters: add cmake command line options, when default generator,
:: #    or default build parameters need to be changed (see cmake -h for options)
:: #     Options:
:: #      -G "Visual Studio 12 2013" , generates solution for MS Visual Studio 2013
:: #      -DCMAKE_BUILD_TYPE="Debug" , generates debug build
:: #      -DEDU_ENABLE_GTEST=ON     , enables the Google Test framework for unit tests
:: #
:: ###############################################################################

:: Create the default build folder as .\build\windows, when not exists
if not exist %cd%\build mkdir %cd%\build
if not exist %cd%\build\windows mkdir %cd%\build\windows

:: Set the build folder as current
cd %cd%\build\windows

:: Launch cmake with the default build generator, passing the received command line arguments
cmake %* %cd%\..\..\

:: Open the generated solution with the relevant IDE and continue the build from there
